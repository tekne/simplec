#include <simplec/preprocessor.hpp>
#include <iostream>

int main(int argc, char** argv)
{
  simplec::Preprocessor P {
    &std::cin,
    simplec::preprocessor_state{
      simplec::preprocessor_file_source{"stdin"},
      "now",
      "now seconds"
    }
  };

  simplec::preprocessor_token tok;
  do {
    tok = P.get_token();
    std::cout<<tok.string();
    if(tok != "\n") std::cout<<" ";
  } while(tok != "");

  return 0;
}
