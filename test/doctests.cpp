// Run all doctests
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include <doctest.h>

#include <simplec/lexer.hpp>
#include <simplec/preprocessor.hpp>

#include <simplec/lexer/read_identifier.hpp>
#include <simplec/lexer/read_char_literal.hpp>
#include <simplec/lexer/read_string_literal.hpp>
#include <simplec/lexer/read_preprocessing_number.hpp>
#include <simplec/lexer/read_punctuator.hpp>
#include <simplec/lexer/skip_whitespace.hpp>
#include <simplec/lexer/read_token.hpp>
#include <simplec/lexer/read_hchar_literal.hpp>
#include <simplec/lexer/read_escape_sequence.hpp>

#include <simplec/preprocessor/tokenization_tests.hpp>
#include <simplec/preprocessor/filesystem_tests.hpp>
#include <simplec/preprocessor/conditional_tests.hpp>
#include <simplec/preprocessor/concatenation_tests.hpp>
#include <simplec/parser/type_tests.hpp>

#include <simplec/parser/lexical_token.hpp>
#include <simplec/parser/types/type_set.hpp>
#include <simplec/parser/token/buffer.hpp>
#include <simplec/parser/expression.hpp>
#include <simplec/parser/declaration.hpp>

#include <simplec/parser/expression_printing_tests.hpp>
#include <simplec/parser/expression_parsing_tests.hpp>
