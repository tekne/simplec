#include <simplec/preprocessor.hpp>

namespace simplec
{

  TEST_SUITE_BEGIN("Concatenation tests");

  TEST_CASE("Basic concatenation functionality")
  {
    StringPreprocessor P {
      "x ## y;\n"
      "concat ##\n"
      "after ## newline ## and ## in ## line;\n"
    };

    std::vector<std::string> goal_tokenization = {
      "xy", ";", "concatafternewlineandinline", ";", ""
    };

    std::vector<preprocessor_token> tokens;
    do {
      tokens.push_back(P.get_stripped());
    } while(!tokens.back().empty());

    REQUIRE_EQ(tokens.size(), goal_tokenization.size());
    for(size_t i = 0; i < tokens.size(); i++)
    {
      CHECK_EQ(tokens[i].string(), goal_tokenization[i]);
    }
  }

  TEST_SUITE_END();

}
