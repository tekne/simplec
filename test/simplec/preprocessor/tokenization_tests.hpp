#include <simplec/preprocessor.hpp>

namespace simplec
{

  TEST_SUITE_BEGIN("Basic tokenization tests");

  TEST_CASE("Simple integer arithmetic expressions are lexed properly")
  {
    StringPreprocessor P {
      "1 + 1;"
      "2 * 1;"
      "4 - 2;"
      "3 / 4;"
      "1 % 4;"
      "1 << 2;"
      "1 >> 2;"
      "5 & 3;"
      "3 | 5;"
      "7 ^ 2;"
      "1 == 3;"
      "1 != 3;"
      "6 > 4;"
      "6 < 4;"
      "6 >= 2;"
      "6 <= 2;"
      "1 || 1;"
      "1 && 0;"
      "-1;"
      "~3;"
      "+1;"
    };

    std::vector<std::string> goal_tokenization = {
      "1", "+", "1", ";", "2", "*", "1", ";", "4", "-", "2", ";",
      "3", "/", "4", ";", "1", "%", "4", ";", "1", "<<", "2", ";",
      "1", ">>", "2", ";", "5", "&", "3", ";", "3", "|", "5", ";",
      "7", "^", "2", ";", "1", "==", "3", ";", "1", "!=", "3", ";",
      "6", ">", "4", ";", "6", "<", "4", ";", "6", ">=", "2", ";",
      "6", "<=", "2", ";", "1", "||", "1", ";", "1", "&&", "0", ";",
      "-", "1", ";", "~", "3", ";", "+", "1", ";", "\n", ""
    };

    std::vector<preprocessor_token> tokens;
    do {
      tokens.push_back(P.get_token());
    } while(!tokens.back().empty());

    REQUIRE_EQ(tokens.size(), goal_tokenization.size());
    for(size_t i = 0; i < tokens.size(); i++)
    {
      CHECK_EQ(tokens[i].string(), goal_tokenization[i]);
    }
  }

  TEST_CASE("Simple floating point expressions are lexed properly")
  {
    StringPreprocessor P {
      "3.141592654 + 2.718281828;"
      "8.9875517873681764 * 1e9;"
      "100.3 / 32.1f;"
      "1.4 - 2.3;"
    };


    std::vector<std::string> goal_tokenization = {
      "3.141592654", "+", "2.718281828", ";",
      "8.9875517873681764", "*", "1e9", ";",
      "100.3", "/", "32.1f", ";",
      "1.4", "-", "2.3", ";", "\n",
      ""
    };

    std::vector<preprocessor_token> tokens;

    do {
      tokens.push_back(P.get_token());
    } while(!tokens.back().empty());

    REQUIRE_EQ(tokens.size(), goal_tokenization.size());
    for(size_t i = 0; i < tokens.size(); i++)
    {
      CHECK_EQ(tokens[i].string(), goal_tokenization[i]);
    }
  }

  TEST_CASE("Arithmetic expressions are lexed properly")
  {
    StringPreprocessor P {
      "#define PI 3.141592654\n"
      "double r = get_radius();\n"
      "double V = (4.0/3.0) * PI * r * r * r;\n"
      "double density = get_density();\n"
      "double stand_volume = get_stand_height() * (r/30);"
      "double stand_mass = density * stand_volume;\n"
      "double mass = density * V + stand_mass;\n"
      "return stand_mass * get_metal_cost();\n"
    };

    std::vector<std::string> goal_tokenization = {
      "double", "r", "=", "get_radius", "(", ")", ";",
      "double", "V", "=", "(", "4.0", "/", "3.0", ")", "*",
      "3.141592654", "*","r", "*", "r", "*", "r", ";",
      "double", "density", "=", "get_density", "(", ")", ";",
      "double", "stand_volume", "=", "get_stand_height", "(", ")", "*",
      "(", "r", "/", "30", ")", ";",
      "double", "stand_mass", "=", "density", "*", "stand_volume", ";",
      "double", "mass", "=", "density", "*", "V", "+", "stand_mass", ";",
      "return", "stand_mass", "*", "get_metal_cost", "(", ")", ";", ""
    };

    std::vector<preprocessor_token> tokens;
    do {
      tokens.push_back(P.get_stripped());
    } while(!tokens.back().empty());

    REQUIRE_EQ(tokens.size(), goal_tokenization.size());
    for(size_t i = 0; i < tokens.size(); i++)
    {
      CHECK_EQ(tokens[i].string(), goal_tokenization[i]);
    }
  }

  TEST_CASE("Maximal munch")
  {
    StringPreprocessor P {
      "c = b+++++a;"
      "x = y/*z;"
    };
    std::vector<std::string> goal_tokenization = {
      "c", "=", "b", "++", "++", "+", "a", ";", "x", "=", "y", ""
    };

    std::vector<preprocessor_token> tokens;
    do {
      tokens.push_back(P.get_stripped());
    } while(!tokens.back().empty());

    REQUIRE_EQ(tokens.size(), goal_tokenization.size());
    for(size_t i = 0; i < tokens.size(); i++)
    {
      CHECK_EQ(tokens[i].string(), goal_tokenization[i]);
    }
  }

  TEST_SUITE_END();

}
