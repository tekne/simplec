#include <simplec/preprocessor.hpp>

namespace simplec
{

  TEST_SUITE_BEGIN("Conditional compilation tests");

  TEST_CASE("Basic conditional compilation functionality")
  {
    StringPreprocessor P {
      "#if (1 + 1) ^ 2\n"
      " puts(\"(1 + 1) ^ 2 != 0\");\n"
      "#elif (1 + 1) ^ 5 == 4\n"
      " puts(\"(1 + 1) ^ 5 == 4\");\n"
      "#else"
      " puts(\"(1 + 1) ^ 2 == 0\");\n"
      "#endif\n"
    };

    std::vector<std::string> goal_tokenization = {
      "puts", "(", "\"(1 + 1) ^ 2 == 0\"", ")", ";", ""
    };

    std::vector<preprocessor_token> tokens;
    do {
      tokens.push_back(P.get_stripped());
    } while(!tokens.back().empty());

    REQUIRE_EQ(tokens.size(), goal_tokenization.size());
    for(size_t i = 0; i < tokens.size(); i++)
    {
      CHECK_EQ(tokens[i].string(), goal_tokenization[i]);
    }
  }

  TEST_CASE("Mixing #elif with #ifdef and #ifndef")
  {
    StringPreprocessor P {
      "#ifdef NOT_DEFINED\n"
      " fprintf(stderr, \"NOT_DEFINED is defined\");\n"
      "#elif defined(NOT_DEFINED)\n"
      " fprintf(stderr, \"NOT_DEFINED is... half defined?\");\n"
      "#elif !defined(NOT_DEFINED)\n"
      " fprintf(stderr, \"NOT_DEFINED is not defined\");\n"
      "#else\n"
      " fprintf(stderr, \"OK something weird is going on...\");\n"
      "#endif\n"
      "#define IS_DEFINED\n"
      "#ifndef IS_DEFINED\n"
      " fprintf(stderr, \"IS_DEFINED is not defined\");\n"
      "#elif defined(IS_DEFINED)\n"
      " fprintf(stderr, \"IS_DEFINED is defined\");\n"
      "#else\n"
      " fprintf(stderr, \"OK something weird is going on...\");\n"
      "#endif\n"
    };

    std::vector<std::string> goal_tokenization = {
      "fprintf", "(", "stderr", ",", "\"NOT_DEFINED is not defined\"", ")", ";",
      "fprintf", "(", "stderr", ",", "\"IS_DEFINED is defined\"", ")", ";", ""
    };

    std::vector<preprocessor_token> tokens;
    do {
      tokens.push_back(P.get_stripped());
    } while(!tokens.back().empty());

    REQUIRE_EQ(tokens.size(), goal_tokenization.size());
    for(size_t i = 0; i < tokens.size(); i++)
    {
      CHECK_EQ(tokens[i].string(), goal_tokenization[i]);
    }
  }

  TEST_SUITE_END();

}
