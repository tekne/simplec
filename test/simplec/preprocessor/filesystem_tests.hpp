#include <simplec/preprocessor.hpp>
#include <fstream>

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>

namespace simplec
{

  TEST_SUITE_BEGIN("Lexer file system tests");

  TEST_CASE("Manually import simple header file")
  {
    auto temp_path =
    boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
    std::ofstream temp_file = boost::filesystem::ofstream(temp_path);
    temp_file << "#define LIBRARY_MACRO library_function\n"
    << "#define FUNC_ARGS int argc, char** argv\n"
    << "int library_function(FUNC_ARGS) {\n"
    << "  return 0;\n"
    << "}\n";

    temp_file.close();

    StringPreprocessor P {
      "int main(int argc, char** argv)\n"
      "{\n"
      " return LIBRARY_MACRO(argc, argv);\n"
      "}\n"
    };

    P.open_path(temp_path);

    std::vector<std::string> goal_tokenization = {
      "int", "library_function", "(", "int", "argc", ",", "char", "*", "*",
      "argv", ")", "{", "return", "0", ";", "}", "int", "main", "(", "int",
      "argc", ",", "char", "*", "*", "argv", ")", "{", "return",
      "library_function", "(", "argc", ",", "argv", ")", ";", "}", ""
    };
    std::vector<preprocessor_token> tokens;

    do {
      tokens.push_back(P.get_stripped());
    } while(!tokens.back().empty());

    REQUIRE_EQ(goal_tokenization.size(), tokens.size());

    for(size_t i = 0; i < tokens.size(); i++)
    {
      CHECK_EQ(goal_tokenization[i], tokens[i].string());
    }

    remove(temp_path.string().c_str());
  }

  TEST_CASE("Include simple header file")
  {
    auto temp_path =
    boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
    std::ofstream temp_file = boost::filesystem::ofstream(temp_path);
    temp_file << "#define LIBRARY_MACRO library_function\n"
    << "#define FUNC_ARGS int argc, char** argv\n"
    << "int library_function(FUNC_ARGS) {\n"
    << "  return 0;\n"
    << "}\n";

    temp_file.close();

    StringPreprocessor P {
      "#include \"" + temp_path.string() + "\"\n"
      "int main(int argc, char** argv)\n"
      "{\n"
      " return LIBRARY_MACRO(argc, argv);\n"
      "}\n"
    };

    std::vector<std::string> goal_tokenization = {
      "int", "library_function", "(", "int", "argc", ",", "char", "*", "*",
      "argv", ")", "{", "return", "0", ";", "}", "int", "main", "(", "int",
      "argc", ",", "char", "*", "*", "argv", ")", "{", "return",
      "library_function", "(", "argc", ",", "argv", ")", ";", "}", ""
    };
    std::vector<preprocessor_token> tokens;

    do {
      tokens.push_back(P.get_stripped());
    } while(!tokens.back().empty());

    REQUIRE_EQ(goal_tokenization.size(), tokens.size());

    for(size_t i = 0; i < tokens.size(); i++)
    {
      CHECK_EQ(goal_tokenization[i], tokens[i].string());
    }

    remove(temp_path.string().c_str());
  }

  TEST_CASE("Line detection and file name modification")
  {
    auto temp_path =
    boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
    std::ofstream temp_file = boost::filesystem::ofstream(temp_path);
    temp_file << "const char* file_name()\n"
    << "{\n"
    << "  return __FILE__;\n"
    << "}\n\n"
    << "unsigned line_number()\n"
    << "{\n"
    << "  return __LINE__;\n"
    << "}\n\n"
    << "#line 44 \"fancy_name.h\"\n"
    << "unsigned new_line_number()\n"
    << "{\n"
    << "  return __LINE__;\n"
    << "}\n\n"
    << "const char* new_file_name()\n"
    << "{\n"
    << "  return __FILE__;"
    << "}\n";

    temp_file.close();

    StringPreprocessor P {
      "#include \"" + temp_path.string() + "\"\n"
      "int main()\n"
      "{\n"
      " const char* file_name = __FILE__;\n"
      " unsigned line_num = __LINE__;\n"
      "#line 11 \"changed_in_function.hpp\"\n"
      " file_name = __FILE__;\n"
      " line_num = __LINE__;\n"
      " return 0;\n"
      "}\n",
      "test_input"
    };

    std::vector<std::string> goal_tokenization = {
      "const", "char", "*", "file_name", "(", ")", "{",
      "return", "\"" + temp_path.string() + "\"", ";", "}",
      "unsigned", "line_number", "(", ")", "{", "return", "6", ";", "}",
      "unsigned", "new_line_number", "(", ")", "{", "return", "47", ";", "}",
      "const", "char", "*", "new_file_name", "(", ")", "{",
      "return", "\"fancy_name.h\"", ";", "}",
      "int", "main", "(", ")", "{", "const", "char", "*",
      "file_name", "=", "\"test_input\"", ";",
      "unsigned", "line_num", "=", "4", ";",
      "file_name", "=", "\"changed_in_function.hpp\"", ";",
      "line_num", "=", "13", ";",
      "return", "0", ";", "}", ""
    };
    std::vector<preprocessor_token> tokens;

    do {
      tokens.push_back(P.get_stripped());
    } while(!tokens.back().empty());

    REQUIRE_EQ(goal_tokenization.size(), tokens.size());

    for(size_t i = 0; i < tokens.size(); i++)
    {
      CHECK_EQ(goal_tokenization[i], tokens[i].string());
    }

    remove(temp_path.string().c_str());
  }

  TEST_SUITE_END();

}
