#ifndef SIMPLEC_EXPRESSION_PARSING_TESTS_INCLUDED
#define SIMPLEC_EXPRESSION_PARSING_TESTS_INCLUDED

#include <simplec/parser/expression.hpp>

namespace simplec
{

  TEST_SUITE_BEGIN("Expression parsing tests");

  TEST_CASE("Basic primary expressions are parsed properly")
  {
    StringExpressionParser P{"12 0x13 2.2l 311llU abc L\"123\" \'a\' if"};

    std::vector<std::string> goal_strings = {
      "12", "19", "2.2l", "311", "abc", "L\"123\"", "97"
    };

    for(size_t i = 0; i < goal_strings.size(); i++) {
      auto expr = P.parse_primary_expression();
      REQUIRE_UNARY(expr);
      CHECK_EQ(expr->to_string(), goal_strings[i]);
    }

    // if should NOT be parsed as an identifier
    CHECK_UNARY_FALSE(P.parse_primary_expression());
    CHECK_UNARY(P.good());
  }

  TEST_CASE("Basic postfix expressions are parsed correctly")
  {
    StringExpressionParser P("p x++-- p++->s.m ++p ++");

    std::vector<std::string> goal_strings = {
      "p", "((x++)--)", "((p++)->s.m++)", "(p++)"
    };

    for(size_t i = 0; i < goal_strings.size(); i++)
    {
      auto expr = P.parse_postfix_expression();
      REQUIRE_UNARY(expr);
      CHECK_EQ(expr->to_string(), goal_strings[i]);
    }
  }

  TEST_CASE("Basic unary increment/decrement expressions are parsed correctly")
  {
    StringExpressionParser P("++++x->s--;");

    std::vector<std::string> goal_strings = {
      "(++(++(x->s--)))"
    };

    for(size_t i = 0; i < goal_strings.size(); i++)
    {
      auto expr = P.parse_unary_expression();
      REQUIRE_UNARY(expr);
      CHECK_EQ(expr->to_string(), goal_strings[i]);
      auto semicolon = P.parse_token();
      CHECK_EQ(semicolon.get_punctuator(), punctuator_semicolon);
    }
  }

  TEST_CASE("Basic unary operator expressions are parsed correctly")
  {
    StringExpressionParser P("-*ptr++;");

    std::vector<std::string> goal_strings = {
      "(-(*(ptr++)))"
    };

    for(size_t i = 0; i < goal_strings.size(); i++)
    {
      auto expr = P.parse_unary_expression();
      REQUIRE_UNARY(expr);
      CHECK_EQ(expr->to_string(), goal_strings[i]);
      auto semicolon = P.parse_token();
      CHECK_EQ(semicolon.get_punctuator(), punctuator_semicolon);
    }
  }

  TEST_CASE("Basic cast expressions are parsed correctly")
  {
    StringExpressionParser P("(my_cool_type)*ptr;");
    P.get_typeset().register_typedef(
      "my_cool_type", {P.get_typeset().get_int_ptr(), const_qualified}
    );

    std::vector<std::string> goal_strings = {
      "(int const)((*ptr))"
    };

    for(size_t i = 0; i < goal_strings.size(); i++)
    {
      auto expr = P.parse_cast_expression();
      REQUIRE_UNARY(expr);
      CHECK_EQ(expr->to_string(), goal_strings[i]);
      auto semicolon = P.parse_token();
      CHECK_EQ(semicolon.get_punctuator(), punctuator_semicolon);
    }
  }

  TEST_CASE("Basic multiplicative expressions are parsed correctly")
  {
    StringExpressionParser P("++a*b->m*c++; (uint32_t)x*y*(uint32_t)z;");
    P.get_typeset().register_typedef(
      "uint32_t", {P.get_typeset().get_int_ptr()}
    );

    std::vector<std::string> goal_strings = {
      "(((++a) * b->m) * (c++))", "(((int)(x) * y) * (int)(z))"
    };

    for(size_t i = 0; i < goal_strings.size(); i++)
    {
      auto expr = P.parse_multiplicative_expression();
      REQUIRE_UNARY(expr);
      CHECK_EQ(expr->to_string(), goal_strings[i]);
      auto semicolon = P.parse_token();
      CHECK_EQ(semicolon.get_punctuator(), punctuator_semicolon);
    }
  }

  TEST_CASE("Basic arithmetic expressions are parsed correctly")
  {
    StringExpressionParser P(
      "(uint32_t)x & y + z * 5 / *ptr--; x-- ? x : y->z++;"
      "x += y->sw ? a : b++;"
      "(x, *y) <<= z--;"
      "arr[35] >>= 1 > 3;"
      "arr[0] = T(arr[1], 0x12);"
    );
    P.get_typeset().register_typedef(
      "uint32_t", {P.get_typeset().get_int_ptr()}
    );

    std::vector<std::string> goal_strings = {
      "((int)(x) & (y + ((z * 5) / (*(ptr--)))))",
      "((x--) ? x : (y->z++))",
      "(x += (y->sw ? a : (b++)))",
      "((x , (*y)) <<= (z--))",
      "(arr[35] >>= (1 > 3))",
      "(arr[0] = (T)(arr[1], 18))"
    };

    for(size_t i = 0; i < goal_strings.size(); i++)
    {
      auto expr = P.parse_expression();
      REQUIRE_UNARY(expr);
      CHECK_EQ(expr->to_string(), goal_strings[i]);
      auto semicolon = P.parse_token();
      CHECK_EQ(semicolon.get_punctuator(), punctuator_semicolon);
    }
  }

  TEST_CASE("Initializers are parsed correctly")
  {
    StringExpressionParser P(
      "x = y; {1, 2, 3}; {.member = 5, 6, 7}; {.member = 5+7, x+=2};"
    );
    P.get_typeset().register_typedef(
      "uint32_t", {P.get_typeset().get_int_ptr()}
    );

    std::vector<std::string> goal_strings = {
      "(x = y)", "{1, 2, 3, }", "{.member = 5, 6, 7, }",
      "{.member = (5 + 7), (x += 2), }"
    };

    for(size_t i = 0; i < goal_strings.size(); i++)
    {
      auto expr = P.parse_initializer();
      REQUIRE_UNARY(expr);
      CHECK_EQ(expr->to_string(), goal_strings[i]);
      auto semicolon = P.parse_token();
      CHECK_EQ(semicolon.get_punctuator(), punctuator_semicolon);
    }
  }

  TEST_SUITE_END();

}

#endif
