#ifndef SIMPLEC_EXPRESSION_PRINTING_TESTS_INCLUDED
#define SIMPLEC_EXPRESSION_PRINTING_TESTS_INCLUDED

#include <simplec/parser/expression/expression.hpp>
#include <simplec/parser/expression/value.hpp>
#include <simplec/parser/expression/identifier.hpp>
#include <simplec/parser/expression/generic_selection.hpp>
#include <simplec/parser/expression/binary_expression.hpp>
#include <simplec/parser/expression/binary_expression/arithmetic_expr.hpp>
#include <simplec/parser/expression/binary_expression/bitwise_expr.hpp>
#include <simplec/parser/expression/binary_expression/array_subscript.hpp>
#include <simplec/parser/expression/binary_expression/logical_expr.hpp>
#include <simplec/parser/expression/binary_expression/assignment_expr.hpp>
#include <simplec/parser/expression/unary_expression.hpp>
#include <simplec/parser/expression/unary_expression/unary_arith_expr.hpp>
#include <simplec/parser/expression/unary_expression/pointer_expression.hpp>
#include <simplec/parser/expression/unary_expression/logical_not.hpp>
#include <simplec/parser/expression/unary_expression/bitwise_not.hpp>
#include <simplec/parser/expression/unary_expression/cast_expression.hpp>
#include <simplec/parser/expression/function_call.hpp>
#include <simplec/parser/expression/member_access.hpp>
#include <simplec/parser/expression/ternary_expression.hpp>
#include <simplec/parser/types/fundamental_type_set.hpp>

#include <simplec/utility/false_share.hpp>

#include <memory>

namespace simplec
{

  TEST_SUITE_BEGIN("Expression printing tests");

  TEST_CASE("Basic arithmetic expressions construct and print properly")
  {
    FundamentalTypeSet F = FundamentalTypeSet::get_basic();

    AdditionExpression A(
      std::make_unique<FloatValue>(7.3, F),
      std::make_unique<FloatValue>(3.14, F),
      F
    );

    CHECK_EQ(A.to_string(), "(7.3f + 3.14f)");
    CHECK_EQ(A.get_raw_type(), F.get_float_ptr());

    AdditionExpression A2(
      std::make_unique<SignedIntegerValue>(352, F.get_int_ptr()),
      std::make_unique<DoubleValue>(3.14159, F),
      F
    );

    CHECK_EQ(A2.to_string(), "(352 + 3.14159)");
    CHECK_EQ(A2.get_raw_type(), F.get_double_ptr());


    DivisionExpression D(
      A.clone(),
      A2.clone(),
      F
    );

    CHECK_EQ(D.to_string(), "((7.3f + 3.14f) / (352 + 3.14159))");
    CHECK_EQ(D.get_raw_type(), F.get_double_ptr());

    BitwiseAndExpression BA(
      std::make_unique<SignedIntegerValue>(128, F.get_char_ptr()),
      std::make_unique<SignedIntegerValue>(263, F.get_long_ptr()),
      F
    );

    CHECK_EQ(BA.to_string(), "(128 & 263)");
    CHECK_EQ(BA.get_raw_type(), F.get_long_ptr());

    PointerType long_double_ptr_type{F.get_long_double_ptr()};

    LogicalNotExpression N(
      std::make_unique<Identifier>("ptr", false_share(long_double_ptr_type)), F
    );

    CHECK_EQ(N.to_string(), "(!ptr)");
    CHECK_EQ(N.get_raw_type(), F.get_int_ptr());

    PostfixIncrementExpression P(
      std::make_unique<Identifier>("C", F.get_long_ptr()), F
    );

    CHECK_EQ(P.to_string(), "(C++)");
    CHECK_EQ(P.get_raw_type(), F.get_long_ptr());

    CastExpression C(
      N.clone(),
      F.get_double_ptr()
    );

    CHECK_EQ(C.to_string(), "(double)((!ptr))");
    CHECK_EQ(C.get_raw_type(), F.get_double_ptr());

    LEQExpression L(
      BA.clone(),
      P.clone(),
      F
    );

    CHECK_EQ(L.to_string(), "((128 & 263) <= (C++))");
    CHECK_EQ(L.get_raw_type(), F.get_int_ptr());

  }

  TEST_CASE("Basic pointer/struct/union expressions construct and print")
  {
    FundamentalTypeSet F = FundamentalTypeSet::get_basic();

    StructureType st {{{"x", F.get_int_ptr()}, {"y", F.get_double_ptr()}}};
    PointerType pt {false_share(st)};

    PointerType long_double_ptr_type{F.get_long_double_ptr()};

    ArraySubscript S(
      std::make_unique<Identifier>("x", false_share(pt)),
      std::make_unique<SignedIntegerValue>(15, F.get_int_ptr())
    );

    CHECK_EQ(S.to_string(), "x[15]");
    CHECK_EQ(S.get_raw_type(), false_share(st));

    IndirectionExpression I(
      std::make_unique<Identifier>("ptr", false_share(pt))
    );

    CHECK_EQ(I.to_string(), "(*ptr)");
    CHECK_EQ(I.get_raw_type(), false_share(st));

    PointerMemberAccess P(
      std::make_unique<Identifier>("ptr", false_share(pt)),
      {"x", nullptr}
    );

    CHECK_EQ(P.to_string(), "ptr->x");
    CHECK_EQ(P.get_raw_type(), F.get_int_ptr());

    DirectMemberAccess D(
      std::make_unique<Identifier>("st", false_share(st)),
      {"y", nullptr}
    );

    CHECK_EQ(D.to_string(), "st.y");
    CHECK_EQ(D.get_raw_type(), F.get_double_ptr());
  }

  TEST_SUITE_END();

}

#endif
