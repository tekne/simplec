#ifndef SIMPLEC_TYPE_TESTS_INCLUDED
#define SIMPLEC_TYPE_TESTS_INCLUDED

#include <simplec/parser/types.hpp>
#include <simplec/utility/false_share.hpp>

namespace simplec
{

  TEST_SUITE_BEGIN("Parser type tests");

  TEST_CASE("Integers are ordered properly by conversion rank")
  {
    FundamentalTypeSet F{FundamentalTypeSet::get_basic()};
    CHECK_LT(F.get_bool(), F.get_int());
    CHECK_LT(F.get_short(), F.get_int());
    CHECK_LT(F.get_int(), F.get_long());
    CHECK_GE(F.get_long_long(), F.get_unsigned_long_long());
  }

  TEST_CASE("Type qualification operations work")
  {
    TypeQualification q{};
    CHECK_UNARY(q.is_valid());
    CHECK_UNARY_FALSE(q.is_const());
    CHECK_UNARY_FALSE(q.is_volatile());
    CHECK_UNARY_FALSE(q.is_restrict());
    CHECK_UNARY_FALSE(q.is_invalid());

    q |= const_qualified;
    CHECK_UNARY(q.is_valid());
    CHECK_UNARY(q.is_const());
    CHECK_UNARY_FALSE(q.is_volatile());
    CHECK_UNARY_FALSE(q.is_restrict());
    CHECK_UNARY_FALSE(q.is_invalid());

    q |= invalid_qualified;
    CHECK_UNARY_FALSE(q.is_valid());
    CHECK_UNARY(q.is_const());
    CHECK_UNARY_FALSE(q.is_volatile());
    CHECK_UNARY_FALSE(q.is_restrict());
    CHECK_UNARY(q.is_invalid());

    q.set_invalid();
    CHECK_UNARY_FALSE(q.is_valid());
    CHECK_UNARY(q.is_const());
    CHECK_UNARY_FALSE(q.is_volatile());
    CHECK_UNARY_FALSE(q.is_restrict());
    CHECK_UNARY(q.is_invalid());

    q.set_valid();
    CHECK_UNARY(q.is_valid());
    CHECK_UNARY(q.is_const());
    CHECK_UNARY_FALSE(q.is_volatile());
    CHECK_UNARY_FALSE(q.is_restrict());
    CHECK_UNARY_FALSE(q.is_invalid());

    q.set_invalid();
    CHECK_UNARY_FALSE(q.is_valid());
    CHECK_UNARY(q.is_const());
    CHECK_UNARY_FALSE(q.is_volatile());
    CHECK_UNARY_FALSE(q.is_restrict());
    CHECK_UNARY(q.is_invalid());
  }

  TEST_CASE("Types construct and serialize correctly")
  {
    FloatingType float_type(32);
    CHECK_EQ(float_type.to_string(), "float");
    CHECK_EQ(float_type.to_imaginary().to_string(), "_Imaginary float");
    CHECK_EQ(float_type.to_complex().to_string(), "_Complex float");

    FloatingType double_type(64);
    CHECK_EQ(double_type.to_string(), "double");
    CHECK_EQ(double_type.to_imaginary().to_string(), "_Imaginary double");
    CHECK_EQ(double_type.to_complex().to_string(), "_Complex double");

    FloatingType efloat_type(128);
    CHECK_EQ(efloat_type.to_string(), "__f128");
    CHECK_EQ(efloat_type.to_imaginary().to_string(), "_Imaginary __f128");
    CHECK_EQ(efloat_type.to_complex().to_string(), "_Complex __f128");

    NamedIntegerType int_type("int", 32);
    CHECK_EQ(int_type.to_string(), "int");
    CHECK_EQ(int_type.to_unsigned().to_string(), "unsigned int");
    ExtendedIntegerType eint_type(32);
    CHECK_EQ(eint_type.to_string(), "__i32");
    CHECK_EQ(eint_type.to_unsigned().to_string(), "__u32");

    PointerType ptr_to_int(false_share(int_type));
    CHECK_EQ(ptr_to_int.to_string(), "int*");
    CHECK_EQ(ptr_to_int.base_type().to_string(), "int");

    PointerType ptr_to_const_int({false_share(int_type), const_qualified});
    CHECK_EQ (ptr_to_const_int.to_string(), "int const*");

    PointerType void_pointer(nullptr);
    CHECK_EQ(void_pointer.to_string(), "void*");

    PointerType const_void_pointer({nullptr, const_qualified});
    CHECK_EQ(const_void_pointer.to_string(), "void const*");

    VectorType int_vector(false_share(int_type), 8);
    CHECK_EQ(int_vector.to_string(), "int<8>");
    CHECK_EQ(int_vector.base_type()->to_string(), "int");

    VectorType ptr_vector(false_share(ptr_to_int), 16);
    CHECK_EQ(ptr_vector.to_string(), "int*<16>");

    StructureType basic_struct(
      {{"x", {false_share(int_type)}}, {"y", {false_share(int_vector)}}}, "z"
    );
    CHECK_EQ(basic_struct.to_string(), "struct z {int x; int<8> y;}");

    FunctionType basic_function(
      {false_share(int_type)},
      {{false_share(int_type)}, {false_share(ptr_to_int)}}
    );
    CHECK_EQ(basic_function.to_string(), "((int, int*) -> int)");

    UnionType basic_union(
      {{"p", {false_share(ptr_to_int)}},
      {"yp", {false_share(ptr_vector)}}}, "u"
    );
    CHECK_EQ(basic_union.to_string(), "union u {int* p; int*<16> yp;}");

    StructureType const_member(
      {{"x", {false_share(int_type), const_qualified}}}
    );
    CHECK_EQ(const_member.to_string(), "struct {int const x;}");

    FunctionType const_function(
      {false_share(ptr_to_const_int), volatile_qualified},
      {{false_share(ptr_to_int), const_qualified}}
    );
    CHECK_EQ(const_function.to_string(),
      "((int* const) -> int const* volatile)"
    );

    FunctionType void_function(
      {nullptr}, {{false_share(ptr_to_int)}}
    );
    CHECK_EQ(void_function.to_string(), "((int*) -> void)");
  }

  TEST_SUITE_END();

};

#endif
