#ifndef SIMPLEC_PREPROCESSOR_UTILS_INCLUDED
#define SIMPLEC_PREPROCESSOR_UTILS_INCLUDED

#include <vector>
#include <string>

namespace simplec
{

  std::vector<std::string> get_raw_tokens(const char* str);
  std::vector<std::string> get_raw_tokens(const std::string& str);

}

#endif
