#ifndef SIMPLEC_PREPROCESSOR_STATE_INCLUDED
#define SIMPLEC_PREPROCESSOR_STATE_INCLUDED

#include <unordered_map>
#include <string>
#include <istream>
#include <ostream>
#include <utility>
#include <vector>
#include <iterator>
#include <cassert>
#include <exception>
#include <algorithm>
#include <deque>

#include <simplec/preprocessor/token.hpp>
#include <simplec/preprocessor/macro.hpp>
#include <iostream>

namespace simplec
{

  static const char* version = "\"0.0.0.1\"";

  class preprocessor_state
  {
    std::unordered_map<std::string, macro> macros;
    std::deque<preprocessor_file_source> opened_files;
    const preprocessor_file_source* current_file;
    std::string alternative_name;
  public:
    unsigned line;
    unsigned counter;

    unsigned get_line() const {return line;}
    preprocessor_state& set_line(unsigned l) {line = l; return *this;}
    unsigned get_counter() const {return counter;}
    preprocessor_state& set_counter(unsigned c) {counter = c; return *this;}

    const preprocessor_file_source* get_file_ptr() const {return current_file;}
    std::string get_file_name() const {
      if(current_file) return current_file->string();
      return "";
    }
    std::string get_file_str_ref() const {
      return "\"" + get_file_name() + "\"";
    }

    preprocessor_state& update_file_macro()
    {
      if(current_file || !alternative_name.empty())
      {
        if(alternative_name.empty())
        {
          macros["__FILE__"] = get_file_str_ref();
        }
        else
        {
          macros["__FILE__"] = alternative_name;
        }
      }
      else
      {
        macros.erase("__FILE__");
      }
      return *this;
    }

    preprocessor_state& enter_file (preprocessor_file_source pt)
    {
      if(opened_files.empty())
      {
        macros["__BASE_FILE__"] = "\"" + pt.string() + "\"";
      }
      alternative_name.clear();
      opened_files.push_back(std::move(pt));
      current_file = &opened_files.back();
      line = 0;
      update_file_macro();
      return *this;
    }

    preprocessor_state& enter_file(const std::string& fl)
    {
      if(opened_files.empty())
      {
        macros["__BASE_FILE__"] = "\"" + fl + "\"";
      }
      if(current_file)
      {
        opened_files.emplace_back(fl, *current_file, line);
      }
      else
      {
        opened_files.emplace_back(fl);
      }
      alternative_name.clear();
      current_file = &opened_files.back();
      line = 0;
      update_file_macro();
      return *this;
    }

    preprocessor_state& set_file_name(std::string an)
    {
      alternative_name = std::move(an);
      update_file_macro();
      return *this;
    }

    preprocessor_state& exit_current_file()
    {
      if(!current_file) throw std::runtime_error("No file opened");
      line = current_file->line();
      current_file = current_file->source();
      alternative_name.clear();
      update_file_macro();
      return *this;
    }

    preprocessor_state()
    : macros {
              {"__LINE__", macro(std::to_string(0))},
              {"__COUNTER__", macro(std::to_string(0))},
              {"__DATE__", macro("")},
              {"__TIME__", macro("")},
              {"__STDC__", macro("1")},
              {"__STDC_VERSION__", macro("201112L")},
              {"__VERSION__", macro(version)}
            },
      opened_files(),
      current_file(nullptr),
      alternative_name(),
      line(0),
      counter(0)
    {}

    preprocessor_state(
      preprocessor_file_source fl,
      std::string date = "",
      std::string tm = "",
      unsigned ln = 0,
      unsigned cnt = 0
    )
    : macros {
              {"__LINE__", macro(std::to_string(ln))},
              {"__COUNTER__", macro(std::to_string(cnt))},
              {"__DATE__", macro(date)},
              {"__TIME__", macro(tm)},
              {"__STDC__", macro("1")},
              {"__STDC_VERSION__", macro("201112L")},
              {"__VERSION__", macro(version)}
            },
      opened_files(),
      current_file(nullptr),
      counter(cnt)
    {
      enter_file(fl);
      line = ln;
    }

    auto begin_macros() {return macros.begin();}
    auto end_macros() {return macros.end();}
    auto begin_macros() const {return macros.begin();}
    auto end_macros() const {return macros.end();}
    auto cbegin_macros() const {return macros.cbegin();}
    auto cend_macros() const {return macros.cend();}

    auto get_macro_iter(const preprocessor_token& name)
    {
      using std::to_string;
      if(name == "__COUNTER__") macros["__COUNTER__"] = to_string(counter++);
      else if(name == "__LINE__") {
        set_line(name.get_line());
        macros["__LINE__"] = to_string(get_line());
      }

      return macros.find(name.string());
    }

    auto get_macro_iter(const std::string& name)
    {
      using std::to_string;
      if(name == "__COUNTER__") macros["__COUNTER__"] = to_string(counter++);
      else if(name == "__LINE__") {
        macros["__LINE__"] = to_string(get_line());
      }

      return macros.find(name);
    }

    template<class T>
    macro* get_macro_ptr(const T& name)
    {
      auto iter = get_macro_iter(name);
      if(iter == end_macros()) return nullptr;
      return &iter->second;
    }

    preprocessor_state& set_macro(const std::string& name, macro value)
    {
      macros[name] = std::move(value);
      return *this;
    }

    preprocessor_state& undef(const std::string& name)
    {
      macros.erase(name);
      return *this;
    }

    preprocessor_state& undef(const preprocessor_token& tok)
    {
      undef(tok.string());
      return *this;
    }

    bool is_defined(const std::string& name) const {
      return macros.find(name) != macros.end();
    }

    bool is_defined(const preprocessor_token& tok) const {
      return is_defined(tok.string());
    }

  };

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_PREPROCESSOR_STATE_TEST_INCLUDED
#define SIMPLEC_PREPROCESSOR_STATE_TEST_INCLUDED

namespace simplec
{

  TEST_CASE("Preprocessor state constructor and basic operations")
  {
    preprocessor_state P {
      preprocessor_file_source{"base.cpp"},
      "Jan 09 1953",
      "23:59:01",
      25,
      32
    };

    // Check constructor appropriately sets line and counter
    CHECK_EQ(P.get_line(), 25);
    CHECK_EQ(P.get_counter(), 32);

    // Check predefined macros are set appropriately
    CHECK_EQ(P.get_macro_iter("__FILE__")->first, "__FILE__");
    CHECK_EQ(P.get_macro_ptr("__FILE__")->resolve_to_str(), "\"base.cpp\"");
    CHECK_EQ(P.get_macro_ptr(
      "__BASE_FILE__")->resolve_to_str(),
      "\"base.cpp\""
    );
    CHECK_EQ(P.get_macro_ptr("__LINE__")->resolve_to_str(), "25");
    P.set_line(123);
    CHECK_EQ(P.get_macro_ptr("__LINE__")->resolve_to_str(), "123");
    CHECK_EQ(P.get_macro_ptr("__STDC__")->resolve_to_str(), "1");
    CHECK_EQ(P.get_macro_ptr("__STDC_VERSION__")->resolve_to_str(), "201112L");
    CHECK_EQ(
      P.get_macro_ptr("__VERSION__")->resolve_to_str(),
      std::string(version)
    );

    // Check that counter is defined but not incremented on check
    CHECK_UNARY(P.is_defined("__COUNTER__"));
    CHECK_EQ(P.get_macro_ptr("__COUNTER__")->resolve_to_str(), "32");

    // Check that counter increments on resolution
    CHECK_EQ(P.get_macro_ptr("__COUNTER__")->resolve_to_str(), "33");

    // Check undefined macros return the end iterator
    CHECK_UNARY_FALSE(P.is_defined("test"));
    CHECK_EQ(P.get_macro_iter("test"), P.end_macros());

    // Check macro definition works correctly
    P.set_macro("test", "best");
    CHECK_EQ(P.get_macro_iter("test")->first, "test");
    CHECK_EQ(P.get_macro_ptr("test")->resolve_to_str(), "best");
    P.set_macro("test", "rest");
    CHECK_EQ(P.get_macro_ptr("test")->resolve_to_str(), "rest");

    // Check functional macro definition works correctly
    P.set_macro("square", {{"X", "*", "X"}, {"X"}});
    CHECK_EQ(P.get_macro_iter("square")->first, "square");
    CHECK_EQ(P.get_macro_ptr("square")->resolve_to_str({"1.5"}), "1.5 * 1.5");

    // Check file stack works correctly
    P.enter_file("string.h");
    CHECK_EQ(P.get_macro_ptr("__FILE__")->resolve_to_str(), "\"string.h\"");
    CHECK_EQ(P.get_macro_ptr(
      "__BASE_FILE__")->resolve_to_str(),
      "\"base.cpp\""
    );
    CHECK_EQ(P.get_line(), 0);
    P.exit_current_file();
    CHECK_EQ(P.get_macro_ptr("__FILE__")->resolve_to_str(), "\"base.cpp\"");
    CHECK_EQ(P.get_macro_ptr(
      "__BASE_FILE__")->resolve_to_str(),
      "\"base.cpp\""
    );
    CHECK_EQ(P.get_line(), 123);
  }

}

#endif
#endif
