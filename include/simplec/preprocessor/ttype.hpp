#ifndef SIMPLEC_TTYPES_INCLUDED
#define SIMPLEC_TTYPES_INCLUDED

#include <simplec/preprocessor/token.hpp>

namespace simplec {

  bool is_identifier(const std::string& s);
  bool is_char_literal(const std::string& s);
  bool is_string_literal(const std::string& s);
  bool is_preprocessing_number(const std::string& s);

  inline bool is_identifier(const preprocessor_token& t) {
    return t.type() == tok_id || is_identifier(t.string());
  }

  inline bool is_string_literal(const preprocessor_token& t) {
    return t.type() == tok_str || is_string_literal(t.string());
  }

  inline bool is_char_literal(const preprocessor_token& t) {
    return t.type() == tok_char_literal || is_char_literal(t.string());
  }

  inline bool is_preprocessing_number(const preprocessor_token& t) {
    return t.type() == tok_num || is_preprocessing_number(t.string());
  }

  inline int get_trigram(int ch)
  {
    switch(ch)
    {
      case '=': return '#';
      case '(': return '[';
      case ')': return ']';
      case '<': return '{';
      case '!': return '|';
      case '>': return '}';
      case '-': return '~';
      case '\'': return '^';
      case '/': return '\\';
      default: return -1;
    }
  }

  //TODO: refactor to use same function as parser
  inline bool is_int_suffix(const char* sfx)
  {
    switch(*(sfx++))
    {
      case 'u': case 'U':
        switch(*(sfx++))
        {
          case '\0': return true;
          case 'l': case 'L':
          switch(*(sfx++))
          {
            case 'l': case 'L': return !(*sfx);
            case '\0': return true;
            default: return false;
          }
          default: return false;
        }
      case 'l': case 'L':
        switch(*(sfx++))
        {
          case '\0': return true;
          case 'u': case 'U': return !(*sfx);
          case 'l': case 'L':
          switch(*(sfx++))
          {
            case 'u': case 'U': return !(*sfx);
            case '\0': return true;
            default: return false;
          }
          default: return false;
        }
      case '\0': return true;
      default: return false;
    }
  }

}


#ifdef DOCTEST_LIBRARY_INCLUDED
#include <algorithm>

namespace simplec
{
  TEST_CASE("C identifiers are identified correctly")
  {
    CHECK_UNARY(is_identifier("x"));
    CHECK_UNARY_FALSE(is_identifier("1x"));
    CHECK_UNARY(is_identifier("this_is_quite_a_long_name"));
    CHECK_UNARY_FALSE(is_identifier("half_a_name\""));
  }

  TEST_CASE("String literals are identified correctly")
  {
    CHECK_UNARY(is_string_literal("\"string literal\""));
    CHECK_UNARY_FALSE(is_string_literal("123"));
    CHECK_UNARY(is_string_literal("\"1 interesting literal\n\n\\n\""));
    CHECK_UNARY_FALSE(is_string_literal("hello\""));
  }

  TEST_CASE("Character literals are identified correctly")
  {
    CHECK_UNARY(is_char_literal("\'x\'"));
    CHECK_UNARY_FALSE(is_char_literal("123"));
    CHECK_UNARY(is_char_literal("\'1 interesting literal\n\n\\n\'"));
    CHECK_UNARY_FALSE(is_char_literal("hello\'"));
  }

  TEST_CASE("Preprocessing numbers are identified correctly")
  {
    CHECK_UNARY(is_preprocessing_number("123"));
    CHECK_UNARY_FALSE(is_preprocessing_number("ABCD"));
    CHECK_UNARY(is_preprocessing_number("4\'1interestingnumber"));
    CHECK_UNARY_FALSE(is_preprocessing_number("123 "));
  }

}

#endif

#endif
