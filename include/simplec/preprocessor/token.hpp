#ifndef SIMPLEC_PREPROCESSOR_TOKEN_INCLUDED
#define SIMPLEC_PREPROCESSOR_TOKEN_INCLUDED

#include <utility>
#include <string>
#include <cstddef>

#include <simplec/preprocessor/file_handling.hpp>
#include <simplec/lexer/ttype.hpp>


namespace simplec
{

  class PreprocessorToken;

  struct token_metadata
  {
    const preprocessor_file_source* src;
    unsigned line;
    unsigned pos;

    token_metadata(
      const preprocessor_file_source* s = nullptr,
      unsigned l = 0,
      unsigned p = 0
    ): src(s), line(l), pos(p) {}
  };

  class preprocessor_token
  {
    std::string value;
    token_metadata md;
    preprocessor_token_type typ;
  public:

    bool operator==(const std::string& vl) const {return value == vl;}
    bool operator!=(const std::string& vl) const {return value != vl;}
    bool operator>=(const std::string& vl) const {return value >= vl;}
    bool operator<=(const std::string& vl) const {return value <= vl;}
    bool operator>(const std::string& vl) const {return value > vl;}
    bool operator<(const std::string& vl) const {return value < vl;}
    bool operator==(const char* vl) const {return value == vl;}
    bool operator!=(const char* vl) const {return value != vl;}
    bool operator>=(const char* vl) const {return value >= vl;}
    bool operator<=(const char* vl) const {return value <= vl;}
    bool operator>(const char* vl) const {return value > vl;}
    bool operator<(const char* vl) const {return value < vl;}
    bool empty() const {return value.empty();}

    bool operator==(const preprocessor_token& vl) const {
      return value == vl.string();
    }
    bool operator!=(const preprocessor_token& vl) const {
      return value != vl.string();
    }

    bool is_valid_type() const {
      return typ <= 8;
    }

    const char& front() const {return value.front();}
    const char& back() const {return value.back();}

    const std::string& string() const {return value;}
    preprocessor_token_type type() const {return typ;}
    token_metadata metadata() const {return md;}

    unsigned get_line() const {return md.line;}
    unsigned get_pos() const {return md.pos;}

    const preprocessor_file_source* get_src() const {return md.src;}

    std::string strip() {
      typ = tok_unknown;
      return std::move(value);
    }

    std::string unsafe_strip() {
      return std::move(value);
    }

    preprocessor_token& operator+=(const preprocessor_token& t)
    {
      value += t.string();
      if(t.type() == typ)
      {
        // num ## num = num, id ## id = id, other ## other = ??
        if(typ != tok_num && typ != tok_id) typ = tok_unknown;
      }
      else
      {
        typ = tok_unknown;
      }
      return *this;
    }

    preprocessor_token operator+(const preprocessor_token& t) const
    {
      preprocessor_token n = *this;
      return n += t;
    }

    preprocessor_token& stringify()
    {
      if(typ == tok_str) {
        value.pop_back();
        value = "\"\\" + value + "\\\"\"";
      }
      else
      {
        value = "\"" + value + "\"";
        typ = tok_str;
      }
      return *this;
    }

    preprocessor_token(
      std::string v,
      token_metadata m,
      preprocessor_token_type t = tok_unknown
    ): value(v), md(m), typ(t) {}

    preprocessor_token(
      std::string v = "",
      const preprocessor_file_source* s = nullptr,
      unsigned l = 0,
      unsigned p = 0,
      preprocessor_token_type t = tok_unknown
    ): preprocessor_token(std::move(v), token_metadata{s, l, p}, t) {}

  };

  inline static std::string strip_token(preprocessor_token tok) {
    return tok.strip();
  }

  inline static std::vector<std::string> strip_tokens(
    const std::vector<preprocessor_token>& v
  )
  {
    std::vector<std::string> result;
    result.reserve(v.size());
    for(const auto& s: v) result.push_back(s.string());
    return result;
  }

  inline static std::vector<std::string> strip_tokens(
    std::vector<preprocessor_token>&& v
  )
  {
    std::vector<std::string> result;
    result.reserve(v.size());
    for(auto& s: v) result.push_back(s.strip());
    v.clear();
    return result;
  }

}

#endif
