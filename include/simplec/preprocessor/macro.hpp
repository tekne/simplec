#ifndef SIMPLEC_PREPROCESSOR_MACRO_INCLUDED
#define SIMPLEC_PREPROCESSOR_MACRO_INCLUDED

#include <unordered_map>
#include <string>
#include <istream>
#include <ostream>
#include <utility>
#include <vector>
#include <iterator>
#include <cassert>
#include <exception>
#include <algorithm>
#include <deque>

#include <simplec/preprocessor/token.hpp>
#include <simplec/preprocessor/utils.hpp>

namespace simplec
{

  enum macro_type: char {token_string, function_like, variadic, generic};

  class macro
  {
    std::vector<std::string> tokens;
    unsigned nargs;
    macro_type type;
    size_t defined_at;
    const preprocessor_file_source* source;

    static int read_argnum(const std::string& s)
    {
      static const char ascii_escape = 27;
      if((s.size() != 1 + sizeof(unsigned)) || s.front() != ascii_escape)
        return -1;
      else return *(reinterpret_cast<const unsigned*>(s.c_str() + 1));
    }

    static std::string write_argnum(unsigned num)
    {
      static const char ascii_escape = 27;
      std::string result (1 + sizeof(unsigned), ascii_escape);
      const char* ptr = reinterpret_cast<const char*>(&num);
      for(int i = 0; i < sizeof(unsigned); i++)
      {
        result[i + 1] = ptr[i];
      }
      return result;
    }

  public:

    macro(
      std::vector<std::string> tk,
      unsigned ng = 0,
      macro_type t = token_string,
      size_t def = 0,
      const preprocessor_file_source* src = nullptr
    ): tokens(std::move(tk)), nargs(ng), type(t), defined_at(def), source(src)
    {
      assert((t != token_string) || (!nargs));
    }

    macro(
      const std::string& tk,
      size_t def = 0,
      const preprocessor_file_source* src = nullptr
    ):
    tokens(get_raw_tokens(tk)), nargs(0), type(token_string),
    defined_at(def), source(src)
    {
      if(!tokens.empty() && tokens.back().empty())
        tokens.pop_back(); // Remove EOF token
    }

    macro(
      const char* tk,
      size_t def = 0,
      const preprocessor_file_source* src = nullptr
    ):
    tokens(get_raw_tokens(tk)), nargs(0), type(token_string),
    defined_at(def), source(src)
    {
      if(!tokens.empty() && tokens.back().empty())
        tokens.pop_back(); // Remove EOF token
    }

    macro(
      std::vector<std::string>,
      std::vector<std::string>,
      bool = false, size_t def = 0, const preprocessor_file_source* src = nullptr
    );

    macro(
      const std::string& tk,
      std::vector<std::string> args,
      bool varargs = false,
      size_t def = 0,
      const preprocessor_file_source* src = nullptr
    ):
    macro(get_raw_tokens(tk), args, varargs, def, src)
    {
      tokens.pop_back(); // Remove EOF token
    }

    macro(): tokens(), nargs(0), type(token_string) {}

    size_t num_args() const {return nargs;}
    size_t definition() const {return defined_at;}
    const preprocessor_file_source* source_file() const {return source;}
    bool is_function_like() const {return type != token_string;}
    bool is_variadic() const {return type == variadic;}

    std::vector<std::string> resolve();
    std::vector<std::string>& append(std::vector<std::string>&);
    std::vector<std::string> resolve(const std::vector<std::string>&);
    std::vector<std::string>& append(
      const std::vector<std::string>&, std::vector<std::string>&
    );

    std::string resolve_to_str();
    std::string resolve_to_str(const std::vector<std::string>& args);
  };

}

#endif
