#ifndef SIMPLEC_PREPROCESSOR_TOKEN_UTILS_INCLUDED
#define SIMPLEC_PREPROCESSOR_TOKEN_UTILS_INCLUDED

#include <string>
#include <vector>

namespace simplec {

  

  std::vector<std::vector<std::string>::iterator> split_vector(
    std::vector<std::string>& input, std::string delim
  )
  {
    std::vector<std::vector<std::string>::iterator> result;
    auto current = input.begin();
    auto end = input.end();
    while(current != end)
    {
      if(*current == delim)
      {
        result.push_back(current);
      }
      current++;
    }
    result.push_back(current);
    return result;
  }

  std::vector<std::vector<std::string>::const_iterator> split_vector(
    const std::vector<std::string>& input, std::string delim
  )
  {
    std::vector<std::vector<std::string>::const_iterator> result;
    auto current = input.begin();
    auto end = input.end();
    while(current != end)
    {
      if(*current == delim)
      {
        result.push_back(current);
      }
      current++;
    }
    result.push_back(current);
    return result;
  }
}

#endif
