#ifndef SIMPLEC_SEARCH_PATH_INCLUDED
#define SIMPLEC_SEARCH_PATH_INCLUDED

#include <vector>
#include <fstream>
#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>

namespace simplec
{

  class preprocessor_file_source
  {
    boost::filesystem::path path;
    const preprocessor_file_source* src;
    size_t ln;

  public:

    preprocessor_file_source(
      boost::filesystem::path p
    ): path(p), src(nullptr), ln(0) {}

    preprocessor_file_source(
      boost::filesystem::path p,
      const preprocessor_file_source& s,
      size_t l = 0
    ): path(p), src(&s), ln(l) {}

    bool is_root() const {return !src;}

    auto string() const {return path.string();}
    size_t line() const {return ln;}
    const preprocessor_file_source* source() const {return src;}
    auto parent() const {return path.parent_path();}

    preprocessor_file_source& set_source(const preprocessor_file_source& s)
    {
      src = &s;
      return *this;
    }

    preprocessor_file_source& set_line(size_t l)
    {
      ln = l;
      return *this;
    }

    std::ifstream open() const {
      return boost::filesystem::ifstream(path);
    }

  };

  class preprocessor_include_paths
  {
    std::vector<boost::filesystem::path> local_search_paths; // "file"
    std::vector<boost::filesystem::path> global_search_paths; // <file>
  public:

    preprocessor_include_paths(
      std::vector<boost::filesystem::path> gsp,
      std::vector<boost::filesystem::path> lsp = {}
    )
    : local_search_paths(std::move(lsp)),
      global_search_paths(std::move(gsp))
    {}

    preprocessor_include_paths()
    : local_search_paths(),
      global_search_paths()
    {}

    preprocessor_file_source get_local(
      const std::string& file,
      const preprocessor_file_source& src
    ) const;
    preprocessor_file_source get_local(
      const std::string& file
    ) const;
    preprocessor_file_source get_global(
      const std::string& file,
      const preprocessor_file_source& src
    ) const;
    preprocessor_file_source get_global(const std::string& file) const;
  };

}

#endif
