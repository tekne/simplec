#ifndef SIMPLEC_PREPROCESSOR_INCLUDED
#define SIMPLEC_PREPROCESSOR_INCLUDED

#include <simplec/preprocessor/token.hpp>
#include <simplec/preprocessor/file_handling.hpp>
#include <simplec/preprocessor/state.hpp>
#include <simplec/lexer.hpp>

#include <boost/container/small_vector.hpp>

namespace simplec
{

  class Preprocessor
  {
    // === Parameters ===
    static const size_t static_buffer_size = 5;

    // === Members ===

    // Tokenizer and buffer
    Lexer lexer;
    boost::container::small_vector<preprocessor_token, static_buffer_size>
      token_buffer;
    size_t bpos;

    // Macro system
    preprocessor_state macro_state;

    // File handling
    preprocessor_include_paths paths;
    std::istream* root_source;
    std::vector<std::ifstream> file_stack;

    // Conditional inclusion
    std::vector<bool> in_if;
    bool in_preprocessor;

    // Flags
    //TODO: add warning/error flags

    // === Private methods ===

    // Lexer adapters
    std::string get_raw_value();
    preprocessor_token get_raw_token();

    // Preprocessor directive handling
    Preprocessor& if_exclude_helper();
    Preprocessor& else_exclude_helper();
    Preprocessor& handle_directive();
    Preprocessor& handle_include();
    Preprocessor& handle_define();
    Preprocessor& handle_if();
    Preprocessor& handle_elif();
    Preprocessor& handle_else();
    Preprocessor& handle_endif();
    Preprocessor& handle_pragma();
    Preprocessor& handle_ifdef();
    Preprocessor& handle_ifndef();
    Preprocessor& handle_line();
    Preprocessor& handle_warning();
    Preprocessor& handle_error();
    Preprocessor& handle_undef();

    // Preprocessor expression parsing
    long long parse_num_expr();
    long long parse_delim_expr(std::string, bool);
    long long parse_line_expr() {return parse_delim_expr("\n", true);}
    long long parse_close_expr() {return parse_delim_expr(")", false);}

  public:

    // === Constructors ===
    Preprocessor(
      std::istream* src = nullptr,
      preprocessor_state m = {},
      preprocessor_include_paths p = {}
    )
    : lexer(src),
      token_buffer(),
      bpos(0),
      macro_state(std::move(m)),
      paths(std::move(p)),
      root_source(src),
      file_stack(),
      in_if(),
      in_preprocessor(false)
    {}

    Preprocessor(
      Lexer L,
      preprocessor_state m = {},
      preprocessor_include_paths p = {}
    )
    : lexer(std::move(L)),
      token_buffer(),
      bpos(0),
      macro_state(std::move(m)),
      paths(std::move(p)),
      root_source(lexer.get_src()),
      file_stack(),
      in_if(),
      in_preprocessor(false)
    {}

    // === Public methods ===

    // File management
    Preprocessor& open_local_file(const std::string&);
    Preprocessor& open_global_file(const std::string&);
    Preprocessor& open_path(preprocessor_file_source);
    Preprocessor& open(std::istream* str) {
      root_source = str;
      if(file_stack.empty()) lexer.open(root_source);
      return *this;
    }
    Preprocessor& open(std::istream& str) {
      return open(&str);
    }

    // State information
    unsigned& line() {return macro_state.line;}
    const unsigned& line() const {return macro_state.line;}
    unsigned get_line() const {return macro_state.get_line();}
    Preprocessor& set_line(unsigned l) {
      macro_state.set_line(l);
      return *this;
    }
    const preprocessor_file_source* get_file_ptr() {
      return macro_state.get_file_ptr();
    }

    // Token modification
    preprocessor_token tag_token(std::string v)
    {
      return preprocessor_token{
        std::move(v),
        get_file_ptr(),
        get_line(),
        lexer.get_pos(),
        lexer.get_current_type()
      };
    }

    // Token access
    // Get the next string, skipping whitespace and newlines
    preprocessor_token get_string();
    // Get the next character literal, skipping whitespace and newlines
    preprocessor_token get_char();
    // Get the next identifier, skipping whitespace and newlines
    preprocessor_token get_id();
    // Get the next punctuator, skipping whitespace and newlines
    preprocessor_token get_punc();
    // Get the next token. Does NOT skip newlines
    preprocessor_token get_token();
    // Get the next token, skipping newlines
    preprocessor_token get_stripped()
    {
      preprocessor_token tok = get_token();
      if(tok == "\n") return get_stripped();
      return tok;
    }

    std::vector<preprocessor_token> get_token_line(
      const std::string&,
      bool strict = true
    );

    // Stream status
    bool str_good() const
    {
      return !file_stack.empty() || lexer.good();
    }

    bool good() const
    {
      // Note that EOF tokens never fill the token buffer
      return (bpos < token_buffer.size()) || str_good();
    }

    // Stream manipulation
    Preprocessor& skip_line()
    {
      macro_state.line += lexer.get_all_offset() + 1;
      lexer.skip_line();
      return *this;
    }

  private:

    // Buffer manipulation
    preprocessor_token buffered_raw_token() {
      if(token_buffer.size() > bpos) return token_buffer[bpos++];
      token_buffer.clear();
      bpos = 0;
      return get_raw_token();
    }

    preprocessor_token buffered_return(preprocessor_token& tok)
    {
      using std::swap;

      if(!token_buffer.empty())
      {
        swap(token_buffer.back(), tok);
        return tok;
      }
      else
      {
        token_buffer.push_back(tok);
        return get_token();
      }
    }

    Preprocessor& clear_buffer()
    {
      token_buffer.clear();
      bpos = 0;
      return *this;
    }

    Preprocessor& prepend_to_buffer(preprocessor_token tok)
    {
      if(bpos == 0) {
        token_buffer.insert(token_buffer.begin(), std::move(tok));
      }
      else {
        token_buffer[--bpos] = std::move(tok);
      }
      return *this;
    }

  };

  class StringPreprocessor: public Preprocessor
  {
  public:
    std::istringstream sp;
    StringPreprocessor(std::string str): Preprocessor(), sp(std::move(str)) {
      this->open(sp);
    }
    StringPreprocessor(std::string str, preprocessor_file_source fn)
    : Preprocessor(nullptr, preprocessor_state{fn}),
      sp(std::move(str))
    {
      this->open(sp);
    }
    StringPreprocessor(std::string str, std::string fn)
    : StringPreprocessor(str, preprocessor_file_source{fn}) {}
  };

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_PREPROCESSOR_TESTS_INCLUDED
#define SIMPLEC_PREPROCESSOR_TESTS_INCLUDED

namespace simplec
{


  TEST_CASE("Basic preprocessor functionality")
  {
    StringPreprocessor P {"\
    int main()\n\
    {\n\
      return 0;\n\
    }\n"
    };

    std::vector<std::string> goal_tokenization = {
      "int", "main", "(", ")", "\n", "{", "\n",
      "return", "0", ";", "\n", "}", "\n", ""
    };
    std::vector<preprocessor_token> tokens {};

    do {
      tokens.push_back(P.get_token());
    } while(!tokens.back().empty());

    REQUIRE_EQ(tokens.size(), goal_tokenization.size());
    for(size_t i = 0; i < tokens.size(); i++)
      CHECK_EQ(tokens[i], goal_tokenization[i]);
  }

  TEST_CASE("Basic conditional checking functionality")
  {
    StringPreprocessor P {
      "#if 1 - 1\n"
      "  function(1);\n"
      "#else\n"
      "  function(0);\n"
      "#endif\n"
      "#if 1 + 1\n"
      "  function(1);\n"
      "#else\n"
      "  function(0);\n"
      "#endif\n"
      "#if (1 * 0) && (500 + 324535)\n"
      "  function(1);\n"
      "#else\n"
      "  function(0);\n"
      "#endif\n"
    };

    std::vector<std::string> goal_tokenization = {
      "function", "(", "0", ")", ";",
      "function", "(", "1", ")", ";",
      "function", "(", "0", ")", ";",
      ""
    };
    std::vector<preprocessor_token> tokens;

    do {
      tokens.push_back(P.get_stripped());
    } while(!tokens.back().empty());

    REQUIRE_EQ(goal_tokenization.size(), tokens.size());
    for(size_t i = 0; i < tokens.size(); i++)
    {
      CHECK_EQ(tokens[i].string(), goal_tokenization[i]);
    }
  }

  TEST_CASE("Basic definition checking functionality")
  {
    StringPreprocessor P {
      "#ifdef SYMBOL\n"
      "  function(1);\n"
      "#else\n"
      "  function(0);\n"
      "#endif\n"
      "#define SYMBOL\n"
      "#ifdef SYMBOL\n"
      "  function(1);\n"
      "#else\n"
      "  function(0);\n"
      "#endif\n"
      "#undef SYMBOL\n"
      "#ifdef SYMBOL\n"
      "  function(1);\n"
      "#else\n"
      "  function(0);\n"
      "#endif\n"
    };

    std::vector<std::string> goal_tokenization = {
      "function", "(", "0", ")", ";",
      "function", "(", "1", ")", ";",
      "function", "(", "0", ")", ";",
      ""
    };
    std::vector<preprocessor_token> tokens;

    do {
      tokens.push_back(P.get_stripped());
    } while(!tokens.back().empty());

    REQUIRE_EQ(goal_tokenization.size(), tokens.size());
    for(size_t i = 0; i < tokens.size(); i++)
    {
      CHECK_EQ(tokens[i].string(), goal_tokenization[i]);
    }
  }

  TEST_CASE("Basic macro definition and redefinition functionality")
  {
    StringPreprocessor P {
      "#define test_macro \"test value\"\n"
      "puts(test_macro);\n"
      "#define test_macro \"other test value\"\n"
      "puts(test_macro);\n"
    };

    std::vector<std::string> goal_tokenization = {
      "\n",
      "puts", "(", "\"test value\"", ")", ";", "\n", "\n",
      "puts", "(", "\"other test value\"", ")", ";", "\n", ""
    };
    std::vector<preprocessor_token> tokens;

    do {
      tokens.push_back(P.get_token());
    } while(!tokens.back().empty());

    REQUIRE_EQ(tokens.size(), goal_tokenization.size());
    for(size_t i = 0; i < tokens.size(); i++)
      CHECK_EQ(tokens[i].string(), goal_tokenization[i]);
  }

}

#endif
#endif
