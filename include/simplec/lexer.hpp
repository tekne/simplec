#ifndef SIMPLEC_TOKENIZER_INCLUDED
#define SIMPLEC_TOKENIZER_INCLUDED

#include <string>
#include <istream>
#include <sstream>
#include <cstddef>
#include <utility>

#include <iostream>

#include <boost/container/small_vector.hpp>

#include <simplec/lexer/ttype.hpp>
#include <simplec/preprocessor/state.hpp>

namespace simplec
{

  class Lexer
  {
    using const_iterator = std::vector<char>::const_iterator;
    static const size_t initial_buffer_size = 4096;

    std::vector<char> linebuffer;
    const_iterator end;
    const_iterator beginning;
    boost::container::small_vector<size_t, 5> line_escapes;
    std::istream* src;
    preprocessor_token_type prev_typ;

    Lexer& append_line();
    Lexer& getline();

    Lexer& refresh_buffer()
    {
      if(end == linebuffer.end())
      {
        getline();
        end = beginning = linebuffer.begin();
      }
      return *this;
    }

  public:

    Lexer(std::istream* s = nullptr)
    : linebuffer(),
      end(linebuffer.end()),
      beginning(linebuffer.begin()),
      line_escapes(),
      src(s),
      prev_typ(tok_unknown)
    {}

    Lexer(std::istream& str): Lexer(&str) {}

    Lexer& open(std::istream* s) {
      src = s;
      return *this;
    }

    Lexer& open(std::istream& s) {
      src = &s;
      return *this;
    }

    Lexer& close() {
      src = nullptr;
      return *this;
    }

    std::istream* get_src() {return src;}

    const_iterator get_identifier(const_iterator);
    const_iterator get_preprocessing_number(const_iterator);
    const_iterator get_char_literal(const_iterator);
    const_iterator get_string_literal(const_iterator);
    const_iterator get_punctuator(const_iterator);
    const_iterator skip_ws(const_iterator);
    const_iterator get_token(const_iterator);
    const_iterator get_hchar_literal(const_iterator);

    const_iterator get_next_identifier() {
      refresh_buffer();
      beginning = end;
      return end = get_identifier(end);
    }

    const_iterator get_next_preprocessing_number() {
      refresh_buffer();
      beginning = end;
      return end = get_preprocessing_number(end);
    }

    const_iterator get_next_char_literal() {
      refresh_buffer();
      beginning = end;
      return end = get_char_literal(end);
    }

    const_iterator get_next_string_literal() {
      refresh_buffer();
      beginning = end;
      return end = get_string_literal(end);
    }

    const_iterator get_next_punctuator() {
      refresh_buffer();
      beginning = end;
      return end = get_punctuator(end);
    }

    const_iterator skip_ws() {
      refresh_buffer();
      beginning = end;
      return end = skip_ws(end);
    }

    const_iterator get_next_token() {
      skip_ws();
      refresh_buffer();
      beginning = end;
      end = get_token(end);
      return end;
    }

    const_iterator get_next_hchar_literal() {
      refresh_buffer();
      beginning = end;
      return end = get_hchar_literal(end);
    }

    const_iterator begin_current() const {return beginning;}
    const_iterator end_current() const {return end;}
    const_iterator begin_buffer() const {return linebuffer.begin();}
    const_iterator end_buffer() const {return linebuffer.end();}

    bool token_read()
    {
      return beginning != end;
    }

    bool good() const
    {
      return src && src->good();
    }

    bool opened() const
    {
      return src;
    }

    unsigned get_offset()
    {
      unsigned r = 0;
      while(!line_escapes.empty())
      {
        if(end > line_escapes.back() + linebuffer.begin()) {
          line_escapes.pop_back();
          r++;
        }
        else break;
      }
      return r;
    }

    unsigned get_all_offset()
    {
      unsigned tmp = static_cast<unsigned>(line_escapes.size());
      line_escapes.clear();
      return tmp;
    }

    Lexer& add_escape(size_t sz)
    {
      line_escapes.insert(line_escapes.begin(), sz);
      return *this;
    }

    std::string get_current_token() const {
      return std::string{beginning, end};
    };

    std::string get_str() {
      get_next_token();
      return get_current_token();
    }

    Lexer& skip_line()
    {
      beginning = end = linebuffer.end();
      return *this;
    }

    Lexer& unget()
    {
      end = beginning;
      return *this;
    }

    preprocessor_token_type get_current_type() const {return prev_typ;}

    unsigned get_pos() {
      return static_cast<unsigned>(begin_current() - begin_buffer());
    }

    unsigned get_end_pos() {
      return static_cast<unsigned>(end_current() - begin_buffer());
    }

    unsigned get_current_length() {
      return static_cast<unsigned>(end_current() - begin_current());
    }

  };

  class StringLexer: public Lexer
  {
  public:
    std::istringstream sp;
    StringLexer(std::string str):  Lexer(), sp(std::move(str)) {
      this->open(sp);
    }
  };


}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_LEXER_TESTS_INCLUDED
#define SIMPLEC_LEXER_TESTS_INCLUDED

namespace simplec
{


  TEST_CASE("Basic lexer functionality")
  {
    StringLexer L {"\
    int main()\n\
    {\n\
      return 0;\n\
    }\n"
    };

    std::vector<std::string> goal_tokenization = {
      "int", "main", "(", ")", "\n", "{", "\n",
      "return", "0", ";", "\n", "}", "\n", ""
    };
    std::vector<std::string> tokens {};

    do {
      tokens.push_back(L.get_str());
    } while(!tokens.back().empty());

    REQUIRE_EQ(tokens.size(), goal_tokenization.size());
    for(size_t i = 0; i < tokens.size(); i++)
      CHECK_EQ(tokens[i], goal_tokenization[i]);
  }

}

#endif
#endif
