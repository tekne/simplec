#ifndef SIMPLEC_LEXER_READ_ESCAPE_SEQUENCE_INCLUDED
#define SIMPLEC_LEXER_READ_ESCAPE_SEQUENCE_INCLUDED

#include <simplec/lexer/read_universal_character_name.hpp>

#include <cctype>

namespace simplec
{

  template<class IT>
  IT read_escape_sequence(IT begin, IT end)
  {
    if(begin == end || *begin != '\\') return begin;
    else
    {
      // Universal character names
      auto temp = read_universal_character_name(begin, end);
      if(temp != begin) return temp;
    }
    auto fail = begin;
    begin++;
    if(begin == end) return fail;
    switch(*begin)
    {
      // Simple escape sequences
      case '\'': case '"': case '\?': case '\\': case 'a': case 'b':
      case 'f': case 'n': case 'r': case 't': case 'v': return ++begin;

      // Hexadecimal escape sequences
      case 'x': begin++;
        if(begin == end || !isxdigit(*begin)) return fail;
        while(begin != end && isxdigit(*begin)) begin++;
        return begin;

      // Octal literals
      default:
        if(!isdigit(*begin) || (*begin - '0' > 8)) return fail;
        while(
          begin != end
          && isdigit(*begin)
          && (*begin - '0' < 8)
          && (begin - fail < 4) // Octal escape sequences can only be 3 long
        ) begin++;
        return begin;
    }
  }

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_LEXER_read_escape_sequence_TEST_INCLUDED
#define SIMPLEC_LEXER_read_escape_sequence_TEST_INCLUDED

#include <string>

namespace simplec
{
  TEST_CASE("read_escape_sequence functions properly")
  {
    std::string id[] = {
      "\\n",
      "\\u03D3",
      "\\x01234nothex",
      "\\0",
      "\\01234",
      " \\0",
      "\\n"
    };

    // \n
    // ~~~^

    CHECK_EQ(
      id[0].size(),
      read_escape_sequence(id[0].begin(), id[0].end()) - id[0].begin()
    );

    // \u03D3
    // ~ϓ~~~~^

    CHECK_EQ(
      id[1].size(),
      read_escape_sequence(id[1].begin(), id[1].end()) - id[1].begin()
    );

    // \x01234abc
    // ~~~~~~~^

    CHECK_EQ(
      7, read_escape_sequence(id[2].begin(), id[2].end()) - id[2].begin()
    );

    // \0
    // ~~^

    CHECK_EQ(
      id[3].size(),
      read_escape_sequence(id[3].begin(), id[3].end()) - id[3].begin()
    );

    // \01234
    // ~~~~~^

    CHECK_EQ(
      4, read_escape_sequence(id[4].begin(), id[4].end()) - id[4].begin()
    );

    //  \0
    // ^

    CHECK_EQ(
      0, read_escape_sequence(id[5].begin(), id[5].end()) - id[5].begin()
    );

    // \\n
    // ~~^

    CHECK_EQ(
      2, read_escape_sequence(id[6].begin(), id[6].end()) - id[6].begin()
    );

  }
}

#endif
#endif
