#ifndef SIMPLEC_LEXER_READ_CHAR_LITERAL_INCLUDED
#define SIMPLEC_LEXER_READ_CHAR_LITERAL_INCLUDED

#include <simplec/lexer/ctype.hpp>
#include <simplec/lexer/read_escape_sequence.hpp>

namespace simplec
{

  template<class IT>
  IT read_char_literal(IT begin, IT end)
  {
    if(begin == end) return begin;
    auto fail = begin;

    switch(*begin)
    {
      case 'u':
      case 'U':
      case 'L':
        begin++;
        break;
    }

    if(*begin != '\'') return fail;
    begin++;
    while(begin != end)
    {
      if(is_cchar(*begin))
      {
        begin++;
      }
      else if('\'' == *begin)
      {
        return ++begin;
      }
      else if('\\' == *begin)
      {
        auto temp = read_escape_sequence(begin, end);
        if(temp == begin) return fail; // Invalid escape sequence
        else begin = temp; // Set pointer to end of valid escape sequence
      }
      else
      {
        break;
      }
    }
    return fail;
  }

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_LEXER_READ_CHAR_LITERAL_TEST_INCLUDED
#define SIMPLEC_LEXER_READ_CHAR_LITERAL_TEST_INCLUDED

#include <string>

namespace simplec
{
  TEST_CASE("read_char_literal functions properly")
  {
    std::string id[] = {"'c'",
      "'This is a rather \\n \\'fancy\\' char literal, \
      made using \\\\n, with \\''",
      "'c' 'then' ' '",
      " ' ' 'then' 'c'",
      "'This is a run-on character literal containing a newline,\n\
      which is invalid according to the C standard'",
      "'This \\'char literal\\' is merely not terminated",
      "this->is_not_even_a_character_literal()",
      "u\'c\' is a char16_t literal",
      "U\'c\' is a char32_t literal",
      "L\'c\' is a wchar_t literal",
      "u is not a char literal"
    };

    // 'c'
    // ~~~^

    CHECK_EQ(id[0].end(), read_char_literal(id[0].begin(), id[0].end()));

    // 'This is a rather \n \'fancy\' char literal, made using \n, with \''
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^

    CHECK_EQ(id[1].end(), read_char_literal(id[1].begin(), id[1].end()));

    // 'c' 'then' ' '
    // ~~~^

    CHECK_EQ(id[2].begin() + 3, read_char_literal(id[2].begin(), id[2].end()));

    //  ' ' 'then' 'c'
    // ^

    CHECK_EQ(id[3].begin(), read_char_literal(id[3].begin(), id[3].end()));

    // "This is a run-on string containing a newline,
    // ^
    // which is invalid according to the C standard"
    //

    CHECK_EQ(id[4].begin(), read_char_literal(id[4].begin(), id[4].end()));

    // "This \'char literal\' is merely not terminated
    // ^

    CHECK_EQ(id[5].begin(), read_char_literal(id[5].begin(), id[5].end()));

    //  this->is_not_even_a_character_literal()
    // ^

    CHECK_EQ(id[6].begin(), read_char_literal(id[6].begin(), id[6].end()));

    // u'c' is a char16_t literal
    // ~~~~^

    CHECK_EQ(id[7].begin() + 4, read_char_literal(id[7].begin(), id[7].end()));

    // U'c' is a char32_t literal
    // ~~~~^

    CHECK_EQ(id[8].begin() + 4, read_char_literal(id[8].begin(), id[8].end()));

    // L'c' is a wchar_t literal
    // ~~~~^

    CHECK_EQ(id[9].begin() + 4, read_char_literal(id[9].begin(), id[9].end()));

    // u is not a char literal
    // ^

    CHECK_EQ(id[10].begin(), read_char_literal(id[10].begin(), id[10].end()));

  }
}

#endif
#endif
