#ifndef SIMPLEC_LEXER_READ_PREPROCESSING_NUMBER_INCLUDED
#define SIMPLEC_LEXER_READ_PREPROCESSING_NUMBER_INCLUDED

#include <simplec/lexer/ctype.hpp>

namespace simplec
{

  template<class IT>
  IT read_preprocessing_number(IT begin, IT end)
  {
    if(begin == end) return begin;
    if(*begin == '.')
    {
      begin++;
      if(!isdigit(*begin)) return --begin;
    }
    else if(isdigit(*begin)) begin++;
    else return begin;

    while(begin != end)
    {
      if(is_exppart(*begin)) {
        begin++;
        if(is_sign(*begin)) begin++;
        continue;
      }
      if(isdigit(*begin) || is_nondigit(*begin) || (*begin == '.')) {
        begin++;
      }
      else break;
    }
    return begin;
  }

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_LEXER_READ_PREPROCESSING_NUMBER_TEST_INCLUDED
#define SIMPLEC_LEXER_READ_PREPROCESSING_NUMBER_TEST_INCLUDED

#include <string>

namespace simplec
{
  TEST_CASE("read_preprocessing_number functions properly")
  {
    std::string id[] = {"identifier1",
      "123",
      "3.14 then space",
      " 6.28 after space",
      "1E12isntreallyvalid... ok"
    };

    // identifier1
    // ^

    CHECK_EQ(id[0].begin(), read_preprocessing_number(id[0].begin(), id[0].end()));

    // 123
    // ~~~^

    CHECK_EQ(id[1].end(), read_preprocessing_number(id[1].begin(), id[1].end()));

    // 3.14 then space
    // ~~~~^

    CHECK_EQ(id[2].begin() + 4, read_preprocessing_number(id[2].begin(), id[2].end()));

    //  6.28 after space
    // ^

    CHECK_EQ(id[3].begin(), read_preprocessing_number(id[3].begin(), id[3].end()));

    // 1E12isntreallyvalid ok
    // ~~~~~~~~~~~~~~~~~~~^

    CHECK_EQ(id[4].begin() + 22, read_preprocessing_number(id[4].begin(), id[4].end()));

  }
}

#endif


#endif
