#ifndef SIMPLEC_LEXER_READ_UNIVERSAL_CHARACTER_NAME_INCLUDED
#define SIMPLEC_LEXER_READ_UNIVERSAL_CHARACTER_NAME_INCLUDED

#include <cctype>
#include <iostream>

namespace simplec
{

  static inline int get_xdigit_from_char(char v)
  {
    if(isalpha(v)) return 10 + tolower(v) - 'a';
    return v - '0';
  }

  template<class IT>
  IT read_universal_character_name(IT begin, IT end, char32_t& value)
  {
    if(begin == end || *begin != '\\') return begin;
    auto fail = begin;
    begin++;
    if(begin == end) return fail;

    value = 0;

    switch(*begin)
    {
      case 'u':
        begin++;
        for(int i = 0; i < 4; i++) // Attempt to read 4 hex digits
        {
          if(begin == end || !isxdigit(*begin)) return fail;
          value *= 0x10;
          value += get_xdigit_from_char(*begin);
          begin++;
        }
        break;
      case 'U':
        begin++;
        for(int i = 0; i < 8; i++) // Attempt to read 8 hex digits
        {
          if(begin == end || !isxdigit(*begin)) return fail;
          value *= 0x10;
          value += get_xdigit_from_char(*begin);
          begin++;
        }
        break;
      default: return fail;
    }

    // Check if universal character falls in invalid range:
    if(value < 0xA0 && value != 0x24 && value != 0x40 && value != 0x60)
      return fail;
    if(value >= 0xD800 && value <= 0xDFFF) return fail;

    // Return end of valid universal character name
    return begin;
  }

  template<class IT>
  IT read_universal_character_name(IT begin, IT end)
  {
    char32_t unused;
    return read_universal_character_name(begin, end, unused);
  }

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_LEXER_READ_UNIVERSAL_CHARACTER_NAME_TEST_INCLUDED
#define SIMPLEC_LEXER_READ_UNIVERSAL_CHARACTER_NAME_TEST_INCLUDED

#include <string>

namespace simplec
{
  TEST_CASE("read_universal_character_name functions properly")
  {
    std::string id[] = {
      "\\u0100",
      "\\u03D3",
      "\\u03d3",
      "\\U0001F600",
      "\\U0001f600",
      " \\u0100",
      "\\u03d32445",
      "\\u000A"
    };

    char32_t value = 0;

    // \u0100
    // ~Ā~~~~^

    CHECK_EQ(
      id[0].size(),
      read_universal_character_name(
        id[0].begin(),
        id[0].end(),
        value
      ) - id[0].begin()
    );

    CHECK_EQ(value, U'Ā');
    value = 0;

    // \u03D3
    // ~ϓ~~~~^

    CHECK_EQ(
      id[1].size(),
      read_universal_character_name(
        id[1].begin(),
        id[1].end(),
        value
      ) - id[1].begin()
    );

    CHECK_EQ(value, U'ϓ');
    value = 0;

    // \u03d3
    // ~ϓ~~~~^

    CHECK_EQ(
      id[2].size(),
      read_universal_character_name(
        id[2].begin(),
        id[2].end(),
        value
      ) - id[2].begin()
    );

    CHECK_EQ(value, U'ϓ');
    value = 0;

    // \U0001F600
    // ~😀~~~~~~~^

    CHECK_EQ(
      id[3].size(),
      read_universal_character_name(
        id[3].begin(),
        id[3].end(),
        value
      ) - id[3].begin()
    );

    CHECK_EQ(value, U'😀');
    value = 0;

    // \U0001f600
    // ~😀~~~~~~~^

    CHECK_EQ(
      id[4].size(),
      read_universal_character_name(
        id[4].begin(),
        id[4].end(),
        value
      ) - id[4].begin()
    );

    CHECK_EQ(value, U'😀');
    value = 0;

    //  \U0001F600
    // ^

    CHECK_EQ(
      0,
      read_universal_character_name(
        id[5].begin(),
        id[5].end(),
        value
      ) - id[5].begin()
    );

    CHECK_EQ(value, U'\0');
    value = 0;

    // \u03d32445
    // ~ϓ~~~~^

    CHECK_EQ(
      6,
      read_universal_character_name(
        id[6].begin(),
        id[6].end(),
        value
      ) - id[6].begin()
    );

    CHECK_EQ(value, U'ϓ');
    value = 0;

    // \u000A (newline, which cannot be represented as a UCN)
    // ^

    CHECK_EQ(
      0,
      read_universal_character_name(
        id[7].begin(),
        id[7].end(),
        value
      ) - id[7].begin()
    );

  }
}

#endif
#endif
