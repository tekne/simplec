#ifndef SIMPLEC_LEXER_READ_PUNCTUATOR_INCLUDED
#define SIMPLEC_LEXER_READ_PUNCTUATOR_INCLUDED

#include <simplec/lexer/ctype.hpp>

namespace simplec
{

  template<class IT>
  IT read_punctuator(IT begin, IT end)
  {
    if(begin == end) return begin;
    auto fail = begin;

    char tmp;

    switch(*begin)
    {
        // Single character punctuators
        case '[':
        case ']':
        case '(':
        case ')':
        case '{':
        case '}':
        case '~':
        case '?':
        case ';':
        case ',':
          return ++begin;
        // Punctuators which can only be followed by =
        case '*':
        case '!':
        case '/':
        case '=':
        case '^':
          begin++;
          if(begin != end && '=' == *begin) return ++begin;
          return begin;
        // Punctuators which can only be followed by themeselves or =
        case '+':
        case '&':
        case '|':
          tmp = *begin;
          begin++;
          if((begin != end) && ('=' == *begin || tmp == *begin)) return ++begin;
          return begin;
        // Ellipses and member access
        case '.':
          fail = ++begin;
          if(begin != end && '.' == *begin)
          {
            begin++;
            if(begin != end && '.' == *begin) return ++begin;
          }
          return fail;
        // -, --, -=, ->
        case '-':
          begin++;
          if((begin != end) && (
            '=' == *begin || '>' == *begin || '-' == *begin
          )) return ++begin;
          return begin;
        // < , <:, <%, <=, <<, <<=
        case '<':
          fail = ++begin;
          if(begin != end)
          {
            switch(*begin)
            {
              case ':': case '%': case '=': return ++begin;
              case '<':
                begin++;
                if(begin != end && '=' == *begin) return ++begin;
                return begin;
            }
          }
          return fail;
        // >, >=, >>, >>=
        case '>':
          fail = ++begin;
          if(begin != end)
          {
            if('=' == *begin) return ++begin;
            if('>' == *begin)
            {
              begin++;
              if(begin != end && '=' == *begin) return ++begin;
              return begin;
            }
          }
          return fail;
        // %, %=, %>, %:, %:%:
        case '%':
          fail = ++begin;
          if(begin != end)
          {
            switch(*begin)
            {
              case '=': case '>': return ++begin;
              case ':':
                fail = ++begin;
                if(begin == end || (*begin != '%')) return fail;
                begin++;
                if(begin == end || (*begin != ':')) return fail;
                return ++begin;
            }
          }
          return fail;
        // :, :>
        case ':':
          begin++;
          if(begin != end && '>' == *begin) return ++begin;
          return begin;
        // #, ##
        case '#':
          begin++;
          if(begin != end && '#' == *begin) return ++begin;
          return begin;
    }
    return fail;
  }

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_LEXER_READ_PUNCTUATOR_TEST_INCLUDED
#define SIMPLEC_LEXER_READ_PUNCTUATOR_TEST_INCLUDED

#include <string>

namespace simplec
{
  TEST_CASE("read_punctuator functions properly")
  {
    std::string id[] = {"++",
    "---",
    "->->->",
    "++x + y",
    " ++x",
    "x++",
    "%:%::++"
    };

    // ++
    // ~~^
    CHECK_EQ(id[0].end(), read_punctuator(id[0].begin(), id[0].end()));

    // ---
    // ~~^
    CHECK_EQ(id[1].begin() + 2, read_punctuator(id[1].begin(), id[1].end()));

    // ->->->
    // ~~^
    CHECK_EQ(id[2].begin() + 2, read_punctuator(id[2].begin(), id[2].end()));

    // ++x + y
    // ~~^
    CHECK_EQ(id[3].begin() + 2, read_punctuator(id[3].begin(), id[3].end()));

    //  ++x
    // ^
    CHECK_EQ(id[4].begin(), read_punctuator(id[4].begin(), id[4].end()));

    // x++
    // ^
    CHECK_EQ(id[5].begin(), read_punctuator(id[5].begin(), id[5].end()));

    // %:%::++
    // ~~~~^
    CHECK_EQ(id[6].begin() + 4, read_punctuator(id[6].begin(), id[6].end()));
  }
}

#endif
#endif
