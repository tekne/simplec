#ifndef SIMPLEC_LEXER_TOK_TYPE_INCLUDED
#define SIMPLEC_LEXER_TOK_TYPE_INCLUDED

#include <cctype>
#include <string>

namespace simplec
{

  enum preprocessor_token_type: unsigned char {
    // Preprocessor token types:
    tok_unknown = 0,
    tok_nl = 1,
    tok_num = 2,
    tok_char_literal = 3,
    tok_sym = 4,
    tok_eof = 5,

    // Dual (valid in both preprocessor and parser) token types:
    tok_id = 6,
    tok_str = 7,
    tok_punc = 8,

    // Invalid preprocessor token (distinct from unknown)
    tok_err = static_cast<unsigned char>(-1)
  };

}

#endif
