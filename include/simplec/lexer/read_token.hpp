#ifndef SIMPLEC_LEXER_READ_TOKEN_INCLUDED
#define SIMPLEC_LEXER_READ_TOKEN_INCLUDED

#include <cstring>

#include <simplec/lexer/ttype.hpp>
#include <simplec/lexer/read_char_literal.hpp>
#include <simplec/lexer/read_identifier.hpp>
#include <simplec/lexer/read_preprocessing_number.hpp>
#include <simplec/lexer/read_punctuator.hpp>
#include <simplec/lexer/read_string_literal.hpp>
#include <simplec/lexer/skip_whitespace.hpp>

namespace simplec
{

  template<class IT>
  IT read_token(IT begin, IT end, preprocessor_token_type& typ)
  {
    // Check for attempt to read empty buffer
    if(begin == end)
    {
      typ = tok_eof;
      return begin;
    }

    // Try to extract a:
    // Preprocessing number
    auto tmp = read_preprocessing_number(begin, end);
    if(tmp != begin)
    {
      typ = tok_num;
      return tmp;
    }
    // Punctuator
    tmp = read_punctuator(begin, end);
    if(tmp != begin)
    {
      typ = tok_punc;
      return tmp;
    }
    // String literal
    tmp = read_string_literal(begin, end);
    if(tmp != begin)
    {
      typ = tok_str;
      return tmp;
    }
    // Character literal
    tmp = read_char_literal(begin, end);
    if(tmp != begin)
    {
      typ = tok_char_literal;
      return tmp;
    }
    // Identifier
    tmp = read_identifier(begin, end);
    if(tmp != begin)
    {
      typ = tok_id;
      return tmp;
    }
    if(begin == end)
    {
      typ = tok_eof;
      return begin;
    }
    else if('\n' == *begin) typ = tok_nl;
    else typ = tok_sym;
    // Single character, not fitting into any of the above categories.
    // Note that whitespace will fall into this category, it should be skipped
    // beforehand!
    return ++begin;
  }

  template<class IT>
  std::vector<std::string> get_raw_tokens(IT begin, IT end)
  {
    preprocessor_token_type ignored;
    std::vector<std::string> result {};
    IT temp;

    begin = skip_whitespace(begin, end);
    while(begin != end) {
      temp = read_token(begin, end, ignored);
      result.emplace_back(begin, temp);
      if(begin == temp) break;
      begin = skip_whitespace(temp, end);
    }

    return result;
  }

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_LEXER_READ_TOKEN_TEST_INCLUDED
#define SIMPLEC_LEXER_READ_TOKEN_TEST_INCLUDED

#include <string>

namespace simplec
{

  TEST_CASE("Basic read_token functionality")
  {
    preprocessor_token_type typ = tok_unknown;
    std::string id[] = {
      "x",
      "++",
      "\"string\"",
      "\'char\'",
      "x++",
      "++x",
      "...",
      ".5",
      "U\"char32_t string\""
    };

    CHECK_EQ(id[0].end(), read_token(id[0].begin(), id[0].end(), typ));
    CHECK_EQ(typ, tok_id);

    CHECK_EQ(id[1].end(), read_token(id[1].begin(), id[1].end(), typ));
    CHECK_EQ(typ, tok_punc);

    CHECK_EQ(id[2].end(), read_token(id[2].begin(), id[2].end(), typ));
    CHECK_EQ(typ, tok_str);

    CHECK_EQ(id[3].end(), read_token(id[3].begin(), id[3].end(), typ));
    CHECK_EQ(typ, tok_char_literal);

    CHECK_EQ(id[4].begin() + 1, read_token(id[4].begin(), id[4].end(), typ));
    CHECK_EQ(typ, tok_id);

    CHECK_EQ(id[5].begin() + 2, read_token(id[5].begin(), id[5].end(), typ));
    CHECK_EQ(typ, tok_punc);

    CHECK_EQ(id[6].end(), read_token(id[6].begin(), id[6].end(), typ));
    CHECK_EQ(typ, tok_punc);

    CHECK_EQ(id[7].end(), read_token(id[7].begin(), id[7].end(), typ));
    CHECK_EQ(typ, tok_num);

    CHECK_EQ(id[8].end(), read_token(id[8].begin(), id[8].end(), typ));
    CHECK_EQ(typ, tok_str);
  }

}

#endif
#endif
//#endif
