#ifndef SIMPLEC_LEXER_READ_STRING_LITERAL_INCLUDED
#define SIMPLEC_LEXER_READ_STRING_LITERAL_INCLUDED

#include <simplec/lexer/ctype.hpp>

namespace simplec
{

  template<class IT>
  IT read_string_literal(IT begin, IT end)
  {
    if(begin == end) return begin;
    auto fail = begin;

    switch(*begin)
    {
      case 'u':
        begin++;
        if(*begin == '8') begin++;
        break;
      case 'U':
      case 'L':
        begin++;
        break;
    }

    if(*begin != '\"') return fail;
    begin++;
    while(begin != end)
    {
      if(is_schar(*begin))
      {
        begin++;
      }
      else if('\"' == *begin)
      {
        return ++begin;
      }
      else if('\\' == *begin)
      {
        //TODO: warn/error if not simple escape sequence
        begin++;
        if(begin != end)
        {
          if(is_schar(*begin) || '\"' == *begin || '\\' == *begin)
          {
            begin++;
            continue;
          }
        }
        break;
      }
      else
      {
        break;
      }
    }
    return fail;
  }

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_LEXER_READ_STRING_LITERAL_TEST_INCLUDED
#define SIMPLEC_LEXER_READ_STRING_LITERAL_TEST_INCLUDED

#include <string>

namespace simplec
{
  TEST_CASE("read_string_literal functions properly")
  {
    std::string id[] = {"\"This is a string literal\"",
      "\"This is a rather \\n \\\"fancy\\\" string, \
      made using \\\\n, with \\\"\"",
      "\"string\" \"then\" \"space\"",
      " \"space\" \"then\" \"string\"",
      "\"This is a run-on string containing a newline,\n\
      which is invalid according to the C standard\"",
      "\"This \\\"string\\\" is merely not terminated",
      "this->is_not_even_a_string()",
      "L\"this is a wchar_t string\"",
      "u8\"this is a UTF-8 string\"",
      "u is not a string"
    };

    // "This is a string literal"
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~^

    CHECK_EQ(id[0].end(), read_string_literal(id[0].begin(), id[0].end()));

    // "This is a rather\n \"fancy\" string, made using \n, with \""
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^

    CHECK_EQ(id[1].end(), read_string_literal(id[1].begin(), id[1].end()));

    // "string" "then" "space",
    // ~~~~~~~~^

    CHECK_EQ(id[2].begin() + 8, read_string_literal(id[2].begin(), id[2].end()));

    //  "space" "then" "string"
    // ^,

    CHECK_EQ(id[3].begin(), read_string_literal(id[3].begin(), id[3].end()));

    // "This is a run-on string containing a newline,
    // ^
    // which is invalid according to the C standard"
    //

    CHECK_EQ(id[4].begin(), read_string_literal(id[4].begin(), id[4].end()));

    // "This \"string\" is merely not terminated
    // ^

    CHECK_EQ(id[5].begin(), read_string_literal(id[5].begin(), id[5].end()));

    // this->is_not_even_a_string()
    // ^

    CHECK_EQ(id[6].begin(), read_string_literal(id[6].begin(), id[6].end()));

    // L"this is a wchar_t string"
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~^

    CHECK_EQ(id[7].end(), read_string_literal(id[7].begin(), id[7].end()));

    // u8"this is a UTF-8 string"
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~^

    CHECK_EQ(id[8].end(), read_string_literal(id[8].begin(), id[8].end()));

    // u is not a string
    // ^

    CHECK_EQ(id[9].begin(), read_string_literal(id[9].begin(), id[9].end()));

  }
}

#endif
#endif
