#ifndef SIMPLEC_CHAR_UTILS_INCLUDED
#define SIMPLEC_CHAR_UTILS_INCLUDED

#include <cctype>
#include <string>

namespace simplec
{

  inline bool is_hchar(int c)
  {
    return std::char_traits<char>::not_eof(c) && (c != '>') && (c != '\n');
  }

  inline bool is_qchar(int c)
  {
    return std::char_traits<char>::not_eof(c) && (c != '\"') && (c != '\n');
  }

  inline bool is_schar(int c)
  {
    return is_qchar(c) && (c != '\\');
  }

  inline bool is_cchar(int c)
  {
    return std::char_traits<char>::not_eof(c)
    && (c != '\'') && (c != '\n') && (c != '\\');
  }

  inline bool is_nondigit(int c)
  {
    return isalpha(c) || (c == '_');
  }

  inline bool is_octdigit(int c)
  {
    return isdigit(c) && (c - '0' < 8);
  }

  inline bool is_exppart(int c)
  {
    return (c == 'e') || (c == 'E') || (c == 'p') || (c == 'P');
  }

  inline bool is_sign(int c)
  {
    return (c == '+') || (c == '-');
  }

  inline bool is_cspace(int c)
  {
    //TODO: parse \0 as whitespace, but warn on detection if enabled
    return !c || (isspace(c) && '\n' != c);
  }

  inline bool is_allowed_in_identifier(char32_t val)
  {
    switch(val)
    {
      case 0xA8: case 0xAA: case 0xAD: case 0xAF: return true;
    }
    return
      (0xB2 <= val && 0xB5 >= val)
      || (0xB7 <= val && 0xBA >= val)
      || (0xBC <= val && 0xBE >= val)
      || (0xC0 <= val && 0xD6 >= val)
      || (0xD8 <= val && 0xF6 >= val)
      || (0xF8 <= val && 0xFF >= val)
      || (0x100 <= val && 0x167F >= val)
      || (0x1681 <= val && 0x180D >= val)
      || (0x180F <= val && 0x1FFF >= val)
      || (0x200B <= val && 0x200D >= val)
      || (0x202A <= val && 0x202E >= val)
      || (0x203F <= val && 0x2040 >= val)
      || (0x2054 == val)
      || (0x2060 <= val && 0x206F >= val)
      || (0x2070 <= val && 0x218F >= val)
      || (0x2460 <= val && 0x24FF >= val)
      || (0x2776 <= val && 0x2793 >= val)
      || (0x2C00 <= val && 0x2DFF >= val)
      || (0x2E80 <= val && 0x2FFF >= val)
      || (0x3004 <= val && 0x3007 >= val)
      || (0x3021 <= val && 0x302F >= val)
      || (0x3031 <= val && 0x303F >= val)
      || (0x3040 <= val && 0xD7FF >= val)
      || (0xF900 <= val && 0xFD3D >= val)
      || (0xFD40 <= val && 0xFDCF >= val)
      || (0xFDF0 <= val && 0xFE44 >= val)
      || (0xFE47 <= val && 0xFFFD >= val)
      || (0x10000 <= val && 0x1FFFD >= val)
      || (0x20000 <= val && 0x2FFFD >= val)
      || (0x30000 <= val && 0x3FFFD >= val)
      || (0x40000 <= val && 0x4FFFD >= val)
      || (0x50000 <= val && 0x5FFFD >= val)
      || (0x60000 <= val && 0x6FFFD >= val)
      || (0x70000 <= val && 0x7FFFD >= val)
      || (0x80000 <= val && 0x8FFFD >= val)
      || (0x90000 <= val && 0x9FFFD >= val)
      || (0xA0000 <= val && 0xAFFFD >= val)
      || (0xB0000 <= val && 0xBFFFD >= val)
      || (0xC0000 <= val && 0xCFFFD >= val)
      || (0xD0000 <= val && 0xDFFFD >= val)
      || (0xE0000 <= val && 0xEFFFD >= val);
  }

  inline char32_t get_utf32_codepoint(char b0) {return b0;}
  inline char32_t get_utf32_codepoint(char b0, char b1) {
    // 0x1F & 0xXX = 0b00011111 & 0bxxxxxxxx = 0b000xxxxx
    b0 = 0x1F & b0;
    // 0x3F & 0xXX = 0b00111111 & 0bxxxxxxxx = 0b00xxxxxx
    b1 = 0x3F & b1;
    char32_t result = b0;
    result <<= 6;
    return result | b1;
  }
  inline char32_t get_utf32_codepoint(char b0, char b1, char b2) {
    // 0x0F & 0xXX = 0b00001111 & 0bxxxxxxxx = 0b0000xxxx
    b0 = 0x0F & b0;
    // 0x3F & 0xXX = 0b00111111 & 0bxxxxxxxx = 0b00xxxxxx
    b1 = 0x3F & b1;
    b2 = 0x3F & b2;
    char32_t result = b0;
    result <<= 6;
    result |= b1;
    result <<= 6;
    result |= b2;
    return result;
  }
  inline char32_t get_utf32_codepoint(char b0, char b1, char b2, char b3) {
    // 0x07 & 0xXX = 0b00000111 & 0bxxxxxxxx = 0b00000xxx
    b0 = 0x07 & b0;
    // 0x3F & 0xXX = 0b00111111 & 0bxxxxxxxx = 0b00xxxxxx
    b1 = 0x3F & b1;
    b2 = 0x3F & b2;
    b3 = 0x3F & b3;
    char32_t result = b0;
    result <<= 6;
    result |= b1;
    result <<= 6;
    result |= b2;
    result <<= 6;
    result |= b3;
    return result;
  }

}

#endif
