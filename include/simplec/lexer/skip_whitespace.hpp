#ifndef SIMPLEC_LEXER_SKIP_INCLUDED
#define SIMPLEC_LEXER_SKIP_INCLUDED

#include <simplec/lexer/ctype.hpp>

namespace simplec
{

  template<class IT>
  IT skip_spaces(IT begin, IT end)
  {
    while(begin != end && is_cspace(*begin)) begin++;
    return begin;
  }

  template<class IT>
  IT skip_comment(IT begin, IT end)
  {
    if(begin == end) return begin;
    auto fail = begin;

    if('/' != *begin) return fail;
    begin++;
    if(begin == end || '/' != *begin) return fail;
    begin++;
    while(begin != end && *begin != '\n') begin++;
    return begin;
  }

  template<class IT>
  IT skip_to_end_of_multiline_comment(IT begin, IT end)
  {
    while(begin != end)
    {
      if('*' == *begin)
      {
        begin++;
        if(begin == end) break;
        else if(*begin == '/') return ++begin;
      }
      begin++;
    }
    return begin;
  }

  template<class IT>
  IT skip_multiline_comment(IT begin, IT end)
  {
    if(begin == end) return begin;
    auto fail = begin;

    if('/' != *begin) return fail;
    begin++;
    if(begin == end || '*' != *begin) return fail;
    begin++;
    return skip_to_end_of_multiline_comment(begin, end);
  }

  template<class IT>
  IT skip_whitespace(IT begin, IT end)
  {
    begin = skip_spaces(begin, end);
    auto com = skip_comment(begin, end);
    if(com != begin) return com;
    com = skip_multiline_comment(begin, end);
    if(com != begin) return skip_whitespace(com, end);
    return begin;
  }

  template<class IT>
  IT skip_partial_whitespace(IT begin, IT end, bool& complete)
  {
    if(complete)
    {
      begin = skip_spaces(begin, end);
      auto com = skip_comment(begin, end);
      if(com != begin) return com;
      com = skip_multiline_comment(begin, end);
      if(com != begin) {
        if(com == end)
        {
          com--;
          complete = ('\n' != *com);
          com++;
          return com;
        }
        else return skip_partial_whitespace(com, end, complete);
      }
      return com;
    }
    else
    {
      if(begin == end) return begin;
      begin = skip_to_end_of_multiline_comment(begin, end);
      if(begin == end)
      {
        begin--;
        complete = ('\n' != *begin);
        return ++begin;
      }
      else
      {
        complete = true;
        return skip_partial_whitespace(begin, end, complete);
      }
    }
  }

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_LEXER_SKIP_WHITESPACE_TEST_INCLUDED
#define SIMPLEC_LEXER_SKIP_WHITESPACE_TEST_INCLUDED

#include <string>

namespace simplec
{
  TEST_CASE("skip_spaces functions properly")
  {
    std::string id[] = {" ",
    "\t\tint",
    "int "
    };

    //
    // ~^
    CHECK_EQ(id[0].end(), skip_spaces(id[0].begin(), id[0].end()));

    //         int
    // ~   ~   ^
    CHECK_EQ(id[1].begin() + 2, skip_spaces(id[1].begin(), id[1].end()));

    // int
    // ^
    CHECK_EQ(id[2].begin(), skip_spaces(id[2].begin(), id[2].end()));

  }

  TEST_CASE("skip_comment functions properly")
  {
    std::string id[] = {"// This is a comment",
    "// This is a comment, followed by a\n// newline!",
    " // This is a comment after a space"
    };

    // // This is a comment
    // ~~~~~~~~~~~~~~~~~~~~^
    CHECK_EQ(id[0].end(), skip_comment(id[0].begin(), id[0].end()));

    // // This is a comment, followed by a
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^
    // // newline!
    //
    CHECK_EQ(id[1].begin() + 35, skip_comment(id[1].begin(), id[1].end()));

    //  // This is a comment after a space
    // ^
    CHECK_EQ(id[2].begin(), skip_comment(id[2].begin(), id[2].end()));

  }

  TEST_CASE("skip_multiline_comment functions properly")
  {
    std::string id[] = { "/*multiline \ncomment*/",
      "/*increment:\nx*/ x++;",
      " /*comment*/",
    };

    // /*multiline
    // ~~~~~~~~~~~~
    // comment*/
    // ~~~~~~~~~^
    CHECK_EQ(id[0].end(), skip_multiline_comment(id[0].begin(), id[0].end()));

    // /*increment:
    // ~~~~~~~~~~~~
    // x*/ x++;
    // ~~~^
    CHECK_EQ(
      id[1].begin() + 16,
      skip_multiline_comment(id[1].begin(), id[1].end())
    );

    //  /*comment*/
    // ^
    CHECK_EQ(id[2].begin(), skip_multiline_comment(id[2].begin(), id[2].end()));
  }

  TEST_CASE("skip_whitespace functions properly")
  {
    std::string id[] = { "        // comment after whitespace",
      "/*multiple*/ /*multi\nline*/ /*comments*/ // in a row\nx++;",
      "//line\n/*then multiline*/",
      "   /*multiline\ncomment*/ x++;",
      "/*doesn't ignore newlines:*/\n/*see!*/"
    };

    //         // comment after whitespace
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^
    CHECK_EQ(id[0].end(), skip_whitespace(id[0].begin(), id[0].end()));

    // /*multiple*/ /*multi
    // ~~~~~~~~~~~~~~~~~~~~
    // line*/ /*comments*/ // in a row
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^
    // x++
    CHECK_EQ(
      id[1].begin() + 52,
      skip_whitespace(id[1].begin(), id[1].end())
    );

    // //line
    // ~~~~~~^
    // /*then multiline*/
    CHECK_EQ(
      id[2].begin() + 6,
      skip_whitespace(id[2].begin(), id[2].end())
    );

    //    /*multiline
    // ~~~~~~~~~~~~~~
    // comment*/ x++;
    // ~~~~~~~~~~^
    CHECK_EQ(
      id[3].begin() + 25,
      skip_whitespace(id[3].begin(), id[3].end())
    );

    // /*doesn't ignore newlines:*/
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~^
    // /*see!*/
    CHECK_EQ(
      id[4].begin() + 28,
      skip_whitespace(id[4].begin(), id[4].end())
    );

  }

  TEST_CASE("skip_partial_whitespace functions properly")
  {
    std::string id[] = { "        // comment after whitespace",
      "/*multiple*/ /*multi\nline*/ /*comments*/ // in a row\nx++;",
      "//line\n/*then multiline*/",
      "   /*multiline\ncomment*/ x++;",
      "/*doesn't ignore newlines:*/\n/*see!*/",
      "   /*begin*/ /*partial_read\n",
      "then the rest*/ /*check\n status*/ ok();"
    };

    bool complete = true;

    //         // comment after whitespace
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^
    CHECK_EQ(
      id[0].end(),
      skip_partial_whitespace(id[0].begin(), id[0].end(), complete)
    );
    CHECK_UNARY(complete);

    // /*multiple*/ /*multi
    // ~~~~~~~~~~~~~~~~~~~~
    // line*/ /*comments*/ // in a row
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~^
    // x++
    CHECK_EQ(
      52,
      skip_partial_whitespace(id[1].begin(), id[1].end(), complete)
      - id[1].begin()
    );
    CHECK_UNARY(complete);

    // //line
    // ~~~~~~^
    // /*then multiline*/
    CHECK_EQ(
      6,
      skip_partial_whitespace(id[2].begin(), id[2].end(), complete)
      - id[2].begin()
    );
    CHECK_UNARY(complete);

    //    /*multiline
    // ~~~~~~~~~~~~~~
    // comment*/ x++;
    // ~~~~~~~~~~^
    CHECK_EQ(
      25,
      skip_partial_whitespace(id[3].begin(), id[3].end(), complete)
      - id[3].begin()
    );
    CHECK_UNARY(complete);

    // /*doesn't ignore newlines:*/
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~^
    // /*see!*/
    CHECK_EQ(
      28,
      skip_partial_whitespace(id[4].begin(), id[4].end(), complete)
      - id[4].begin()
    );
    CHECK_UNARY(complete);

    //    /*begin*/ /*partial read
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~&
    // then the rest*/ /*check
    // &~~~~~~~~~~~~~~~~~~~~~~
    // status*/ ok();
    // ~~~~~~~~~^
    CHECK_EQ(
      id[5].size(),
      skip_partial_whitespace(id[5].begin(), id[5].end(), complete)
      - id[5].begin()
    );
    CHECK_UNARY_FALSE(complete);
    CHECK_EQ(
      34,
      skip_partial_whitespace(id[6].begin(), id[6].end(), complete)
      - id[6].begin()
    );
    CHECK_UNARY(complete);

  }
}

#endif
#endif
