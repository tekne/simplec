#ifndef SIMPLEC_LEXER_READ_HCHAR_LITERAL_INCLUDED
#define SIMPLEC_LEXER_READ_HCHAR_LITERAL_INCLUDED

namespace simplec
{

  template<class IT>
  IT read_hchar_literal(IT begin, IT end)
  {
    if(begin == end) return begin;
    else if('<' != *begin) return begin;
    auto fail = begin;

    begin++;
    if(begin == end) return fail;

    while(begin != end)
    {
      if('>' == *begin) return ++begin;
      else if('\n' == *begin) return fail;
      else begin++;
    }

    return fail;
  }

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_LEXER_READ_HCHAR_LITERAL_TEST_INCLUDED
#define SIMPLEC_LEXER_READ_HCHAR_LITERAL_TEST_INCLUDED

#include <string>

namespace simplec
{
  TEST_CASE("read_hchar_literal functions properly")
  {
    std::string id[] = {
      "<this/is/an/hchar/literal>",
      "<also an hchar literal>",
      "<no newlines\n allowed>",
      "<must be terminated",
      "must start correctly>",
      " <that means no spaces>"
    };

    // <this/is/an/hchar/literal>
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~^

    CHECK_EQ(id[0].end(), read_hchar_literal(id[0].begin(), id[0].end()));

    // <also an hchar literal>
    // ~~~~~~~~~~~~~~~~~~~~~~~^

    CHECK_EQ(id[1].end(), read_hchar_literal(id[1].begin(), id[1].end()));

    // <no newlines
    // ^
    //  allowed>
    //

    CHECK_EQ(id[2].begin(), read_hchar_literal(id[2].begin(), id[2].end()));

    // <must be terminated
    // ^

    CHECK_EQ(id[3].begin(), read_hchar_literal(id[3].begin(), id[3].end()));

    // must start correctly>
    // ^

    CHECK_EQ(id[4].begin(), read_hchar_literal(id[4].begin(), id[4].end()));

    //  <that means no spaces>
    // ^

    CHECK_EQ(id[5].begin(), read_hchar_literal(id[5].begin(), id[5].end()));

  }
}

#endif
#endif
