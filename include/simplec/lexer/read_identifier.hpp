#ifndef SIMPLEC_LEXER_READ_IDENTIFIER_INCLUDED
#define SIMPLEC_LEXER_READ_IDENTIFIER_INCLUDED

#include <simplec/lexer/ctype.hpp>
#include <simplec/lexer/read_universal_character_name.hpp>

namespace simplec
{

  template<class IT>
  IT read_identifier(IT begin, IT end)
  {
    //TODO: allow unicode characters themselves in identifiers
    if(begin == end) return begin;
    auto fail = begin;
    char32_t value = 0;
    IT temp = read_universal_character_name(begin, end, value);
    if(temp != begin) { // Universal character name has been read
      if(!is_allowed_in_identifier(value)) return fail;
    }
    else if(isalpha(*begin) || '_' == *begin) begin++;
    else return fail;

    while(begin != end)
    {
      temp = read_universal_character_name(begin, end, value);
      if(temp != begin) { // Universal character name has been read
        if(!is_allowed_in_identifier(value)) return begin;
        begin = temp;
      }
      else if(
        isalnum(*begin)
        || '_' == *begin
      ) begin++;
      else break;
    }
    return begin;
  }

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_LEXER_READ_IDENTIFIER_TEST_INCLUDED
#define SIMPLEC_LEXER_READ_IDENTIFIER_TEST_INCLUDED

#include <string>

namespace simplec
{
  TEST_CASE("read_identifier functions properly")
  {
    std::string id[] = {"identifier1",
      "__reserved_identifier",
      "id then space",
      " space then id",
      "123",
      "\\u00DCniversal first",
      "Un\\u0129versal later",
      "üniversal first",
      "unĩversal later",
      "\xC0\xC0 is an invalid unicode character"
    };

    // identifier1
    // ~~~~~~~~~~~^

    CHECK_EQ(
      id[0].size(),
      read_identifier(id[0].begin(), id[0].end()) - id[0].begin()
    );

    // __reserved_identifier
    // ~~~~~~~~~~~~~~~~~~~~~^

    CHECK_EQ(
      id[1].size(),
      read_identifier(id[1].begin(), id[1].end()) - id[1].begin()
    );

    // id then space
    // ~~^

    CHECK_EQ(2, read_identifier(id[2].begin(), id[2].end()) - id[2].begin());

    //  space then id
    // ^

    CHECK_EQ(0, read_identifier(id[3].begin(), id[3].end()) - id[3].begin());

    //  123
    // ^

    CHECK_EQ(0, read_identifier(id[4].begin(), id[4].end()) - id[4].begin());

    // \u00DCniversal first
    // ~ü~~~~~~~~~~~~^

    CHECK_EQ(14, read_identifier(id[5].begin(), id[5].end()) - id[5].begin());

    // Un\u0129versal later
    // ~~~ĩ~~~~~~~~~~^

    CHECK_EQ(14, read_identifier(id[6].begin(), id[6].end()) - id[6].begin());

    // üniversal first
    // ~~~~~~~~~^

    CHECK_EQ(14, read_identifier(id[5].begin(), id[5].end()) - id[5].begin());

    // Unĩversal later
    // ~~~~~~~~~^

    CHECK_EQ(14, read_identifier(id[6].begin(), id[6].end()) - id[6].begin());

    // ?? is an invalid unicode character
    // ^

    CHECK_EQ(0, read_identifier(id[7].begin(), id[7].end()) - id[7].begin());

  }
}

#endif
#endif
