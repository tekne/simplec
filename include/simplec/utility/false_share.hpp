#ifndef SIMPLEC_UTILITY_FALSE_SHARE_INCLUDED
#define SIMPLEC_UTILITY_FALSE_SHARE_INCLUDED

#include <memory>

namespace simplec
{

  template<class T>
  inline static std::shared_ptr<T> false_share(T* o) {
    return std::shared_ptr<T>(o, [](const T*){});
  }

  template<class T>
  inline static std::shared_ptr<const T> false_share(const T* o) {
    return std::shared_ptr<const T>(o, [](const T*){});
  }

  template<class T>
  inline static std::shared_ptr<T> false_share(T& o) {
    return false_share(&o);
  }

  template<class T>
  inline static std::shared_ptr<const T> false_share(const T& o) {
    return false_share(&o);
  }

}

#endif
