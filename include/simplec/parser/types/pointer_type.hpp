#ifndef SIMPLEC_POINTER_TYPE_INCLUDED
#define SIMPLEC_POINTER_TYPE_INCLUDED

#include <memory>
#include <utility>
#include <string>

#include <simplec/parser/types/type.hpp>
#include <simplec/parser/types/scalar_type.hpp>

namespace simplec
{

  class PointerType: public ScalarType
  {
    QualifiedType rt;
  public:
    PointerType(QualifiedType qt, uint64_t fa = 0)
    : ScalarType(fa), rt(std::move(qt)) {}

    static std::shared_ptr<const PointerType> make_pointer(QualifiedType t) {
      return std::make_shared<const PointerType>(t);
    }

    const Type& print_to_stream(std::ostream& str) const {
      rt.print_to_stream(str);
      str << "*";
      return *this;
    }

    PointerType& set_pointed_type(QualifiedType t) {
      rt = t;
      return *this;
    }

    QualifiedType base_type() const {
      return rt;
    }
    std::shared_ptr<const Type> raw_base_type() const {
      return rt.raw_type();
    }
    const bool is_const_ptr() const {return rt.is_const();}
    const bool is_volatile_ptr() const {return rt.is_volatile();}
    const bool is_restrict_ptr() const {return rt.is_restrict();}
    const bool is_cv_ptr() const {return rt.is_cv();}
    const bool is_qualified_ptr() const {return rt.is_qualified();}

    bool equal_type(const Type* t) const final {
      const PointerType* pt = dynamic_cast<const PointerType*>(t);
      if(!pt) return false;
      if(forced_alignment() != pt->forced_alignment()) return false;
      return rt == pt->rt;
    }

    bool compatible_type(const Type* t) const final {
      const PointerType* pt = dynamic_cast<const PointerType*>(t);
      if(!pt) return false;
      if(forced_alignment() != pt->forced_alignment()) return false;
      return rt.compatible_type(pt->base_type());
    }
  };

}

#endif
