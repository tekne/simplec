#ifndef SIMPLEC_AGGREGATE_TYPE_INCLUDED
#define SIMPLEC_AGGREGATE_TYPE_INCLUDED

#include <simplec/parser/types/type.hpp>

namespace simplec
{

  class AggregateType: public Type
  {
  public:
    AggregateType(uint64_t fa = 0): Type(fa) {}
  };

}

#endif
