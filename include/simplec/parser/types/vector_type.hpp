#ifndef SIMPLEC_Vector_TYPE_INCLUDED
#define SIMPLEC_Vector_TYPE_INCLUDED

#include <memory>
#include <utility>
#include <exception>

#include <simplec/parser/types/arithmetic_type.hpp>
#include <simplec/parser/types/scalar_type.hpp>

namespace simplec
{

  class VectorType: public ScalarType
  {
    std::shared_ptr<const ScalarType> rt; // Referenced type
    uint32_t count;
  public:

    VectorType(
      std::shared_ptr<const ScalarType> r,
      uint32_t c = 1,
      uint64_t fa = 0
    )
    : ScalarType(fa), rt(std::move(r)) , count(c) {
      if(!rt) throw std::runtime_error(
          "Internal error: attempted to make vector of null pointer type"
        );
      if(!c) throw std::runtime_error(
        "Cannot have vector type without elements"
      );
    }

    const Type& print_to_stream(std::ostream& str) const {
      rt->print_to_stream(str);
      str << "<" << count << ">";
      return *this;
    }

    std::shared_ptr<const ScalarType> base_type() const {
      return rt;
    }

    bool equal_type(const Type* t) const final {
      const VectorType* vt = dynamic_cast<const VectorType*>(t);
      if(!vt) return false;
      if(count != vt->count) return false;
      return rt->equal_type(vt->base_type());
    }

    uint64_t vector_size() const {return count;}
  };

}

#endif
