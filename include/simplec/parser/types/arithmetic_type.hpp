#ifndef SIMPLEC_ARITHMETIC_TYPE_INCLUDED
#define SIMPLEC_ARITHMETIC_TYPE_INCLUDED

#include <simplec/parser/types/scalar_type.hpp>
#include <cstdint>

namespace simplec
{

  enum arithmetic_type_domain : char {real_domain = 0, complex_domain = 1};

  class ArithmeticType: public ScalarType
  {
  public:
    ArithmeticType(uint64_t fa = 0): ScalarType(fa) {}
    virtual arithmetic_type_domain type_domain() const=0;

    bool is_real() const {return type_domain() == real_domain;}
    bool is_complex() const {return type_domain() == complex_domain;}
  };

}

#endif
