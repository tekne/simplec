#ifndef SIMPLEC_PARSER_ARITHMETIC_TYPE_CONVERSIONS_INCLUDED
#define SIMPLEC_PARSER_ARITHMETIC_TYPE_CONVERSIONS_INCLUDED

#include <simplec/parser/type/arithmetic_type.hpp>
#include <simplec/parser/type/integer_type.hpp>
#include <simplec/parser/type/floating_type.hpp>
#include <simplec/parser/type/scalar_type.hpp>
#include <simplec/parser/type/vector_type.hpp>
#include <simplec/parser/type/fundamental_type_set.hpp>

#include <memory>

namespace simplec
{

  std::shared_ptr<const FloatingType> common_floating_type(
    const FloatingType& t1, const FloatingType& t2
  );

  std::shared_ptr<const IntegerType> common_integer_type(
    const IntegerType& t1, const IntegerType& t2
  );

  std::shared_ptr<const IntegerType> common_integer_type(
    const IntegerType& t1, const IntegerType& t2, const FundamentalTypeSet& F
  );

  std::shared_ptr<const ArithmeticType> common_arithmetic_type(
    const ArithmeticType* t1, const ArithmeticType* t2,
    const FundamentalTypeSet& F
  );

}

#endif
