#ifndef SIMPLEC_PARSER_FUNDAMENTAL_TYPE_SET_INCLUDED
#define SIMPLEC_PARSER_FUNDAMENTAL_TYPE_SET_INCLUDED

#include <simplec/utility/false_share.hpp>
#include <simplec/parser/types/integer_type.hpp>
#include <simplec/parser/types/floating_type.hpp>
#include <simplec/parser/types/vector_type.hpp>
#include <simplec/parser/types/pointer_type.hpp>
#include <exception>
#include <memory>

namespace simplec
{

  #define SIMPLEC_TO_GET_PTR(X) \
    auto get_ ## X ## _ptr() const {return false_share(get_ ## X());}

  #define SIMPLEC_TO_PTR_TYPE(X) \
    PointerType ptr_to_ ## X; \
    PointerType ptr_to_const_ ## X

  #define SIMPLEC_INIT_PTR_TYPE(X) \
    ptr_to_ ## X {get_ ## X ## _ptr()}, \
    ptr_to_const_ ## X {{get_ ## X ## _ptr(), const_qualified}}

  #define SIMPLEC_GET_PTR_TYPE(X) \
    const auto& get_ptr_to_ ## X() const {return ptr_to_ ## X;} \
    const auto& get_ptr_to_const_ ## X() const {return ptr_to_const_ ## X;}

  #define SIMPLEC_GET_PTR_TYPE_PTR(X) \
    auto get_ptr_to_ ## X ## _ptr() const { \
      return false_share(get_ptr_to_ ## X()); \
    } \
    auto get_ptr_to_const_ ## X ## _ptr() const { \
      return false_share(get_ptr_to_const_ ## X()); \
    }

  class FundamentalTypeSet
  {
    // Fundamental unsigned integer types
    BooleanType bool_type;
    NamedIntegerType char_type;
    // Fundamental signed integer types
    NamedIntegerType signed_char_type;
    NamedIntegerType short_type;
    NamedIntegerType int_type;
    NamedIntegerType long_type;
    NamedIntegerType long_long_type;
    // Unsigned variations
    NamedIntegerType unsigned_char_type;
    NamedIntegerType unsigned_short_type;
    NamedIntegerType unsigned_int_type;
    NamedIntegerType unsigned_long_type;
    NamedIntegerType unsigned_long_long_type;
    // Floating point types
    FloatingType float_type;
    FloatingType double_type;
    FloatingType long_double_type;
    FloatingType imag_float_type;
    FloatingType imag_double_type;
    FloatingType imag_long_double_type;
    FloatingType complex_float_type;
    FloatingType complex_double_type;
    FloatingType complex_long_double_type;
    // Special character types
    std::shared_ptr<const IntegerType> wchar_t_type; // Guaranteed <= 32 bits
    std::shared_ptr<const IntegerType> char16_t_type;
    std::shared_ptr<const IntegerType> char32_t_type;
    // Pointer Types
    SIMPLEC_TO_PTR_TYPE(bool);
    SIMPLEC_TO_PTR_TYPE(char);
    SIMPLEC_TO_PTR_TYPE(signed_char);
    SIMPLEC_TO_PTR_TYPE(short);
    SIMPLEC_TO_PTR_TYPE(int);
    SIMPLEC_TO_PTR_TYPE(long);
    SIMPLEC_TO_PTR_TYPE(long_long);
    SIMPLEC_TO_PTR_TYPE(unsigned_char);
    SIMPLEC_TO_PTR_TYPE(unsigned_short);
    SIMPLEC_TO_PTR_TYPE(unsigned_int);
    SIMPLEC_TO_PTR_TYPE(unsigned_long);
    SIMPLEC_TO_PTR_TYPE(unsigned_long_long);
    SIMPLEC_TO_PTR_TYPE(float);
    SIMPLEC_TO_PTR_TYPE(double);
    SIMPLEC_TO_PTR_TYPE(long_double);
    SIMPLEC_TO_PTR_TYPE(imag_float);
    SIMPLEC_TO_PTR_TYPE(imag_double);
    SIMPLEC_TO_PTR_TYPE(imag_long_double);
    SIMPLEC_TO_PTR_TYPE(complex_float);
    SIMPLEC_TO_PTR_TYPE(complex_double);
    SIMPLEC_TO_PTR_TYPE(complex_long_double);
    SIMPLEC_TO_PTR_TYPE(wchar);
    SIMPLEC_TO_PTR_TYPE(char16);
    SIMPLEC_TO_PTR_TYPE(char32);
    SIMPLEC_TO_PTR_TYPE(void);

  public:
    FundamentalTypeSet& set_wide_chars(uint8_t wcb);

    const BooleanType& get_bool() const {return bool_type;}
    const NamedIntegerType& get_char() const {return char_type;}
    const NamedIntegerType& get_signed_char() const {return signed_char_type;}
    const NamedIntegerType& get_short() const {return short_type;}
    const NamedIntegerType& get_int() const {return int_type;}
    const NamedIntegerType& get_long() const {return long_type;}
    const NamedIntegerType& get_long_long() const {return long_long_type;}
    const NamedIntegerType& get_unsigned_char() const {
      return unsigned_char_type;
    }
    const NamedIntegerType& get_unsigned_short() const {
      return unsigned_short_type;
    }
    const NamedIntegerType& get_unsigned_int() const {
      return unsigned_int_type;
    }
    const NamedIntegerType& get_unsigned_long() const {
      return unsigned_long_type;
    }
    const NamedIntegerType& get_unsigned_long_long() const {
      return unsigned_long_long_type;
    }

    const FloatingType& get_float() const {return float_type;}
    const FloatingType& get_double() const {return double_type;}
    const FloatingType& get_long_double() const {return long_double_type;}
    const FloatingType& get_imag_float() const {return imag_float_type;}
    const FloatingType& get_imag_double() const {return imag_double_type;}
    const FloatingType& get_imag_long_double() const {
      return imag_long_double_type;
    }
    const FloatingType& get_complex_float() const {return complex_float_type;}
    const FloatingType& get_complex_double() const {return complex_double_type;}
    const FloatingType& get_complex_long_double() const {
      return complex_long_double_type;
    }

    const IntegerType& get_wchar() const {return *wchar_t_type;}
    const IntegerType& get_char16() const {return *char16_t_type;}
    const IntegerType& get_char32() const {return *char32_t_type;}

    SIMPLEC_TO_GET_PTR(bool);
    SIMPLEC_TO_GET_PTR(char);
    SIMPLEC_TO_GET_PTR(signed_char);
    SIMPLEC_TO_GET_PTR(short);
    SIMPLEC_TO_GET_PTR(int);
    SIMPLEC_TO_GET_PTR(long);
    SIMPLEC_TO_GET_PTR(long_long);
    SIMPLEC_TO_GET_PTR(unsigned_char);
    SIMPLEC_TO_GET_PTR(unsigned_short);
    SIMPLEC_TO_GET_PTR(unsigned_int);
    SIMPLEC_TO_GET_PTR(unsigned_long);
    SIMPLEC_TO_GET_PTR(unsigned_long_long);
    SIMPLEC_TO_GET_PTR(float);
    SIMPLEC_TO_GET_PTR(double);
    SIMPLEC_TO_GET_PTR(long_double);
    SIMPLEC_TO_GET_PTR(wchar);
    SIMPLEC_TO_GET_PTR(char16);
    SIMPLEC_TO_GET_PTR(char32);
    SIMPLEC_TO_GET_PTR(imag_float);
    SIMPLEC_TO_GET_PTR(imag_double);
    SIMPLEC_TO_GET_PTR(imag_long_double);
    SIMPLEC_TO_GET_PTR(complex_float);
    SIMPLEC_TO_GET_PTR(complex_double);
    SIMPLEC_TO_GET_PTR(complex_long_double);

    SIMPLEC_GET_PTR_TYPE(bool);
    SIMPLEC_GET_PTR_TYPE(char);
    SIMPLEC_GET_PTR_TYPE(signed_char);
    SIMPLEC_GET_PTR_TYPE(short);
    SIMPLEC_GET_PTR_TYPE(int);
    SIMPLEC_GET_PTR_TYPE(long);
    SIMPLEC_GET_PTR_TYPE(long_long);
    SIMPLEC_GET_PTR_TYPE(unsigned_char);
    SIMPLEC_GET_PTR_TYPE(unsigned_short);
    SIMPLEC_GET_PTR_TYPE(unsigned_int);
    SIMPLEC_GET_PTR_TYPE(unsigned_long);
    SIMPLEC_GET_PTR_TYPE(unsigned_long_long);
    SIMPLEC_GET_PTR_TYPE(float);
    SIMPLEC_GET_PTR_TYPE(double);
    SIMPLEC_GET_PTR_TYPE(long_double);
    SIMPLEC_GET_PTR_TYPE(wchar);
    SIMPLEC_GET_PTR_TYPE(char16);
    SIMPLEC_GET_PTR_TYPE(char32);
    SIMPLEC_GET_PTR_TYPE(imag_float);
    SIMPLEC_GET_PTR_TYPE(imag_double);
    SIMPLEC_GET_PTR_TYPE(imag_long_double);
    SIMPLEC_GET_PTR_TYPE(complex_float);
    SIMPLEC_GET_PTR_TYPE(complex_double);
    SIMPLEC_GET_PTR_TYPE(complex_long_double);
    SIMPLEC_GET_PTR_TYPE(void);

    SIMPLEC_GET_PTR_TYPE_PTR(bool);
    SIMPLEC_GET_PTR_TYPE_PTR(char);
    SIMPLEC_GET_PTR_TYPE_PTR(signed_char);
    SIMPLEC_GET_PTR_TYPE_PTR(short);
    SIMPLEC_GET_PTR_TYPE_PTR(int);
    SIMPLEC_GET_PTR_TYPE_PTR(long);
    SIMPLEC_GET_PTR_TYPE_PTR(long_long);
    SIMPLEC_GET_PTR_TYPE_PTR(unsigned_char);
    SIMPLEC_GET_PTR_TYPE_PTR(unsigned_short);
    SIMPLEC_GET_PTR_TYPE_PTR(unsigned_int);
    SIMPLEC_GET_PTR_TYPE_PTR(unsigned_long);
    SIMPLEC_GET_PTR_TYPE_PTR(unsigned_long_long);
    SIMPLEC_GET_PTR_TYPE_PTR(float);
    SIMPLEC_GET_PTR_TYPE_PTR(double);
    SIMPLEC_GET_PTR_TYPE_PTR(long_double);
    SIMPLEC_GET_PTR_TYPE_PTR(wchar);
    SIMPLEC_GET_PTR_TYPE_PTR(char16);
    SIMPLEC_GET_PTR_TYPE_PTR(char32);
    SIMPLEC_GET_PTR_TYPE_PTR(imag_float);
    SIMPLEC_GET_PTR_TYPE_PTR(imag_double);
    SIMPLEC_GET_PTR_TYPE_PTR(imag_long_double);
    SIMPLEC_GET_PTR_TYPE_PTR(complex_float);
    SIMPLEC_GET_PTR_TYPE_PTR(complex_double);
    SIMPLEC_GET_PTR_TYPE_PTR(complex_long_double);
    SIMPLEC_GET_PTR_TYPE_PTR(void);

    std::shared_ptr<const NamedIntegerType> resolve_signed(
      std::shared_ptr<const NamedIntegerType>
    ) const;
    std::shared_ptr<const NamedIntegerType> resolve_unsigned(
      std::shared_ptr<const NamedIntegerType>
    ) const;
    std::shared_ptr<const FloatingType> resolve_real(
      std::shared_ptr<const FloatingType>
    ) const;
    std::shared_ptr<const FloatingType> resolve_imaginary(
      std::shared_ptr<const FloatingType>
    ) const;
    std::shared_ptr<const FloatingType> resolve_complex(
      std::shared_ptr<const FloatingType>
    ) const;
    std::shared_ptr<const PointerType> resolve_pointer(
      QualifiedType
    ) const;
    std::shared_ptr<const PointerType> resolve_const_pointer(
      QualifiedType
    ) const;
    std::shared_ptr<const Type> resolve_base(
      QualifiedType
    ) const;

    std::shared_ptr<const IntegerType> max_int_type(
      std::shared_ptr<const IntegerType>, std::shared_ptr<const IntegerType>)
      const;
    std::shared_ptr<const FloatingType> max_flt_type(
      std::shared_ptr<const FloatingType>, std::shared_ptr<const FloatingType>)
      const;
    std::shared_ptr<const ArithmeticType> max_type(
      std::shared_ptr<const ArithmeticType>,
      std::shared_ptr<const ArithmeticType>) const;
    std::shared_ptr<const ArithmeticType> arith_common_type(
      std::shared_ptr<const ArithmeticType>,
      std::shared_ptr<const ArithmeticType>) const;
    static std::shared_ptr<const VectorType> arith_common_type(
      std::shared_ptr<const VectorType>,
      std::shared_ptr<const VectorType>);
    std::shared_ptr<const ScalarType> scalar_common_type(
      std::shared_ptr<const ScalarType>,
      std::shared_ptr<const ScalarType>) const;

    std::shared_ptr<const IntegerType> promote_integer(
      std::shared_ptr<const IntegerType> i
    ) const {
      if(!i) return nullptr;
      if(i->is_unsigned()) return max_int_type(get_unsigned_int_ptr(), i);
      return max_int_type(get_int_ptr(), i);
    }

    std::shared_ptr<const ScalarType> promote(
      std::shared_ptr<const ScalarType> s
    ) const {
      auto i = std::dynamic_pointer_cast<const IntegerType>(s);
      if(i) return promote_integer(i);
      return s;
    }

    std::shared_ptr<const IntegerType> get_signed_fit(uint64_t) const;
    std::shared_ptr<const IntegerType> get_unsigned_fit(uint64_t) const;

    FundamentalTypeSet(
      NamedIntegerType ct,
      NamedIntegerType sct,
      NamedIntegerType st,
      NamedIntegerType it,
      NamedIntegerType lt,
      NamedIntegerType llt,
      FloatingType ft,
      FloatingType dt,
      FloatingType ldt,
      uint8_t wchar_needed_bits = 16
    )
    : bool_type(),
    char_type(std::move(ct)),
    signed_char_type(std::move(sct)),
    short_type(std::move(st)),
    int_type(std::move(it)),
    long_type(std::move(lt)),
    long_long_type(std::move(llt)),
    unsigned_char_type(char_type.to_unsigned()),
    unsigned_short_type(short_type.to_unsigned()),
    unsigned_int_type(int_type.to_unsigned()),
    unsigned_long_type(long_type.to_unsigned()),
    unsigned_long_long_type(long_long_type.to_unsigned()),
    float_type(ft),
    double_type(dt),
    long_double_type(ldt),
    imag_float_type(ft.to_imaginary()),
    imag_double_type(dt.to_imaginary()),
    imag_long_double_type(ldt.to_imaginary()),
    complex_float_type(ft.to_complex()),
    complex_double_type(dt.to_complex()),
    complex_long_double_type(ldt.to_complex()),
    SIMPLEC_INIT_PTR_TYPE(bool),
    SIMPLEC_INIT_PTR_TYPE(char),
    SIMPLEC_INIT_PTR_TYPE(signed_char),
    SIMPLEC_INIT_PTR_TYPE(short),
    SIMPLEC_INIT_PTR_TYPE(int),
    SIMPLEC_INIT_PTR_TYPE(long),
    SIMPLEC_INIT_PTR_TYPE(long_long),
    SIMPLEC_INIT_PTR_TYPE(unsigned_char),
    SIMPLEC_INIT_PTR_TYPE(unsigned_short),
    SIMPLEC_INIT_PTR_TYPE(unsigned_int),
    SIMPLEC_INIT_PTR_TYPE(unsigned_long),
    SIMPLEC_INIT_PTR_TYPE(unsigned_long_long),
    SIMPLEC_INIT_PTR_TYPE(float),
    SIMPLEC_INIT_PTR_TYPE(double),
    SIMPLEC_INIT_PTR_TYPE(long_double),
    SIMPLEC_INIT_PTR_TYPE(imag_float),
    SIMPLEC_INIT_PTR_TYPE(imag_double),
    SIMPLEC_INIT_PTR_TYPE(imag_long_double),
    SIMPLEC_INIT_PTR_TYPE(complex_float),
    SIMPLEC_INIT_PTR_TYPE(complex_double),
    SIMPLEC_INIT_PTR_TYPE(complex_long_double),
    ptr_to_wchar(invalid_type),
    ptr_to_const_wchar(invalid_type),
    ptr_to_char16(invalid_type),
    ptr_to_const_char16(invalid_type),
    ptr_to_char32(invalid_type),
    ptr_to_const_char32(invalid_type),
    ptr_to_void(nullptr),
    ptr_to_const_void({nullptr, const_qualified})
    {
      if(signed_char_type.get_bits() != char_type.get_bits()) throw
        std::runtime_error(
          "signed char must have the same number of bits as char"
        );
      if(short_type.get_bits() < char_type.get_bits()) throw std::runtime_error(
        "sizeof(short int) < sizeof(char)"
      );
      if(int_type.get_bits() < short_type.get_bits()) throw std::runtime_error(
        "sizeof(int) < sizeof(short int)"
      );
      if(long_type.get_bits() < int_type.get_bits()) throw std::runtime_error(
        "sizeof(long int) < sizeof(int)"
      );
      if(long_long_type.get_bits() < long_type.get_bits())
      throw std::runtime_error("sizeof(long long int) < sizeof(long int)");

      if(int_type.get_bits() < 16) std::runtime_error(
        "int must be at least 16 bits wide"
      );

      set_wide_chars(wchar_needed_bits);
    }

    static FundamentalTypeSet get_basic() {
      return FundamentalTypeSet(
        {"char", 8},
        {"signed char", 8},
        {"short int", 16},
        {"int", 32},
        {"long int", 64},
        {"long long int", 64},
        32, 64,
        FloatingType::get_long_double_type(64)
      );
    }


  };

};

#undef SIMPLEC_TO_GET_PTR

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_PARSER_FUNDAMENTAL_TYPE_SET_TESTS_INCLUDED
#define SIMPLEC_PARSER_FUNDAMENTAL_TYPE_SET_TESTS_INCLUDED

#include <simplec/parser/types/pointer_type.hpp>

namespace simplec
{

  TEST_CASE("Basic fundamental type set defined correctly")
  {
    FundamentalTypeSet F = FundamentalTypeSet::get_basic();
    CHECK_EQ(F.get_bool().to_string(), "_Bool");
    CHECK_EQ(F.get_char().to_string(), "char");
    CHECK_EQ(F.get_signed_char().to_string(), "signed char");
    CHECK_EQ(F.get_unsigned_char().to_string(), "unsigned char");
    CHECK_EQ(F.get_short().to_string(), "short int");
    CHECK_EQ(F.get_unsigned_short().to_string(), "unsigned short int");
    CHECK_EQ(F.get_int().to_string(), "int");
    CHECK_EQ(F.get_unsigned_int().to_string(), "unsigned int");
    CHECK_EQ(F.get_long().to_string(), "long int");
    CHECK_EQ(F.get_unsigned_long().to_string(), "unsigned long int");
    CHECK_EQ(F.get_long_long().to_string(), "long long int");
    CHECK_EQ(F.get_unsigned_long_long().to_string(), "unsigned long long int");
    CHECK_EQ(F.get_float().to_string(), "float");
    CHECK_EQ(F.get_double().to_string(), "double");
    CHECK_EQ(F.get_long_double().to_string(), "long double");
    CHECK_LE(F.get_wchar().get_bits(), 32);
    CHECK_GE(F.get_wchar().get_bits(), F.get_char().get_bits());
    CHECK_EQ(F.get_char16().get_bits(), 16);
    CHECK_EQ(F.get_char32().get_bits(), 32);
  }

  TEST_CASE("Variations of fundamental types are resolved correctly")
  {
    FundamentalTypeSet F = FundamentalTypeSet::get_basic();

    CHECK_EQ(F.resolve_signed(F.get_unsigned_int_ptr()), F.get_int_ptr());
    CHECK_EQ(
      F.resolve_unsigned(F.get_signed_char_ptr()), F.get_unsigned_char_ptr()
    );
    CHECK_EQ(
      F.resolve_complex(F.get_double_ptr()), F.get_complex_double_ptr()
    );
    CHECK_EQ(
      F.resolve_signed(F.get_unsigned_long_ptr()), F.get_long_ptr()
    );
    CHECK_EQ(
      F.resolve_complex(F.get_imag_double_ptr()), F.get_complex_double_ptr()
    );
  }

  TEST_CASE("Common types are computed correctly")
  {
    FundamentalTypeSet F = FundamentalTypeSet::get_basic();

    CHECK_EQ(
      F.scalar_common_type(F.get_char_ptr(), F.get_char_ptr()),
      F.get_char_ptr()
    );
    CHECK_EQ(
      F.scalar_common_type(F.get_long_ptr(), F.get_int_ptr()),
      F.get_long_ptr()
    );
    CHECK_EQ(
      F.scalar_common_type(F.get_float_ptr(), F.get_unsigned_long_long_ptr()),
      F.get_float_ptr()
    );
    CHECK_EQ(
      F.scalar_common_type(F.get_unsigned_int_ptr(), F.get_int_ptr()),
      F.get_int_ptr()
    );
    CHECK_EQ(
      F.scalar_common_type(F.get_unsigned_int_ptr(), F.get_unsigned_long_ptr()),
      F.get_unsigned_long_ptr()
    );
    CHECK_EQ(
      F.scalar_common_type(F.get_float_ptr(), F.get_imag_double_ptr()),
      F.get_complex_double_ptr()
    );

    PointerType test_ptr{F.get_int_ptr()};

    CHECK_EQ(
      F.scalar_common_type(false_share(test_ptr), F.get_int_ptr()),
      false_share(test_ptr)
    );

    CHECK_EQ(
      F.scalar_common_type(false_share(test_ptr), false_share(test_ptr)),
      nullptr
    );
  }

}

#endif
#endif
