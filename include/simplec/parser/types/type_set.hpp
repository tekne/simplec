#ifndef SIMPLEC_PARSER_TYPE_SET_INCLUDED
#define SIMPLEC_PARSER_TYPE_SET_INCLUDED

#include <simplec/utility/false_share.hpp>

#include <simplec/parser/types/fundamental_type_set.hpp>
#include <simplec/parser/types/structure_type.hpp>
#include <simplec/parser/types/union_type.hpp>
#include <simplec/parser/types/type.hpp>

#include <unordered_map>
#include <memory>
#include <string>
#include <exception>

namespace simplec
{


  class TypeSet: public FundamentalTypeSet
  {
    std::unordered_map<std::string, StructureType> tagged_structs;
    std::unordered_map<std::string, UnionType> tagged_unions;
    std::unordered_map<std::string, QualifiedType> typedefs;
  public:
    TypeSet(FundamentalTypeSet F):
    FundamentalTypeSet(std::move(F)),
    tagged_structs{},
    tagged_unions{},
    typedefs{
      {"wchar_t", get_wchar_ptr()},
      {"char16_t", get_char16_ptr()},
      {"char32_t", get_char32_ptr()}
    }
    {}

    std::shared_ptr<const StructureType> register_struct(std::string tag) {
      auto it = tagged_structs.insert({std::move(tag), StructureType()});
      if(it.second) {
        it.first->second.set_tag(it.first->first.c_str());
      }
      return false_share(it.first->second);
    }
    std::shared_ptr<const StructureType> register_struct(
      std::string tag, StructureType S
    ) {
      if(!S.is_complete()) return register_struct(tag);
      auto it = tagged_structs.insert({std::move(tag), S});

      if(!it.second)
      {
        if(it.first->second.is_complete())
          throw std::runtime_error("Struct redefinition!");
        it.first->second.define(std::move(S));
      }
      return false_share(it.first->second);
    }

    std::shared_ptr<const UnionType> register_union(std::string tag) {
      auto it = tagged_unions.insert({std::move(tag), UnionType()});
      if(it.second) {
        it.first->second.set_tag(it.first->first.c_str());
      }
      return false_share(it.first->second);
    }
    std::shared_ptr<const UnionType> register_union(
      std::string tag, UnionType U
    ) {
      if(!U.is_complete()) return register_union(tag);
      auto it = tagged_unions.insert({std::move(tag), U});

      if(!it.second)
      {
        if(it.first->second.is_complete())
          throw std::runtime_error("Union redefinition!");
        it.first->second.define(std::move(U));
      }
      return false_share(it.first->second);
    }

    TypeSet& register_typedef(
      std::string name, QualifiedType typ
    ) {
      auto it = typedefs.insert({std::move(name), std::move(typ)});
      if(!it.second) throw std::runtime_error("Typedef redefinition!");
      return *this;
    }

    std::shared_ptr<const StructureType> resolve_struct(const std::string& t)
    {
      auto it = tagged_structs.find(t);
      if(it == tagged_structs.end()) return nullptr;
      return false_share(it->second);
    }

    std::shared_ptr<const UnionType> resolve_union(const std::string& t)
    {
      auto it = tagged_unions.find(t);
      if(it == tagged_unions.end()) return nullptr;
      return false_share(it->second);
    }

    QualifiedType resolve_typedef(const std::string& t)
    {
      auto it = typedefs.find(t);
      if(it == typedefs.end()) return invalid_type;
      return it->second;
    }

    bool is_typedef(const std::string& t)
    {
      return typedefs.find(t) != typedefs.end();
    }

  };

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_PARSER_TYPE_SET_TESTS_INCLUDED
#define SIMPLEC_PARSER_TYPE_SET_TESTS_INCLUDED

namespace simplec
{

  TEST_CASE("Registering typedefs into a TypeSet works properly")
  {
    TypeSet T(FundamentalTypeSet::get_basic());
    CHECK_UNARY(T.resolve_typedef("uint32_t").is_invalid());
    T.register_typedef("uint32_t", T.get_int_ptr());
    CHECK_EQ(T.resolve_typedef("uint32_t").raw_type(), T.get_int_ptr());
  }

  TEST_CASE("Registering structs and unions into a TypeSet works properly")
  {
    TypeSet T(FundamentalTypeSet::get_basic());

    CHECK_UNARY_FALSE(T.resolve_struct("tag"));
    auto sptr2 = T.register_struct("tag");
    auto sptr = T.resolve_struct("tag");

    REQUIRE_UNARY(sptr);
    CHECK_EQ(sptr, sptr2);
    CHECK_UNARY_FALSE(sptr->is_complete());
    CHECK_EQ(sptr->to_string(), "struct tag");

    StructureType S(
      {{"x", {T.get_int_ptr()}}}
    );
    T.register_struct("tag", S);
    CHECK_UNARY(sptr->is_complete());
    CHECK_EQ(sptr->to_string(), "struct tag {int x;}");

    CHECK_UNARY_FALSE(T.resolve_union("tag"));
    auto uptr2 = T.register_union("tag");
    auto uptr = T.resolve_union("tag");

    REQUIRE_UNARY(uptr);
    CHECK_EQ(uptr, uptr2);
    CHECK_UNARY_FALSE(uptr->is_complete());
    CHECK_EQ(uptr->to_string(), "union tag");

    UnionType U(
      {{"y", {T.get_short_ptr()}}, {"z", {T.get_long_ptr()}}}
    );
    T.register_union("tag", U);
    CHECK_UNARY(uptr->is_complete());
    CHECK_EQ(uptr->to_string(), "union tag {short int y; long int z;}");
  }

}

#endif
#endif
