#ifndef SIMPLEC_TYPE_INCLUDED
#define SIMPLEC_TYPE_INCLUDED

#include <cstdint>
#include <string>
#include <memory>
#include <ostream>
#include <sstream>

namespace simplec
{

  class Type
  {
    uint64_t fa;
  public:
    Type(uint64_t f = 0): fa(f) {}

    // Stringification
    virtual const Type& print_to_stream(std::ostream& str) const {
      str << "(unknown-type)"; return *this;
    }
    std::string to_string() const {
      std::stringstream sstr;
      print_to_stream(sstr);
      return sstr.str();
    }

    // Comparison
    virtual bool equal_type(const Type* t) const=0;
    virtual bool compatible_type(const Type* t) const {
      return equal_type(t);
    }
    bool equal_type(std::shared_ptr<const Type> p) const {
      return equal_type(p.get());
    }
    bool compatible_type(std::shared_ptr<const Type> p) const {
      return compatible_type(p.get());
    }

    virtual bool operator==(const Type& t) const {
      return equal_type(&t);
    }
    bool operator!=(const Type& t) const {
      return !equal_type(&t);
    }

    // Whether a type is complete or not

    // Complete types are required to have a defined, unchanging size at
    // compile-time unless they are Variable Length Arrays (VLAs)
    virtual bool is_complete() const=0;
    virtual bool is_vla() const=0;
    virtual bool is_array() const {return false;}

    // Forced alignment requirements for the type.
    // Used to compute target-specific alignment requirements
    uint64_t forced_alignment() const {return fa;}

    // Type qualifications
    virtual bool is_const() const {return false;}
    virtual bool is_volatile() const {return false;}
    virtual bool is_restrict() const {return false;}
    virtual bool is_cv() const {return false;}
    virtual bool is_qualified() const {return false;}

    // Virtual destructor
    virtual ~Type() {}
  };

  class TypeQualification
  {
    char qualifications;
  public:
    constexpr TypeQualification(bool c, bool v, bool r, bool i = false)
    : qualifications(c | (v << 1) | (r << 2) | (i << 7)) {}
    constexpr TypeQualification(char v = 0): qualifications(v) {}

    constexpr TypeQualification operator|(TypeQualification t) const {
      return {static_cast<char>(qualifications | t.qualifications)};
    }
    constexpr TypeQualification operator&(TypeQualification t) const {
      return {static_cast<char>(qualifications & t.qualifications)};
    }
    constexpr TypeQualification& operator|=(TypeQualification t) {
      return (*this = *this | t);
    }
    constexpr TypeQualification& operator&=(TypeQualification t) {
      return (*this = *this & t);
    }
    constexpr bool operator==(TypeQualification t) const {
      return qualifications == t.qualifications;
    }
    constexpr bool operator!=(TypeQualification t) const {
      return qualifications != t.qualifications;
    }
    constexpr bool is_const() const {return qualifications & 0x1;}
    constexpr bool is_volatile() const {return qualifications & 0x2;}
    constexpr bool is_restrict() const {return qualifications & 0x4;}
    constexpr bool is_cv() const {return qualifications & 0x3;}
    constexpr bool is_qualified() const {return qualifications;}
    constexpr bool is_invalid() const {return qualifications & 0x80;}
    constexpr bool is_valid() const {return !is_invalid();}

    TypeQualification& set_valid() {
      if(is_invalid()) qualifications ^= 0x80; return *this;
    }
    TypeQualification& set_invalid() {
      if(is_valid()) qualifications ^= 0x80; return *this;
    }

    const TypeQualification& print_to_stream(std::ostream& str) const
    {
      if(is_const()) str << "const";
      if(is_volatile())
      {
        if(is_const()) str << " ";
        str << "volatile";
      }
      if(is_restrict())
      {
        if(is_const() || is_volatile()) str << " ";
        str << "restrict";
      }
      if(is_invalid())
      {
        if(is_qualified()) str << " ";
        str << "(INVALID)";
      }
      return *this;
    }

    std::string to_string() const {
      std::stringstream sstr;
      print_to_stream(sstr);
      return sstr.str();
    }
  };

  static TypeQualification const_qualified(true, false, false);
  static TypeQualification volatile_qualified(false, true, false);
  static TypeQualification restrict_qualified(false, false, true);
  static TypeQualification invalid_qualified(false, false, false, true);

  class QualifiedType: public Type
  {
    std::shared_ptr<const Type> type;
    TypeQualification qual;
  public:
    QualifiedType(
      std::shared_ptr<const Type> t,
      TypeQualification q,
      size_t fa = 0
    )
    : Type(fa), type(std::move(t)), qual(q) {}
    template<class T>
    QualifiedType(std::shared_ptr<T> t): type(std::move(t)), qual(0) {}
    QualifiedType(std::nullptr_t): type(nullptr), qual(0) {}

    std::shared_ptr<const Type> raw_type() const {return type;}

    TypeQualification get_qual() const {return qual;}
    QualifiedType& set_qual(TypeQualification q) {
      qual = q; return *this;
    }
    QualifiedType& set_valid() {qual.set_valid(); return *this;}
    QualifiedType& set_invalid() {qual.set_invalid(); return *this;}

    bool operator==(QualifiedType qt) const {
      if(qt.get_qual() != qual) return false;
      if(!type) return !qt.raw_type();
      return type->equal_type(qt.raw_type());
    }
    bool operator!=(QualifiedType qt) const {
      return !(*this == qt);
    }

    bool is_const() const {return qual.is_const();}
    bool is_volatile() const {return qual.is_volatile();}
    bool is_restrict() const {return qual.is_restrict();}
    bool is_cv() const {return qual.is_cv();}
    bool is_qualified() const {return qual.is_qualified();}
    bool is_invalid() const {return qual.is_invalid();}
    bool is_valid() const {return qual.is_valid();}

    bool is_void() const {return !type;}

    const QualifiedType& print_to_stream(std::ostream& str) const {
      if(type) type->print_to_stream(str);
      else str << "void";

      if(is_qualified())
      {
        str << " ";
        qual.print_to_stream(str);
      }
      return *this;
    }
    std::string to_string() const {
      std::stringstream sstr;
      print_to_stream(sstr);
      return sstr.str();
    }

    bool is_complete() const {return type && type->is_complete();}
    bool is_vla() const {return type && type->is_vla();}

    bool equal_type(const Type* t) const {
      if(!type) return !t;
      return type->equal_type(t);
    }
    bool compatible_type(const Type* t) const {
      if(!type) return !t;
      return type->compatible_type(t);
    }
    bool compatible_type(QualifiedType t) const {
      if(!type) return t.is_void();
      return type->compatible_type(&t);
    }
  };

  static QualifiedType invalid_type {nullptr, invalid_qualified};

}

#endif
