#ifndef SIMPLEC_ARRAY_TYPE_INCLUDED
#define SIMPLEC_ARRAY_TYPE_INCLUDED

#include <memory>
#include <utility>

#include <simplec/parser/types/aggregate_type.hpp>
#include <simplec/parser/types/pointer_type.hpp>

namespace simplec
{

  class ArrayType: public AggregateType
  {
    std::shared_ptr<const Type> rt; // Referenced type
    uint64_t count;
    bool iv;
  public:

    ArrayType(
      std::shared_ptr<const Type> r,
      uint64_t c = -1,
      bool iv = false,
      uint64_t fa = 0
    )
    : AggregateType(fa), rt(std::move(r)) , count(c), iv(iv) {
      if(!rt) throw std::runtime_error(
        "Cannot have array of void"
      );
      if(!rt->is_complete()) throw std::runtime_error(
        "Cannot have array of incomplete type"
      );
    }

    const Type& print_to_stream(std::ostream& str) const {
      rt->print_to_stream(str);
      str << "[" << count << "]";
      return *this;
    }

    std::shared_ptr<const Type> base_type() const {
      return rt;
    }

    const PointerType decay_to_pointer() const {
      return PointerType(base_type());
    }

    bool equal_type(const Type* t) const final {
      const ArrayType* at = dynamic_cast<const ArrayType*>(t);
      if(!at) return false;
      if(forced_alignment() != at->forced_alignment()) return false;
      if(count != at->count) return false;
      if(iv != at->iv) return false;
      return rt->equal_type(at->base_type());
    }
    bool is_complete() const final {
      return count != -1 && !iv;
    }
    bool is_vla() const final {return iv;}
    bool is_array() const final {return true;}

    uint64_t array_size() const {return count;}
  };

}

#endif
