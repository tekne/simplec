#ifndef SIMPLEC_INTEGER_TYPE_INCLUDED
#define SIMPLEC_INTEGER_TYPE_INCLUDED

#include <cstdint>
#include <memory>
#include <string>

#include <simplec/parser/types/arithmetic_type.hpp>

namespace simplec
{

  class IntegerType: public ArithmeticType
  {
    uint32_t bits; // To support all integer types LLVM does
    bool sg;
  public:
    IntegerType(uint32_t b, bool s = true, uint64_t fa = 0):
    ArithmeticType(fa), bits(b), sg(s) {}

    const Type& print_to_stream(std::ostream& str) const {
      str << (sg ? "__i" : "__u") << bits;
      return *this;
    }

    uint32_t get_bits() const {return bits;}

    arithmetic_type_domain type_domain() const final {return real_domain;}
    bool is_signed() const {return sg;}
    bool is_unsigned() const {return !sg;}
    virtual bool is_named() const=0;

    virtual bool operator<(const IntegerType&) const=0;
    virtual bool operator<=(const IntegerType&) const=0;
    bool operator>(const IntegerType& i) const {
      return !(*this <= i);
    }
    bool operator>=(const IntegerType& i) const {
      return !(*this < i);
    }

    virtual std::shared_ptr<const IntegerType> to_unsigned_ptr() const=0;
    virtual std::shared_ptr<const IntegerType> to_signed_ptr() const=0;

    static bool is_signed_int(std::shared_ptr<const Type> i)
    {
      auto p = dynamic_cast<const IntegerType*>(i.get());
      return p && p->is_signed();
    }

    static bool is_unsigned_int(std::shared_ptr<const Type> i)
    {
      auto p = dynamic_cast<const IntegerType*>(i.get());
      return p && p->is_unsigned();
    }

    static bool is_int(std::shared_ptr<const Type> i)
    {
      return dynamic_cast<const IntegerType*>(i.get());
    }

  };

  class BooleanType: public IntegerType
  {
  public:
    BooleanType(): IntegerType(1, false) {}
    const Type& print_to_stream(std::ostream& str) const {
      str << "_Bool";
      return *this;
    }
    bool is_named() const {return true;}
    bool operator<(const IntegerType& i) const {
      return !dynamic_cast<const BooleanType*>(&i);
    }
    bool operator<=(const IntegerType& i) const {return true;}
    bool equal_type(const Type* t) const {
      return dynamic_cast<const BooleanType*>(t);
    }
    std::shared_ptr<const IntegerType> to_unsigned_ptr() const {
      return std::make_shared<const BooleanType>();
    }
    std::shared_ptr<const IntegerType> to_signed_ptr() const {
      return nullptr;
    }
  };

  //TODO: add enumerations

  class NamedIntegerType: public IntegerType
  {
    std::string name;
    uint8_t rank;
  public:
    NamedIntegerType(
      std::string n, uint32_t bits, bool sg = true, uint8_t r = 0
    ): IntegerType(bits, sg), name(n), rank(r) {}

    const std::string& get_name() const {return name;}
    const Type& print_to_stream(std::ostream& str) const {
      if(is_unsigned()) str << "unsigned ";
      str << get_name();
      return *this;
    }
    uint8_t get_rank() const {return rank;}
    bool is_named() const {return true;}

    bool operator==(const NamedIntegerType& n) const {
      return n.get_name() == get_name()
      && n.get_bits() == get_bits()
      && n.is_signed() == is_signed();
    }
    bool operator<(const IntegerType& i) const {
      if(get_bits() < i.get_bits()) return true;
      if(get_bits() > i.get_bits()) {
        return false;
      }
      if(i.is_signed() && !is_signed()) return true;
      if(is_signed() && !i.is_signed()) {
        return false;
      }
      if(i.is_named()) {
        const NamedIntegerType& n = dynamic_cast<const NamedIntegerType&>(i);
        if(get_rank() < n.get_rank()) return true;
      }
      return false;
    }
    bool operator<=(const IntegerType& i) const {
      if(get_bits() < i.get_bits()) return true;
      if(i.get_bits() < get_bits()) return false;

      if(i.is_named()) {
        const NamedIntegerType& n = dynamic_cast<const NamedIntegerType&>(i);
        if(get_rank() <= n.get_rank()) return true;
      }
      return false;
    }

    bool equal_type(const Type* t) const {
      const NamedIntegerType* nt = dynamic_cast<const NamedIntegerType*>(t);
      if(!nt) return false;
      return *nt == *this;
    }
    NamedIntegerType to_signed() const {
      return NamedIntegerType(get_name(), get_bits(), true, forced_alignment());
    }
    NamedIntegerType to_unsigned() const {
      return NamedIntegerType(
        get_name(), get_bits(), false, forced_alignment()
      );
    }
    std::shared_ptr<const IntegerType> to_unsigned_ptr() const {
      return std::make_shared<const NamedIntegerType>(to_unsigned());
    }
    std::shared_ptr<const IntegerType> to_signed_ptr() const {
      return std::make_shared<const NamedIntegerType>(to_signed());
    }
  };

  class ExtendedIntegerType: public IntegerType
  {
  public:
    ExtendedIntegerType(uint32_t bits, bool sg = true, uint64_t fa = 0):
    IntegerType(bits, sg, fa) {}

    bool operator==(const ExtendedIntegerType& e) const {
      return e.get_bits() == get_bits() && e.is_signed() == is_signed();
    }
    bool equal_type(const Type* t) const {
      const ExtendedIntegerType* et =
        dynamic_cast<const ExtendedIntegerType*>(t);
      if(!et) return false;
      return *et == *this;
    }
    bool operator<(const IntegerType& i) const {
      if(get_bits() < i.get_bits()) return true;
      if(get_bits() > i.get_bits()) return false;
      if(i.is_named()) return false;
      return false;
    }
    bool operator<=(const IntegerType& i) const {
      if(get_bits() < i.get_bits()) return true;
      if(get_bits() > i.get_bits()) return false;
      if(i.is_named()) return false;
      return true;
    }
    bool is_named() const {return false;}

    ExtendedIntegerType to_signed() const {
      return ExtendedIntegerType(get_bits(), true, forced_alignment());
    }
    ExtendedIntegerType to_unsigned() const {
      return ExtendedIntegerType(get_bits(), false, forced_alignment());
    }
    std::shared_ptr<const IntegerType> to_unsigned_ptr() const {
      return std::make_shared<const ExtendedIntegerType>(to_unsigned());
    }
    std::shared_ptr<const IntegerType> to_signed_ptr() const {
      return std::make_shared<const ExtendedIntegerType>(to_signed());
    }
  };

}

#endif
