#ifndef SIMPLEC_TYPE_ID_MAP_INCLUDED
#define SIMPLEC_TYPE_ID_MAP_INCLUDED

#include <boost/container/small_vector.hpp>
#include <boost/container/flat_map.hpp>
#include <simplec/parser/types/type.hpp>
#include <utility>
#include <ostream>
#include <vector>
#include <exception>

namespace simplec
{

  class TypeIDMap
  {
  protected:
    static const size_t sv_size = 5;
    boost::container::small_vector<QualifiedType, sv_size> members;
    boost::container::flat_map<std::string, size_t> identifiers;
    std::vector<size_t> anonymous;
    boost::container::small_vector<size_t, sv_size> mapback;
  public:

    TypeIDMap& define_map(boost::container::small_vector<
      std::pair<std::string, QualifiedType>, sv_size
    >);
    TypeIDMap& define_map(TypeIDMap M)
    {
      members = std::move(M.members);
      identifiers = std::move(M.identifiers);
      anonymous = std::move(M.anonymous);
      mapback = std::move(M.mapback);
      return *this;
    }

    TypeIDMap(): members{}, identifiers{}, anonymous{}, mapback{} {}
    TypeIDMap(
      boost::container::small_vector<
        std::pair<std::string, QualifiedType>, sv_size
      > def): TypeIDMap()
    {
      define_map(std::move(def));
    }

    bool compatible_map(const TypeIDMap&) const;
    bool equal_map(const TypeIDMap&) const;

    const TypeIDMap& map_to_stream(std::ostream&) const;

    size_t get_position(const std::string& id) const {
      auto iter = identifiers.find(id);
      if(iter == identifiers.end()) return -1;
      return iter->second;
    }

    QualifiedType get_type(size_t p) const {
      return members[p];
    }

    const QualifiedType* get_type_ptr(const std::string& id) const {
      size_t p = get_position(id);
      if(p == -1) return nullptr;
      return &members[p];
    }

    QualifiedType get_type(const std::string& id) const
    {
      size_t p = get_position(id);
      if(p == -1) throw std::runtime_error("Invalid identifier " + id);
      return members[p];
    }

    std::pair<const QualifiedType*, TypeQualification>
    recursive_get_type_ptr(const std::string& id) const {

      auto tp = get_type_ptr(id);
      if(tp) return {tp, TypeQualification()};

      for(auto a : anonymous)
      {
        const TypeIDMap* M = dynamic_cast<const TypeIDMap*>(
          members[a].raw_type().get()
        );
        if(!M) continue;
        auto ptr = M->recursive_get_type_ptr(id);
        if(ptr.first) return {ptr.first, ptr.second | members[a].get_qual()};
      }
      return {nullptr, 0};
    }

    QualifiedType recursive_get_type(const std::string& id) const {
      auto ptr = recursive_get_type_ptr(id);
      if(!ptr.first) throw std::runtime_error("Invalid identifier " + id);
      return {ptr.first->raw_type(), ptr.first->get_qual() | ptr.second};
    }

    virtual ~TypeIDMap() {}

  };


}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_TYPE_ID_MAP_TESTS_INCLUDED
#define SIMPLEC_TEST_ID_MAP_TESTS_INCLUDED

#include <simplec/parser/types/fundamental_type_set.hpp>

namespace simplec
{

  TEST_CASE("TypeIDMaps compute positions correctly")
  {
    FundamentalTypeSet F = FundamentalTypeSet::get_basic();
  }

}

#endif
#endif
