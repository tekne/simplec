#ifndef SIMPLEC_FUNCTION_TYPE_INCLUDED
#define SIMPLEC_FUNCTION_TYPE_INCLUDED

#include <memory>
#include <sstream>
#include <string>
#include <exception>
#include <utility>
#include <boost/container/small_vector.hpp>

#include <simplec/parser/types/type.hpp>

namespace simplec
{

  class FunctionType: public Type
  {
    static const size_t sv_size = 5;

    QualifiedType return_typ;
    boost::container::small_vector<QualifiedType, sv_size> arguments;
    bool varargs;
  public:
    FunctionType(
      QualifiedType rt,
      boost::container::small_vector<QualifiedType, sv_size> args = {},
      bool va = false
    ): Type(), return_typ(rt), arguments(std::move(args)), varargs(va)
    {}

    const Type& print_to_stream(std::ostream& str) const {
      str << "((";
      if(!arguments.empty())
      {
        str << arguments[0].to_string();
        for(size_t i = 1; i < arguments.size(); i++)
        {
          str << ", " + arguments[i].to_string();
        }
        if(varargs)
        {
          str << ", ";
        }
      }
      if(varargs) str << "...";
      str << ") -> " + return_typ.to_string() + ")";
      return *this;
    }

    QualifiedType return_type() const {return return_typ;}
    std::shared_ptr<const Type>  raw_return_type() const {
      return return_typ.raw_type();
    }
    const auto& parameter_list() const {
      return arguments;
    }
    size_t parameter_count() const {return arguments.size();}
    bool has_varargs() const {return varargs;}

    bool equal_type(const Type* t) const final {
      const FunctionType* ft = dynamic_cast<const FunctionType*>(t);
      if(!ft) return false;
      if(parameter_count() != ft->parameter_count()) return false;
      if(return_type() != ft->return_type()) return false;
      for(size_t i = 0; i < arguments.size(); i++)
      {
        if(arguments[i] != ft->arguments[i]) return false;
      }
      return true;
    }

    bool is_complete() const final {return false;}
    bool is_vla() const final {return false;}

  };

}

#endif
