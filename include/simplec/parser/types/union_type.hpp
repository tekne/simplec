#ifndef SIMPLEC_UNION_TYPE_INCLUDED
#define SIMPLEC_UNION_TYPE_INCLUDED

#include <boost/container/small_vector.hpp>
#include <memory>
#include <utility>
#include <string>
#include <exception>
#include <sstream>

#include <simplec/parser/types/aggregate_type.hpp>

namespace simplec
{

  class UnionType: public Type, public TypeIDMap
  {
    typedef std::pair<std::string, QualifiedType> struct_name_pair;

    const char* tag;
    bool complete;
  public:
    UnionType(const char* t = nullptr, uint64_t fa = 0)
    : Type(fa), TypeIDMap{}, tag(t), complete(false) {}
    UnionType(
      boost::container::small_vector<struct_name_pair, sv_size> e,
      const char* tg = nullptr,
      uint64_t fa = 0)
    : Type(fa), TypeIDMap{std::move(e)}, tag(tg), complete(true) {}

    const Type& print_to_stream(std::ostream& str) const {
      if(!tag) str << "union";
      else str << "union " << tag;
      if(!is_complete()) return *this;
      str << " ";
      map_to_stream(str);
      return *this;
    }

    bool is_compatible(const UnionType& s) const {
      if(s.forced_alignment() != forced_alignment()) return false;
      if(s.tag != tag) return false;
      if(!(s.complete && complete)) return true;
      return compatible_map(s);
    }

    bool is_equal(const UnionType& s) const {
      if(s.forced_alignment() != forced_alignment()) return false;
      if(s.tag != tag) return false;
      if(!(s.complete && complete)) return true;
      return equal_map(s);
    }

    bool equal_type(const Type* t) const final {
      const UnionType* ut = dynamic_cast<const UnionType*>(t);
      if(!ut) return false;
      return is_equal(*ut);
    }
    bool compatible_type(const Type* t) const final {
      const UnionType* ut = dynamic_cast<const UnionType*>(t);
      if(!ut) return false;
      return is_compatible(*ut);
    }

    bool is_complete() const final {
      return complete;
    }
    bool is_vla() const final {
      return false; //TODO: think about this...
    }

    template<size_t N>
    UnionType& define(
      boost::container::small_vector<struct_name_pair, N> definition
    ) {
      define_map(std::move(definition));
      complete = true;
      return *this;
    }
    UnionType& define(
      UnionType def
    ) {
      define_map(std::move(def));
      complete = true;
      return *this;
    }
    UnionType& set_tag(const char* t) {tag = t; return *this;}
    UnionType& set_tag(const std::string& t) {return set_tag(t.c_str());}

  };


}

#endif
