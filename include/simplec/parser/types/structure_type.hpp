#ifndef SIMPLEC_STRUCTURE_TYPE_INCLUDED
#define SIMPLEC_STRUCTURE_TYPE_INCLUDED

#include <boost/container/small_vector.hpp>
#include <memory>
#include <utility>
#include <string>
#include <exception>
#include <sstream>

#include <simplec/parser/types/aggregate_type.hpp>
#include <simplec/parser/types/type_id_map.hpp>

namespace simplec
{

  class StructureType: public AggregateType, public TypeIDMap
  {
    typedef std::pair<std::string, QualifiedType> struct_name_pair;

    const char* tag;
    bool complete;
  public:
    StructureType(const char* t = nullptr, uint64_t fa = 0)
    : AggregateType(fa), TypeIDMap{}, tag(t), complete(false) {}
    StructureType(
      boost::container::small_vector<struct_name_pair, sv_size> e,
      const char* tg = nullptr,
      uint64_t fa = 0)
    : AggregateType(fa), TypeIDMap{std::move(e)}, tag(tg), complete(true) {}

    const Type& print_to_stream(std::ostream& str) const {
      if(!tag) str << "struct";
      else str << "struct " << tag;
      if(!is_complete()) return *this;
      str << " ";
      map_to_stream(str);
      return *this;
    }

    bool is_compatible(const StructureType& s) const {
      if(s.forced_alignment() != forced_alignment()) return false;
      if(s.tag != tag) return false;
      if(!(s.complete && complete)) return true;
      return compatible_map(s);
    }

    bool is_equal(const StructureType& s) const {
      if(s.forced_alignment() != forced_alignment()) return false;
      if(s.tag != tag) return false;
      if(!(s.complete && complete)) return true;
      return equal_map(s);
    }

    bool equal_type(const Type* t) const final {
      const StructureType* st = dynamic_cast<const StructureType*>(t);
      if(!st) return false;
      return is_equal(*st);
    }
    bool compatible_type(const Type* t) const final {
      const StructureType* st = dynamic_cast<const StructureType*>(t);
      if(!st) return false;
      return is_compatible(*st);
    }

    bool is_complete() const final {
      return complete;
    }
    bool is_vla() const final {
      return false; //TODO: think about this...
    }

    template<size_t N>
    StructureType& define(
      boost::container::small_vector<struct_name_pair, N> definition
    ) {
      define_map(std::move(definition));
      complete = true;
      return *this;
    }
    StructureType& define(
      StructureType def
    ) {
      define_map(std::move(def));
      complete = true;
      return *this;
    }
    StructureType& set_tag(const char* t) {tag = t; return *this;}
    StructureType& set_tag(const std::string& t) {return set_tag(t.c_str());}

  };

}

#endif
