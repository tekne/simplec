#ifndef SIMPLEC_FLOATING_TYPES_INCLUDED
#define SIMPLEC_FLOATING_TYPES_INCLUDED

#include <simplec/parser/types/arithmetic_type.hpp>
#include <cstdint>

namespace simplec
{

  enum floating_point_type_domain : char {
    real_floating_domain = 0, imaginary_floating_domain = 1,
    complex_floating_domain = 2
  };

  class FloatingType: public ArithmeticType
  {
    uint8_t bits;
    floating_point_type_domain domain;
    bool lng;
  public:
    FloatingType(
      uint8_t b,
      floating_point_type_domain d = real_floating_domain,
      bool l = false,
      uint64_t fa = 0)
    :  ArithmeticType(fa), bits(b), domain(d), lng(l) {}

    static FloatingType get_long_double_type(uint8_t b) {
      return FloatingType(b, real_floating_domain, true, 0);
    }

    std::string base_name() const {
      if(lng) return "long double";
      switch(bits)
      {
        case 32: return "float";
        case 64: return "double";
        default: return "__f" + std::to_string(bits);
      }
    }

    const Type& print_to_stream(std::ostream& str) const {
      switch(domain)
      {
      case real_floating_domain:
        str << base_name(); break;
      case imaginary_floating_domain:
        str << "_Imaginary " << base_name(); break;
      case complex_floating_domain:
        str << "_Complex " << base_name(); break;
      default:
        str << "(invalid-domain) " << base_name(); break;
      }
      return *this;
    }

    bool operator==(const FloatingType& t) const {
      return bits == t.bits && domain == t.domain && t.lng == lng
      && forced_alignment() == t.forced_alignment();;
    }
    bool equal_type(const Type* t) const final {
      const FloatingType* it = dynamic_cast<const FloatingType*>(t);
      if(!it) return false;
      return *this == *it;
    }

    uint8_t get_bits() const {return bits;}

    arithmetic_type_domain type_domain() const final {
      if(domain == real_floating_domain) return real_domain;
      return complex_domain;
    }
    bool is_imaginary() const {
      return domain == imaginary_floating_domain;
    }

    FloatingType to_real() const {
      return FloatingType(bits, real_floating_domain, this->forced_alignment());
    }
    FloatingType to_imaginary() const {
      return FloatingType(
        bits, imaginary_floating_domain, this->forced_alignment()
      );
    }
    FloatingType to_complex() const {
      return FloatingType(
        bits, complex_floating_domain, this->forced_alignment()
      );
    }
  };
}

#endif
