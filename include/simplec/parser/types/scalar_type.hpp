#ifndef SIMPLEC_SCALAR_TYPE_INCLUDED
#define SIMPLEC_SCALAR_TYPE_INCLUDED

#include <simplec/parser/types/type.hpp>

namespace simplec
{

  class ScalarType: public Type
  {
  public:
    ScalarType(uint64_t fa = 0): Type(fa) {}

    bool is_complete() const final {return true;}
    bool is_vla() const final {return false;}
  };

}

#endif
