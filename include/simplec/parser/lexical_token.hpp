#ifndef SIMPLEC_PARSER_LEXICAL_TOKEN_INCLUDED
#define SIMPLEC_PARSER_LEXICAL_TOKEN_INCLUDED

#include <simplec/parser/token/keyword.hpp>
#include <simplec/parser/token/punctuator.hpp>
#include <simplec/parser/token/string_literal.hpp>
#include <simplec/parser/token/constant.hpp>

namespace simplec
{

  enum lexical_token_type: unsigned char
  {
    tok_identifier = 0,
    tok_keyword,
    tok_punctuator,
    tok_constant,
    tok_string_literal,

    tok_invalid = static_cast<unsigned char>(-1)
  };

  enum lexical_token_type_id: unsigned char
  {
    lex_tok_identifier = 0,
    lex_tok_keyword,
    lex_tok_punctuator,
    lex_tok_literal
  };

  enum identifier_token_type_id: unsigned char
  {
    id_is_valid = 0,
    id_is_eof = 1
  };

  enum literal_type: unsigned char
  {
    integer_constant,
    floating_constant,
    character_constant,
    string_literal,

    invalid_literal = static_cast<unsigned char>(-1)
  };

  class Token
  {
    std::string val;
    token_metadata md;
    unsigned char type;
    Token(std::string v, unsigned char t, token_metadata m)
    : val(std::move(v)), md(m), type(t) {}

    static constexpr unsigned char append_id(
      unsigned char id, lexical_token_type_id t) {
      return (id << 2) | static_cast<unsigned char>(t);
    }
    static constexpr unsigned char get_keyword_type_id(C_keyword k) {
      return append_id(static_cast<unsigned char>(k), lex_tok_keyword);
    }
    static constexpr unsigned char get_punctuator_type_id(C_punctuator p) {
      return append_id(static_cast<unsigned char>(p), lex_tok_punctuator);
    }
    static constexpr unsigned char get_identifier_type_id() {
      return
        append_id(static_cast<unsigned char>(id_is_valid), lex_tok_identifier);
    }
    static constexpr unsigned char get_eof_type_id() {
      return
        append_id(static_cast<unsigned char>(id_is_eof), lex_tok_identifier);
    }
    static constexpr unsigned char get_literal_type_id(
      literal_type t, unsigned char id) {
      return append_id(
        (id << 2) | static_cast<unsigned char>(t),
        lex_tok_literal
      );
    }
    static constexpr unsigned char get_string_literal_type_id(
      string_literal_prefix p) {
      return get_literal_type_id(string_literal, static_cast<unsigned char>(p));
    }
    static constexpr unsigned char get_int_constant_type_id(
      int_literal_suffix s) {
      return
        get_literal_type_id(integer_constant, static_cast<unsigned char>(s));
    }
    static constexpr unsigned char get_float_constant_type_id(
      floating_constant_type f) {
      return
        get_literal_type_id(floating_constant, static_cast<unsigned char>(f));
    }
    static constexpr unsigned char get_character_constant_type_id(
      character_constant_prefix p) {
      return
        get_literal_type_id(character_constant, static_cast<unsigned char>(p));
    }

    C_keyword get_raw_keyword() const {
      return static_cast<C_keyword>(type >> 2);
    }
    C_punctuator get_raw_punctuator() const {
      return static_cast<C_punctuator>(type >> 2);
    }

  public:
    token_metadata metadata() const {return md;}

    static Token get_eof_token(token_metadata m) {
      return Token("", get_eof_type_id(), m);
    }

    lexical_token_type_id get_raw_id() const {
      return static_cast<lexical_token_type_id>(type & 3);
    }

    bool is_literal() const {
      return get_raw_id() == lex_tok_literal;
    }

    literal_type get_literal_type() const {
      if(!is_literal()) return invalid_literal;
      return static_cast<literal_type>((type & 0xC) >> 2);
    }
    bool is_identifier() const {
      return get_raw_id() == lex_tok_identifier && !(type >> 2);
    }
    bool is_eof() const {
      return get_raw_id() == lex_tok_identifier && (type >> 2) == 1;
    }
    bool is_keyword() const {
      return get_raw_id() == lex_tok_keyword;
    }
    bool is_punctuator() const {
      return get_raw_id() == lex_tok_punctuator;
    }
    bool is_constant() const {
      auto lt = get_literal_type();
      return lt != string_literal && lt != invalid_literal;
    }
    bool is_string_literal() const {
      return get_literal_type() == string_literal;
    }
    bool is_int_constant() const {
      return get_literal_type() == integer_constant;
    }
    bool is_float_constant() const {
      return get_literal_type() == floating_constant;
    }
    bool is_char_constant() const {
      return get_literal_type() == character_constant;
    }

    string_literal_prefix get_string_prefix() const {
      if(!is_string_literal()) return invalid_prefix;
      return static_cast<string_literal_prefix>(type >> 4);
    }
    int_literal_suffix get_int_constant_suffix() const {
      if(!is_int_constant()) return invalid_suffix;
      return static_cast<int_literal_suffix>(type >> 4);
    }
    floating_constant_type get_float_type() const {
      if(!is_float_constant()) return invalid_floating_type;
      return static_cast<floating_constant_type>(type >> 4);
    }
    character_constant_prefix get_char_prefix() const {
      if(!is_char_constant()) return invalid_char_prefix;
      return static_cast<character_constant_prefix>(type >> 4);
    }

    lexical_token_type get_type() const {
      switch(get_raw_id())
      {
        case lex_tok_identifier:
          if(is_identifier()) return tok_identifier;
          return tok_invalid;
        case lex_tok_keyword: return tok_keyword;
        case lex_tok_punctuator: return tok_punctuator;
        case lex_tok_literal:
          if(is_string_literal()) return tok_string_literal;
          return tok_constant;
      }
      return tok_invalid;
    }

    C_keyword get_keyword() const {
      if(!is_keyword()) return invalid_keyword;
      return get_raw_keyword();
    }
    C_punctuator get_punctuator() const {
      if(!is_punctuator()) return invalid_punctuator;
      return get_raw_punctuator();
    }

    const std::string& data() const {return val;}
    std::string strip() const {return std::move(val);}

    Token(C_keyword k, token_metadata m)
    : val(), md(m), type(get_keyword_type_id(k)){}
    Token(C_punctuator p, token_metadata m)
    : val(), md(m), type(get_punctuator_type_id(p)) {}

    Token(preprocessor_token tok);
  };

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_PARSER_LEXICAL_TOKEN_TEST_INCLUDED
#define SIMPLEC_PARSER_LEXICAL_TOKEN_TEST_INCLUDED

namespace simplec
{

  namespace test
  {
    template<class T>
    T inc_cast(T i) {return static_cast<T>(static_cast<unsigned char>(i) + 1);}
  }

  TEST_CASE("Keyword/punctuator lexical tokens are constructed correctly")
  {
    for(C_keyword i = keyword_char; i < keyword__Static_assert;
        i = test::inc_cast(i))
    {
      auto t = Token(i, {});
      CHECK_EQ(t.get_keyword(), i);
      CHECK_EQ(t.get_punctuator(), invalid_punctuator);
    }
    for(C_punctuator i = punctuator_lsquare; i < punctuator_comma;
        i = test::inc_cast(i))
    {
      auto t = Token(i, {});
      CHECK_EQ(t.get_punctuator(), i);
      CHECK_EQ(t.get_keyword(), invalid_keyword);
    }
  }

  TEST_CASE("Lexical tokens are properly constructed from preprocessor tokens")
  {
    std::vector<preprocessor_token> toks {
      {"123L", token_metadata(), tok_num},
      {"0123uL", token_metadata(), tok_num},
      {"0x12", token_metadata(), tok_num},
      {"1.23", token_metadata(), tok_num},
      {"1.2e3", token_metadata(), tok_num},
      {"1e3f", token_metadata(), tok_num},
      {"1.23l", token_metadata(), tok_num},
      {"L\'0\'", token_metadata(), tok_char_literal},
      {"U\"wchar_t string\"", token_metadata(), tok_str},
      {"hello", token_metadata(), tok_id},
      {"if", token_metadata(), tok_id},
      {"while", token_metadata(), tok_id},
      {"++", token_metadata(), tok_punc},
      {"--", token_metadata(), tok_punc},
      {"", token_metadata(), tok_eof}
    };

    std::vector<std::string> target_data {
      "123",
      "0123",
      "0x12",
      "1.23",
      "1.2e3",
      "1e3",
      "1.23",
      "0",
      "wchar_t string",
      "hello",
      "",
      "",
      "",
      "",
      ""
    };

    std::vector<lexical_token_type> target_types {
      tok_constant,
      tok_constant,
      tok_constant,
      tok_constant,
      tok_constant,
      tok_constant,
      tok_constant,
      tok_constant,
      tok_string_literal,
      tok_identifier,
      tok_keyword,
      tok_keyword,
      tok_punctuator,
      tok_punctuator,
      tok_invalid
    };

    std::vector<C_keyword> keywords {
      invalid_keyword,
      invalid_keyword,
      invalid_keyword,
      invalid_keyword,
      invalid_keyword,
      invalid_keyword,
      invalid_keyword,
      invalid_keyword,
      invalid_keyword,
      invalid_keyword,
      keyword_if,
      keyword_while,
      invalid_keyword,
      invalid_keyword,
      invalid_keyword
    };

    std::vector<C_punctuator> punctuators {
      invalid_punctuator,
      invalid_punctuator,
      invalid_punctuator,
      invalid_punctuator,
      invalid_punctuator,
      invalid_punctuator,
      invalid_punctuator,
      invalid_punctuator,
      invalid_punctuator,
      invalid_punctuator,
      invalid_punctuator,
      invalid_punctuator,
      punctuator_increment,
      punctuator_decrement,
      invalid_punctuator
    };

    std::vector<literal_type> literal_types {
      integer_constant,
      integer_constant,
      integer_constant,
      floating_constant,
      floating_constant,
      floating_constant,
      floating_constant,
      character_constant,
      string_literal,
      invalid_literal,
      invalid_literal,
      invalid_literal,
      invalid_literal,
      invalid_literal,
      invalid_literal
    };

    std::vector<floating_constant_type> float_types {
      invalid_floating_type,
      invalid_floating_type,
      invalid_floating_type,
      floating_type_double,
      floating_type_double,
      floating_type_single,
      floating_type_long_double,
      invalid_floating_type,
      invalid_floating_type,
      invalid_floating_type,
      invalid_floating_type,
      invalid_floating_type,
      invalid_floating_type,
      invalid_floating_type,
      invalid_floating_type
    };

    std::vector<int_literal_suffix> suffixes {
      int_suffix_l,
      int_suffix_ul,
      no_suffix,
      invalid_suffix,
      invalid_suffix,
      invalid_suffix,
      invalid_suffix,
      invalid_suffix,
      invalid_suffix,
      invalid_suffix,
      invalid_suffix,
      invalid_suffix,
      invalid_suffix,
      invalid_suffix,
      invalid_suffix
    };

    std::vector<string_literal_prefix> str_prefixes {
      invalid_prefix,
      invalid_prefix,
      invalid_prefix,
      invalid_prefix,
      invalid_prefix,
      invalid_prefix,
      invalid_prefix,
      invalid_prefix,
      prefix_U,
      invalid_prefix,
      invalid_prefix,
      invalid_prefix,
      invalid_prefix,
      invalid_prefix,
      invalid_prefix
    };

    std::vector<character_constant_prefix> char_prefixes {
      invalid_char_prefix,
      invalid_char_prefix,
      invalid_char_prefix,
      invalid_char_prefix,
      invalid_char_prefix,
      invalid_char_prefix,
      invalid_char_prefix,
      char_prefix_L,
      invalid_char_prefix,
      invalid_char_prefix,
      invalid_char_prefix,
      invalid_char_prefix,
      invalid_char_prefix,
      invalid_char_prefix,
      invalid_char_prefix
    };

    std::vector<Token> lex_toks;
    lex_toks.reserve(toks.size());
    for(auto& tok : toks) lex_toks.emplace_back(std::move(tok));
    size_t i = 0;
    for(const auto& tok : lex_toks)
    {
      CHECK_EQ(tok.get_type(), target_types[i]);
      CHECK_EQ(tok.data(), target_data[i]);
      CHECK_EQ(tok.get_keyword(), keywords[i]);
      CHECK_EQ(tok.get_punctuator(), punctuators[i]);
      CHECK_EQ(tok.get_literal_type(), literal_types[i]);
      CHECK_EQ(tok.get_float_type(), float_types[i]);
      CHECK_EQ(tok.get_int_constant_suffix(), suffixes[i]);
      CHECK_EQ(tok.get_string_prefix(), str_prefixes[i]);
      CHECK_EQ(tok.get_char_prefix(), char_prefixes[i]);
      i++;
    }

    CHECK_UNARY(lex_toks.back().is_eof());
  }

}

#endif
#endif
