#ifndef SIMPLEC_PARSER_TOKEN_BUFFER_INCLUDED
#define SIMPLEC_PARSER_TOKEN_BUFFER_INCLUDED

#include <simplec/parser/lexical_token.hpp>
#include <simplec/parser/token/io.hpp>

#include <boost/container/small_vector.hpp>

#include <cassert>

namespace simplec
{

  class TokenBuffer
  {
    static const size_t sv_size = 5;
    boost::container::small_vector<Token, sv_size> token_buffer;
    size_t bpos;

    TokenBuffer& push_back(Token tok) {
      token_buffer.push_back(std::move(tok));
      return *this;
    }
  public:
    TokenBuffer(): token_buffer{}, bpos(0) {}

    // Token parsing never fails in normal execution, so a reference is returned
    template<class ST>
    Token& parse_token(ST& src)
    {
      if(token_buffer.empty() || bpos >= token_buffer.size())
      {
        bpos = token_buffer.size();
        push_back(src.get_stripped());
      }
      return token_buffer[bpos++];
    }

    TokenBuffer& unget_tokens(size_t a) {
      if(a > bpos) bpos = 0;
      else bpos -= a;
      return *this;
    }

    TokenBuffer& unget_token() {return unget_tokens(1);}
    TokenBuffer& parsed_buffer() {
      assert(bpos <= token_buffer.size());
      token_buffer.erase(token_buffer.begin(), token_buffer.begin() + bpos);
      bpos = 0;
      return *this;
    }
    TokenBuffer& parsed_buffer(size_t a) {
      assert(bpos <= token_buffer.size());
      if(a > bpos) a = bpos;
      token_buffer.erase(
        token_buffer.begin() + bpos - a, token_buffer.begin() + bpos
      );
      return *this;
    }

    // Debugging utility
    const TokenBuffer& print(std::ostream& str) const {
      str << "[";
      for(size_t i = 0; i < bpos; i++)
      {
        str << " " << token_buffer[i] << " ";
      }
      if(bpos < token_buffer.size()) str << " (" << token_buffer[bpos] << ") ";
      else str << "END";
      for(size_t i = bpos + 1; i < token_buffer.size(); i++)
      {
        str << " " << token_buffer[i] << " ";
      }
      str << "]";
      return *this;
    }
  };

}

#endif
