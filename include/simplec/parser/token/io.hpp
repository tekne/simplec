#ifndef SIMPLEC_PARSER_TOKEN_IO_INCLUDED
#define SIMPLEC_PARSER_TOKEN_IO_INCLUDED

#include <ostream>
#include <sstream>
#include <simplec/parser/lexical_token.hpp>

namespace simplec
{

  inline std::ostream& operator<<(std::ostream& str, const Token& tok) {
    if(tok.is_eof()) return str;

    switch(tok.get_type())
    {
      case tok_identifier: str << tok.data(); break;
      case tok_keyword: str << C_keyword_to_str(tok.get_keyword()); break;
      case tok_punctuator: str << C_punctuator_to_str(tok.get_punctuator());
      break;
      case tok_constant:
        if(tok.is_int_constant())
        {
          str << tok.data();
          switch(tok.get_int_constant_suffix())
          {
            case int_suffix_l: str << "L"; break;
            case int_suffix_ll: str << "LL"; break;
            case int_suffix_u: str << "U"; break;
            case int_suffix_ul: str << "UL"; break;
            case int_suffix_ull: str << "ULL"; break;
            default: break;
          }
        }
        else if(tok.is_float_constant())
        {
          str << tok.data();
          switch(tok.get_float_type())
          {
            case floating_type_single: str << "f"; break;
            case floating_type_long_double: str << "l"; break;
            default: break;
          }
        }
        else if(tok.is_char_constant())
        {
          switch(tok.get_char_prefix())
          {
            case char_prefix_u: str << "u"; break;
            case char_prefix_U: str << "U"; break;
            case char_prefix_L: str << "L"; break;
            default: break;
          }
          str << "\'" << tok.data() << "\'";
        }
        break;
      case tok_string_literal:
        switch(tok.get_string_prefix())
        {
          case prefix_u: str << "u"; break;
          case prefix_u8: str << "u8"; break;
          case prefix_U: str << "U"; break;
          case prefix_L: str << "L"; break;
          default: break;
        }
        str << "\"" << tok.data() << "\"";
        break;
      default: break;
    }
    return str;
  }

  inline std::string tok_to_string(const Token& tok)
  {
    std::stringstream sstr;
    sstr << tok;
    return sstr.str();
  }

}

#endif
