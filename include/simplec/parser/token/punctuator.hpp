#ifndef SIMPLEC_PARSER_TOKEN_PUNCTUATOR_INCLUDED
#define SIMPLEC_PARSER_TOKEN_PUNCTUATOR_INCLUDED

#include <simplec/preprocessor/token.hpp>

#include <string>

namespace simplec
{

  enum C_punctuator: unsigned char {
    // Preprocessor punctuators
    preprocessor_hash, // #
    preprocessor_concat, // ##

    // Valid parser punctuators
    punctuator_lsquare, // [
    punctuator_rsquare, // ]
    punctuator_lparen, // (
    punctuator_rparen, // )
    punctuator_lcurly, // {
    punctuator_rcurly, // }
    punctuator_period, // .
    punctuator_arrow, // ->
    punctuator_increment, // ++
    punctuator_decrement, // --
    punctuator_ampersand, // &
    punctuator_star, // *
    punctuator_plus, // +
    punctuator_minus, // -
    punctuator_tilde, // ~
    punctuator_not, // !
    punctuator_divide, // /
    punctuator_modulo, // %
    punctuator_lshift, // <<
    punctuator_rshift, // >>
    punctuator_lt, // <
    punctuator_gt, // >
    punctuator_leq, // <=
    punctuator_geq, // >=
    punctuator_eq, // ==
    punctuator_neq, // !=
    punctuator_xor, // ^
    punctuator_pipe, // |
    punctuator_and, // &&
    punctuator_or, // ||
    punctuator_ternary, // ?
    punctuator_colon, // :
    punctuator_semicolon, // ;
    punctuator_ellipsis, // ...
    punctuator_assign, // =
    punctuator_timeseq, // *=
    punctuator_diveq, // /=
    punctuator_modeq, // %=
    punctuator_pluseq, // +=
    punctuator_mineq, // -=
    punctuator_lshifteq, // <<=
    punctuator_rshifteq, // >>=
    punctuator_andeq, // &=
    punctuator_xoreq, // ^=
    punctuator_oreq, // |=
    punctuator_comma, // ,

    // Invalid punctuators
    invalid_punctuator = static_cast<unsigned char>(-1) // Invalid ($, //, etc.)
  };

  C_punctuator str_to_C_punctuator(const std::string&);
  inline C_punctuator str_to_C_punctuator(const preprocessor_token& tok)
  {
    if(tok.type() == tok_punc) return str_to_C_punctuator(tok.string());
    return invalid_punctuator;
  }
  inline bool is_unary_operator(C_punctuator p)
  {
    switch(p)
    {
      case punctuator_ampersand:
      case punctuator_star:
      case punctuator_plus:
      case punctuator_minus:
      case punctuator_tilde:
      case punctuator_not:
        return true;
      default: return false;
    }
  }

  #define SIMPLEC_MAP_PUNCTUATOR_TO_STR(str, name) \
  case punctuator_ ## name : return str;

  inline const char* C_punctuator_to_str(C_punctuator p)
  {
    switch(p)
    {
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("[", lsquare);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("]", rsquare);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("(", lparen);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR(")", rparen);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("{", lcurly);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("}", rcurly);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR(".", period);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("->", arrow);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("++", increment);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("--", decrement);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("&", ampersand);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("*", star);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("+", plus);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("-", minus);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("~", tilde);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("!", not);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("/", divide);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("%", modulo);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("<<", lshift);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR(">>", rshift);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("<", lt);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR(">", gt);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("<=", leq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR(">=", geq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("==", eq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("!=", neq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("^", xor);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("|", pipe);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("&&", and);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("||", or);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("?", ternary);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR(":", colon);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR(";", semicolon);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("...", ellipsis);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("=", assign);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("*=", timeseq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("/=", diveq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("%=", modeq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("+=", pluseq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("-=", mineq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("<<=", lshifteq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR(">>=", rshifteq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("&=", andeq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("^=", xoreq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR("|=", oreq);
      SIMPLEC_MAP_PUNCTUATOR_TO_STR(",", comma);
      case preprocessor_hash: return "#";
      case preprocessor_concat: return "##";
      default: return "(INVALID PUNCTUATOR)";
    }
  }

  #undef SIMPLEC_MAP_PUNCTUATOR_TO_STR

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_PARSER_TOKEN_PUNCTUATOR_TEST_INCLUDED
#define SIMPLEC_PARSER_TOKEN_PUNCTUATOR_TEST_INCLUDED

#define SIMPLEC_CHECK_PUNCTUATOR_PARSES(str, name) \
  do {CHECK_EQ(str_to_C_punctuator(str), C_punctuator::punctuator_ ## name);} \
  while(false)

#define SIMPLEC_CHECK_PUNCTUATOR_DOESNT_PARSE(str) \
  do {CHECK_EQ(str_to_C_punctuator(str), C_punctuator::invalid_punctuator);} \
  while(false)

namespace simplec
{

  TEST_CASE("Punctuators are identified correctly")
  {
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("[", lsquare);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("]", rsquare);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("(", lparen);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES(")", rparen);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("{", lcurly);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("}", rcurly);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES(".", period);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("->", arrow);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("++", increment);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("--", decrement);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("&", ampersand);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("*", star);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("+", plus);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("-", minus);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("~", tilde);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("!", not);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("/", divide);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("%", modulo);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("<<", lshift);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES(">>", rshift);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("<", lt);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES(">", gt);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("<=", leq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES(">=", geq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("==", eq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("!=", neq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("^", xor);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("|", pipe);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("&&", and);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("||", or);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("?", ternary);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES(":", colon);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES(";", semicolon);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("...", ellipsis);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("=", assign);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("*=", timeseq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("/=", diveq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("%=", modeq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("+=", pluseq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("-=", mineq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("<<=", lshifteq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES(">>=", rshifteq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("&=", andeq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("^=", xoreq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("|=", oreq);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES(",", comma);
    CHECK_EQ(str_to_C_punctuator("#"), preprocessor_hash);
    CHECK_EQ(str_to_C_punctuator("##"), preprocessor_concat);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("<:", lsquare);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES(":>", rsquare);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("<%", lcurly);
    SIMPLEC_CHECK_PUNCTUATOR_PARSES("%>", rcurly);
    CHECK_EQ(str_to_C_punctuator("%:"), preprocessor_hash);
    CHECK_EQ(str_to_C_punctuator("%:%:"), preprocessor_concat);

    SIMPLEC_CHECK_PUNCTUATOR_DOESNT_PARSE(" +");
    SIMPLEC_CHECK_PUNCTUATOR_DOESNT_PARSE("@");
    SIMPLEC_CHECK_PUNCTUATOR_DOESNT_PARSE("$");
    SIMPLEC_CHECK_PUNCTUATOR_DOESNT_PARSE("\"string\"");
    SIMPLEC_CHECK_PUNCTUATOR_DOESNT_PARSE("and");
  }

}

#endif
#endif
