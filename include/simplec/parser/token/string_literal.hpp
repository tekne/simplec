#ifndef SIMPLEC_PARSER_TOKEN_STRING_LITERAL_INCLUDED
#define SIMPLEC_PARSER_TOKEN_STRING_LITERAL_INCLUDED

#include <string>

namespace simplec
{

  enum string_literal_prefix: unsigned char {
    // No prefix (standard C-string)
    no_prefix,
    prefix_u8,
    prefix_u,
    prefix_U,
    prefix_L,

    // Error tokens
    invalid_prefix = static_cast<unsigned char>(-1)
  };

  inline string_literal_prefix str_to_string_literal_prefix(
    const std::string& str
  )
  {
    if(str == "u8") return prefix_u8;
    else if(str == "u") return prefix_u;
    else if(str == "U") return prefix_U;
    else if(str == "L") return prefix_L;
    else return invalid_prefix;
  }

}

#endif
