#ifndef SIMPLEC_PARSER_TOKEN_CONSTANT_INCLUDED
#define SIMPLEC_PARSER_TOKEN_CONSTANT_INCLUDED

#include <simplec/preprocessor/token.hpp>

namespace simplec
{

  enum int_literal_suffix: unsigned char {
    // No suffix (null token)
    no_suffix,

    // Integer suffixes
    // Integers with the L and LL suffix, as in the standard
    int_suffix_l,
    int_suffix_ll,

    int_suffix_u, // Smallest valid unsigned type that can hold the value
    // Unsigned integers with the UL and ULL suffix, as in the standard
    int_suffix_ul,
    int_suffix_ull,

    invalid_suffix = static_cast<unsigned char>(-1)
  };

  int_literal_suffix get_int_suffix(const char*);

  enum character_constant_prefix: unsigned char
  {
    no_char_prefix,
    char_prefix_u,
    char_prefix_U,
    char_prefix_L,

    invalid_char_prefix = static_cast<unsigned char>(-1)
  };

  enum floating_constant_type: unsigned char {
    floating_type_double,
    floating_type_single,
    floating_type_long_double,

    invalid_floating_type = static_cast<unsigned char>(-1)
  };

  int get_multichar_constant(const std::string&);
  char32_t get_character_constant(const std::string&);

}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_PARSER_TOKEN_CONSTANT_TEST_INCLUDED
#define SIMPLEC_PARSER_TOKEN_CONSTANT_TEST_INCLUDED

namespace simplec
{

  TEST_CASE("Integer suffixes are identified correctly")
  {
    CHECK_EQ(no_suffix, get_int_suffix(""));
    CHECK_EQ(int_suffix_u, get_int_suffix("u"));
    CHECK_EQ(int_suffix_u, get_int_suffix("U"));
    CHECK_EQ(int_suffix_l, get_int_suffix("l"));
    CHECK_EQ(int_suffix_l, get_int_suffix("L"));
    CHECK_EQ(int_suffix_ul, get_int_suffix("ul"));
    CHECK_EQ(int_suffix_ul, get_int_suffix("lu"));
    CHECK_EQ(int_suffix_ul, get_int_suffix("Ul"));
    CHECK_EQ(int_suffix_ul, get_int_suffix("LU"));
    CHECK_EQ(int_suffix_ll, get_int_suffix("ll"));
    CHECK_EQ(int_suffix_ll, get_int_suffix("lL"));
    CHECK_EQ(int_suffix_ull, get_int_suffix("lLu"));
    CHECK_EQ(int_suffix_ull, get_int_suffix("ulL"));
    CHECK_EQ(invalid_suffix, get_int_suffix("lLk"));
    CHECK_EQ(invalid_suffix, get_int_suffix(" ll"));
  }

  TEST_CASE("char32_t constants are identified correctly")
  {
    CHECK_EQ('a', get_character_constant("a"));
    CHECK_EQ(U'\u0100', get_character_constant("\\u0100"));
    CHECK_EQ(0, get_character_constant("\\0"));
    CHECK_EQ('\n', get_character_constant("\\12"));
    CHECK_EQ('\n', get_character_constant("\\n"));
  }

  TEST_CASE("Multicharacter constants are identified correctly")
  {
    CHECK_EQ('a', get_multichar_constant("a"));
    CHECK_EQ(0x6162, get_multichar_constant("ab"));
    CHECK_EQ('\0', get_multichar_constant("\\0"));
    CHECK_EQ('\f', get_multichar_constant("\\14"));
    CHECK_EQ('\n', get_multichar_constant("\\n"));
  }

}

#endif
#endif
