#ifndef SIMPLEC_PARSER_TOKEN_KEYWORD_INCLUDED
#define SIMPLEC_PARSER_TOKEN_KEYWORD_INCLUDED

#include <string>
#include <simplec/preprocessor/token.hpp>

namespace simplec
{

  enum C_keyword: unsigned char {
    // Type specifiers
    keyword_char = 0,
    keyword_double,
    keyword_float,
    keyword_int,
    keyword_long,
    keyword_short,
    keyword_signed,
    keyword_unsigned,
    keyword_void,
    keyword__Bool,
    keyword__Complex,
    keyword__Imaginary,
    // --> Struct/class/enum specifiers
    keyword_enum,
    keyword_struct,
    keyword_union,
    // Storage-class specifiers
    keyword_auto,
    keyword_extern,
    keyword_register,
    keyword_static,
    keyword_typedef,
    keyword__Thread_local,
    // Type qualifiers
    keyword_const,
    keyword_restrict,
    keyword_volatile,
    keyword__Atomic,
    // Function qualifiers
    keyword_inline,
    keyword__Noreturn,
    // Alignment specifiers
    keyword__Alignas,
    // Control flow
    keyword_break,
    keyword_case,
    keyword_continue,
    keyword_default,
    keyword_do,
    keyword_else,
    keyword_for,
    keyword_goto,
    keyword_if,
    keyword_return,
    keyword_switch,
    keyword_while,
    // Unevaluated expressions
    keyword_alignof,
    keyword_sizeof,
    keyword__Generic,
    keyword__Static_assert,

    // Invalid keyword
    invalid_keyword = static_cast<unsigned char>(-1)
  };

  C_keyword str_to_C_keyword(const std::string& str);
  inline C_keyword str_to_C_keyword(const preprocessor_token& tok)
  {
    if(tok.type() == tok_id) return str_to_C_keyword(tok.string());
    return invalid_keyword;
  }

#define SIMPLEC_MAP_KEYWORD_TO_STR(word) \
  case keyword_ ## word: return #word;

  inline const char* C_keyword_to_str(C_keyword k)
  {
    switch(k)
    {
      SIMPLEC_MAP_KEYWORD_TO_STR(auto);
      SIMPLEC_MAP_KEYWORD_TO_STR(alignof);
      SIMPLEC_MAP_KEYWORD_TO_STR(break);
      SIMPLEC_MAP_KEYWORD_TO_STR(case);
      SIMPLEC_MAP_KEYWORD_TO_STR(char);
      SIMPLEC_MAP_KEYWORD_TO_STR(const);
      SIMPLEC_MAP_KEYWORD_TO_STR(continue);
      SIMPLEC_MAP_KEYWORD_TO_STR(default);
      SIMPLEC_MAP_KEYWORD_TO_STR(do);
      SIMPLEC_MAP_KEYWORD_TO_STR(double);
      SIMPLEC_MAP_KEYWORD_TO_STR(else);
      SIMPLEC_MAP_KEYWORD_TO_STR(enum);
      SIMPLEC_MAP_KEYWORD_TO_STR(extern);
      SIMPLEC_MAP_KEYWORD_TO_STR(float);
      SIMPLEC_MAP_KEYWORD_TO_STR(for);
      SIMPLEC_MAP_KEYWORD_TO_STR(goto);
      SIMPLEC_MAP_KEYWORD_TO_STR(if);
      SIMPLEC_MAP_KEYWORD_TO_STR(inline);
      SIMPLEC_MAP_KEYWORD_TO_STR(int);
      SIMPLEC_MAP_KEYWORD_TO_STR(long);
      SIMPLEC_MAP_KEYWORD_TO_STR(register);
      SIMPLEC_MAP_KEYWORD_TO_STR(restrict);
      SIMPLEC_MAP_KEYWORD_TO_STR(return);
      SIMPLEC_MAP_KEYWORD_TO_STR(short);
      SIMPLEC_MAP_KEYWORD_TO_STR(signed);
      SIMPLEC_MAP_KEYWORD_TO_STR(sizeof);
      SIMPLEC_MAP_KEYWORD_TO_STR(static);
      SIMPLEC_MAP_KEYWORD_TO_STR(struct);
      SIMPLEC_MAP_KEYWORD_TO_STR(switch);
      SIMPLEC_MAP_KEYWORD_TO_STR(typedef);
      SIMPLEC_MAP_KEYWORD_TO_STR(union);
      SIMPLEC_MAP_KEYWORD_TO_STR(unsigned);
      SIMPLEC_MAP_KEYWORD_TO_STR(void);
      SIMPLEC_MAP_KEYWORD_TO_STR(volatile);
      SIMPLEC_MAP_KEYWORD_TO_STR(while);
      SIMPLEC_MAP_KEYWORD_TO_STR(_Alignas);
      SIMPLEC_MAP_KEYWORD_TO_STR(_Atomic);
      SIMPLEC_MAP_KEYWORD_TO_STR(_Bool);
      SIMPLEC_MAP_KEYWORD_TO_STR(_Complex);
      SIMPLEC_MAP_KEYWORD_TO_STR(_Generic);
      SIMPLEC_MAP_KEYWORD_TO_STR(_Imaginary);
      SIMPLEC_MAP_KEYWORD_TO_STR(_Noreturn);
      SIMPLEC_MAP_KEYWORD_TO_STR(_Static_assert);
      SIMPLEC_MAP_KEYWORD_TO_STR(_Thread_local);
      default: return "(INVALID KEYWORD)";
    }
  }

  #undef SIMPLEC_MAP_KEYWORD_TO_STR
}

#endif

#ifdef DOCTEST_LIBRARY_INCLUDED
#ifndef SIMPLEC_PARSER_TOKEN_KEYWORD_TEST_INCLUDED
#define SIMPLEC_PARSER_TOKEN_KEYWORD_TEST_INCLUDED

#define SIMPLEC_CHECK_KEYWORD_PARSES(argument) \
  do {CHECK_EQ(str_to_C_keyword(#argument), keyword_ ## argument);} while(false)

namespace simplec
{

  TEST_CASE("Keywords are identified correctly")
  {
    SIMPLEC_CHECK_KEYWORD_PARSES(auto);
    SIMPLEC_CHECK_KEYWORD_PARSES(alignof);
    SIMPLEC_CHECK_KEYWORD_PARSES(break);
    SIMPLEC_CHECK_KEYWORD_PARSES(case);
    SIMPLEC_CHECK_KEYWORD_PARSES(char);
    SIMPLEC_CHECK_KEYWORD_PARSES(const);
    SIMPLEC_CHECK_KEYWORD_PARSES(continue);
    SIMPLEC_CHECK_KEYWORD_PARSES(default);
    SIMPLEC_CHECK_KEYWORD_PARSES(do);
    SIMPLEC_CHECK_KEYWORD_PARSES(double);
    SIMPLEC_CHECK_KEYWORD_PARSES(else);
    SIMPLEC_CHECK_KEYWORD_PARSES(enum);
    SIMPLEC_CHECK_KEYWORD_PARSES(extern);
    SIMPLEC_CHECK_KEYWORD_PARSES(float);
    SIMPLEC_CHECK_KEYWORD_PARSES(for);
    SIMPLEC_CHECK_KEYWORD_PARSES(goto);
    SIMPLEC_CHECK_KEYWORD_PARSES(if);
    SIMPLEC_CHECK_KEYWORD_PARSES(inline);
    SIMPLEC_CHECK_KEYWORD_PARSES(int);
    SIMPLEC_CHECK_KEYWORD_PARSES(long);
    SIMPLEC_CHECK_KEYWORD_PARSES(register);
    SIMPLEC_CHECK_KEYWORD_PARSES(restrict);
    SIMPLEC_CHECK_KEYWORD_PARSES(return);
    SIMPLEC_CHECK_KEYWORD_PARSES(short);
    SIMPLEC_CHECK_KEYWORD_PARSES(signed);
    SIMPLEC_CHECK_KEYWORD_PARSES(sizeof);
    SIMPLEC_CHECK_KEYWORD_PARSES(static);
    SIMPLEC_CHECK_KEYWORD_PARSES(struct);
    SIMPLEC_CHECK_KEYWORD_PARSES(switch);
    SIMPLEC_CHECK_KEYWORD_PARSES(typedef);
    SIMPLEC_CHECK_KEYWORD_PARSES(union);
    SIMPLEC_CHECK_KEYWORD_PARSES(unsigned);
    SIMPLEC_CHECK_KEYWORD_PARSES(void);
    SIMPLEC_CHECK_KEYWORD_PARSES(volatile);
    SIMPLEC_CHECK_KEYWORD_PARSES(while);
    SIMPLEC_CHECK_KEYWORD_PARSES(_Alignas);
    SIMPLEC_CHECK_KEYWORD_PARSES(_Atomic);
    SIMPLEC_CHECK_KEYWORD_PARSES(_Bool);
    SIMPLEC_CHECK_KEYWORD_PARSES(_Complex);
    SIMPLEC_CHECK_KEYWORD_PARSES(_Generic);
    SIMPLEC_CHECK_KEYWORD_PARSES(_Imaginary);
    SIMPLEC_CHECK_KEYWORD_PARSES(_Noreturn);
    SIMPLEC_CHECK_KEYWORD_PARSES(_Static_assert);
    SIMPLEC_CHECK_KEYWORD_PARSES(_Thread_local);
  }

  TEST_CASE("Invalid keywords are identified as so")
  {
    CHECK_EQ(str_to_C_keyword("this"), invalid_keyword);
    CHECK_EQ(str_to_C_keyword("not"), invalid_keyword);
  }

}

#endif
#endif
