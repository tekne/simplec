#ifndef SIMPLEC_STORAGE_CLASS_SPECIFIER_INCLUDED
#define SIMPLEC_STORAGE_CLASS_SPECIFIER_INCLUDED

#include <simplec/parser/token/keyword.hpp>

namespace simplec
{

  enum storage_class_specifier_values: unsigned char {
    value_none,
    value_typedef,
    value_extern,
    value_static,
    value_auto,
    value_register,
    value__Thread_local_extern,
    value__Thread_local_static,
    value__Thread_local,

    value_invalid = static_cast<unsigned char>(-1)
  };

  class StorageClassSpecifier
  {
    storage_class_specifier_values val;

    StorageClassSpecifier(storage_class_specifier_values v): val(v) {}
  public:
    constexpr bool is_none() const {return value_none;}
    constexpr bool is_typedef() const {return val == value_typedef;}
    constexpr bool is_extern() const {
      return val == value_extern || val == value__Thread_local_extern;
    }
    constexpr bool is_static() const {
      return val == value_static || val == value__Thread_local_static;
    }
    constexpr bool is_thread_local() const {
      return val == value__Thread_local_static
      || val == value__Thread_local_extern
      || val == value__Thread_local;
    }
    constexpr bool is_auto() const {return val == value_auto;}
    constexpr bool is_register() const {return val == value_register;}
    constexpr bool is_valid() const {
      return val == value_none
      || val == value_typedef
      || val == value_extern
      || val == value_static
      || val == value_auto
      || val == value_register
      || val == value__Thread_local_extern
      || val == value__Thread_local_static;
    }
    constexpr bool can_be_valid() const {
      return is_valid() || val == value__Thread_local;
    }

    constexpr StorageClassSpecifier operator|(StorageClassSpecifier s)
    {
      if(is_none()) return s;
      if(s.is_none()) return *this;

      if(s.val == value__Thread_local)
      {
        if(val == value_static)
          return StorageClassSpecifier(value__Thread_local_static);
        else if(val == value_extern)
          return StorageClassSpecifier(value__Thread_local_extern);
      }
      else if(val == value__Thread_local)
      {
        if(s.val == value_static)
          return StorageClassSpecifier(value__Thread_local_static);
        else if(s.val == value_extern)
          return StorageClassSpecifier(value__Thread_local_extern);
      };
      return StorageClassSpecifier(value_invalid);
    }

    constexpr StorageClassSpecifier(C_keyword k): val(value_invalid) {
      switch(k)
      {
        case keyword_auto: val = value_auto;
        case keyword_extern: val = value_extern;
        case keyword_register: val = value_register;
        case keyword_static: val = value_static;
        case keyword_typedef: val = value_typedef;
        case keyword__Thread_local: val = value__Thread_local;
        default: break;
      }
    }
  };

}

#endif
