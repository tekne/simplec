#ifndef SIMPLEC_DECLARATION_PARSER_INCLUDED
#define SIMPLEC_DECLARATION_PARSER_INCLUDED

#include <simplec/parser/declaration/storage_class_specifier.hpp>
#include <simplec/parser/expression.hpp>

namespace simplec
{

  class DeclarationParser: public ExpressionParser
  {
  public:
    DeclarationParser(Preprocessor P, TypeSet T):
    ExpressionParser(std::move(P), std::move(T)) {}
  };

  class StringDeclarationParser: public DeclarationParser
  {
  public:
    std::istringstream sp;
    StringDeclarationParser(
      std::string str, TypeSet T = FundamentalTypeSet::get_basic()
    ): DeclarationParser(Preprocessor(), std::move(T)), sp(std::move(str)) {
      this->get_preprocessor().open(sp);
    }
    StringDeclarationParser(
      std::string str,
      preprocessor_file_source fn,
      TypeSet T = FundamentalTypeSet::get_basic()
    )
    : DeclarationParser(
        Preprocessor(nullptr, preprocessor_state{fn}), std::move(T)
      ), sp(std::move(str))
    {
      this->get_preprocessor().open(sp);
    }
    StringDeclarationParser(std::string str, std::string fn)
    : StringDeclarationParser(str, preprocessor_file_source{fn}) {}
  };

}

#endif
