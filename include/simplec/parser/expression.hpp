#ifndef SIMPLEC_EXPRESSION_PARSER_INCLUDED
#define SIMPLEC_EXPRESSION_PARSER_INCLUDED

#include <simplec/parser/token/buffer.hpp>
#include <simplec/parser/expression/expression.hpp>
#include <simplec/parser/expression/value.hpp>
#include <simplec/preprocessor.hpp>
#include <simplec/parser/types/type_set.hpp>
#include <simplec/parser/expression/function_call.hpp>
#include <simplec/parser/expression/initializer.hpp>

#include <boost/optional.hpp>

namespace simplec
{

  class ExpressionParser
  {
    TokenBuffer buffer;
    Preprocessor preprocessor;
    TypeSet typeset;

    bool prev_unar;

    ExpressionParser& parsed_buffer() {buffer.parsed_buffer(); return *this;}
    ExpressionParser& parsed_buffer(size_t a) {
      buffer.parsed_buffer(a); return *this;
    }
    ExpressionParser& unget_token() {buffer.unget_token(); return *this;}
    ExpressionParser& unget_tokens(size_t a) {
      buffer.unget_tokens(a); return *this;
    }

  public:
    Token& parse_token() {return buffer.parse_token(preprocessor);}

    InitializerList parse_initializer_list();
    boost::optional<InitializerList> parse_bracketed_initializer_list();
    std::unique_ptr<Initializer> parse_initializer();

    QualifiedType parse_type_name();
    QualifiedType parse_bracketed_type_name();
    QualifiedType parse_specifier_qualifier_list();
    QualifiedType parse_abstract_declarator(QualifiedType t);
    QualifiedType parse_direct_abstract_declarator(QualifiedType t);

    Preprocessor& get_preprocessor() {return preprocessor;}
    const Preprocessor& get_preprocessor() const {return preprocessor;}
    bool good() const {return preprocessor.good();}

    TypeSet& get_typeset() {return typeset;}
    const TypeSet& get_typeset() const {return typeset;}

    bool is_typedef(const std::string& id) {
      return typeset.is_typedef(id);
    }

    ExpressionParser(Preprocessor P, TypeSet T)
    : buffer(), preprocessor(std::move(P)), typeset(std::move(T)),
    prev_unar(false) {}

    std::unique_ptr<Value> parse_value();
    FunctionCall::arglist_type parse_argument_expression_list();

    std::unique_ptr<Expression> parse_primary_expression();
    std::unique_ptr<Expression> parse_postfix_expression();
    std::unique_ptr<Expression> parse_unary_expression();
    std::unique_ptr<Expression> parse_cast_expression();
    std::unique_ptr<Expression> parse_multiplicative_expression();
    std::unique_ptr<Expression> parse_additive_expression();
    std::unique_ptr<Expression> parse_shift_expression();
    std::unique_ptr<Expression> parse_relational_expression();
    std::unique_ptr<Expression> parse_equality_expression();
    std::unique_ptr<Expression> parse_AND_expression();
    std::unique_ptr<Expression> parse_XOR_expression();
    std::unique_ptr<Expression> parse_OR_expression();
    std::unique_ptr<Expression> parse_logical_AND_expression();
    std::unique_ptr<Expression> parse_logical_OR_expression();
    std::unique_ptr<Expression> parse_conditional_expression();
    std::unique_ptr<Expression> parse_assignment_expression();
    std::unique_ptr<Expression> parse_expression();
  };

  class StringExpressionParser: public ExpressionParser
  {
  public:
    std::istringstream sp;
    StringExpressionParser(
      std::string str, TypeSet T = FundamentalTypeSet::get_basic()
    ): ExpressionParser(Preprocessor(), std::move(T)), sp(std::move(str)) {
      this->get_preprocessor().open(sp);
    }
    StringExpressionParser(
      std::string str,
      preprocessor_file_source fn,
      TypeSet T = FundamentalTypeSet::get_basic()
    )
    : ExpressionParser(
        Preprocessor(nullptr, preprocessor_state{fn}), std::move(T)
      ), sp(std::move(str))
    {
      this->get_preprocessor().open(sp);
    }
    StringExpressionParser(std::string str, std::string fn)
    : StringExpressionParser(str, preprocessor_file_source{fn}) {}
  };
}

#endif
