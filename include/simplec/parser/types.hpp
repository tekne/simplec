#ifndef SIMPLEC_PARSER_TYPES_INCLUDED
#define SIMPLEC_PARSER_TYPES_INCLUDED

#include <simplec/parser/types/type.hpp>
#include <simplec/parser/types/scalar_type.hpp>
#include <simplec/parser/types/arithmetic_type.hpp>
#include <simplec/parser/types/integer_type.hpp>
#include <simplec/parser/types/floating_type.hpp>
#include <simplec/parser/types/vector_type.hpp>
#include <simplec/parser/types/pointer_type.hpp>
#include <simplec/parser/types/aggregate_type.hpp>
#include <simplec/parser/types/structure_type.hpp>
#include <simplec/parser/types/union_type.hpp>
#include <simplec/parser/types/array_type.hpp>
#include <simplec/parser/types/function_type.hpp>

#endif
