#ifndef SIMPLEC_PARSER_EXPRESSION_INCLUDED
#define SIMPLEC_PARSER_EXPRESSION_INCLUDED

#include <simplec/parser/types/type.hpp>

#include <memory>
#include <sstream>

namespace simplec
{
  class FundamentalTypeSet;

  class Expression
  {
    QualifiedType type;
  protected:
    Expression& set_type(QualifiedType t) {
      type = t;
      return *this;
    }

  public:
    Expression(QualifiedType t): type(std::move(t)) {}

    QualifiedType get_type() const {return type;}
    std::shared_ptr<const Type> get_raw_type() const {return type.raw_type();}
    TypeQualification get_qual() const {return type.get_qual();}

    virtual bool is_constant() const=0;
    bool has_type() const {
      return type.is_valid();
    }
    virtual Expression& evaluate() {return *this;}
    virtual Expression& resolve_type(const FundamentalTypeSet* F = nullptr)
    {
      return *this;
    }
    virtual Expression& update_type(const FundamentalTypeSet* F = nullptr)
    {
      return *this;
    }
    virtual Expression& refresh_type(const FundamentalTypeSet* F = nullptr)
    {
      return *this;
    }

    Expression& resolve_type(const FundamentalTypeSet& F)
    {
      return resolve_type(&F);
    }
    Expression& update_type(const FundamentalTypeSet& F)
    {
      return update_type(&F);
    }
    Expression& refresh_type(const FundamentalTypeSet& F)
    {
      return refresh_type(&F);
    }

    virtual const Expression& print_to_stream(std::ostream&) const=0;

    std::string to_string()
    {
      std::stringstream sstr;
      print_to_stream(sstr);
      return sstr.str();
    }

    virtual std::unique_ptr<Expression> clone() const=0;

    virtual bool is_identifier_primary_expression() const {return false;}

    virtual ~Expression() {}
  };

  inline std::ostream& operator<<(std::ostream& str, const Expression& e)
  {
    e.print_to_stream(str);
    return str;
  }

};

#endif
