#ifndef SIMPLEC_PARSER_INITIALIZER_LIST_INCLUDED
#define SIMPLEC_PARSER_INITIALIZER_LIST_INCLUDED

#include <simplec/parser/expression/expression.hpp>
#include <simplec/parser/expression/identifier.hpp>
#include <vector>
#include <boost/container/small_vector.hpp>
#include <cstdint>
#include <ostream>
#include <sstream>

namespace simplec
{

  class InitializerList;
  class ExpressionInitializer;

  class Initializer
  {
  public:
    Initializer() {}

    virtual bool is_list() const {return false;}
    virtual Expression* get_expr() {return nullptr;}
    virtual const Expression* get_expr() const {return nullptr;}

    virtual const Initializer& print_to_stream(std::ostream& str) const=0;

    std::string to_string()
    {
      std::stringstream sstr{};
      print_to_stream(sstr);
      return sstr.str();
    }
  };

  inline std::ostream& operator<<(std::ostream& str, const Initializer& in)
  {
    in.print_to_stream(str);
    return str;
  }

  class InitializerList: public Initializer
  {
    std::vector<std::unique_ptr<Initializer>> initializer_sublist;
    std::vector<std::pair<Identifier, size_t>> id_map;
    std::vector<std::pair<uint64_t, size_t>> idx_map;
  public:
    InitializerList(): Initializer(),
    initializer_sublist{}, id_map{}, idx_map{} {}

    bool is_list() const final {return false;}
    bool at_designation() const {
      if(idx_map.empty())
      {
        if(id_map.empty()) return false;
        return id_map.back().second == initializer_sublist.size();
      }
      return idx_map.back().second == initializer_sublist.size();
    }

    InitializerList& push_back(std::unique_ptr<Initializer> expr) {
      initializer_sublist.push_back(std::move(expr));
      return *this;
    }

    InitializerList& push_back(Identifier i) {
      id_map.emplace_back(std::move(i), initializer_sublist.size());
      return *this;
    }

    InitializerList& push_back(uint64_t n) {
      idx_map.emplace_back(n, initializer_sublist.size());
      return *this;
    }

   const Initializer& print_to_stream(std::ostream& str) const final {
      str << "{";
      size_t idptr = 0;
      size_t idxptr = 0;
      bool read = false;
      for(size_t i = 0; i < initializer_sublist.size(); i++)
      {
        while(idptr < id_map.size() && id_map[idptr].second == i)
        {
          str << "." << id_map[idptr++].first << " ";
          read = true;
        }
        while(idxptr < idx_map.size() && idx_map[idptr].second == i)
        {
          str << "[" << idx_map[idptr].first << "] ";
          read = true;
        }
        if(read)
        {
          str << "= ";
          read = false;
        }
        assert(initializer_sublist[i]);
        str << *initializer_sublist[i] << ", ";
      }
      str << "}";
      return *this;
    }
  };

  class ExpressionInitializer: public Initializer
  {
    std::unique_ptr<Expression> expr;
  public:
    ExpressionInitializer(std::unique_ptr<Expression> e)
    : Initializer(), expr(std::move(e)) {}

    virtual Expression* get_expr() final {return expr.get();}
    virtual const Expression* get_expr() const final {return expr.get();}

    const Initializer& print_to_stream(std::ostream& str) const final {
      assert(expr);
      str << *expr;
      return *this;
    }
  };

}

#endif
