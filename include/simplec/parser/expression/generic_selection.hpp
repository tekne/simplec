#ifndef SIMPLEC_PARSER_GENERIC_SELECTION_INCLUDED
#define SIMPLEC_PARSER_GENERIC_SELECTION_INCLUDED

#include <simplec/parser/types/type.hpp>
#include <simplec/parser/expression/expression.hpp>
#include <boost/container/small_vector.hpp>

#include <memory>
#include <cstdint>
#include <exception>

namespace simplec
{

  class GenericSelection
  {
    typedef std::pair<std::shared_ptr<const Type>, std::unique_ptr<Expression>>
      generic_assoc;
    static const size_t sv_size = 5;

    std::unique_ptr<Expression> generic_selector;
    boost::container::small_vector<generic_assoc, sv_size> generic_assoc_list;
    std::unique_ptr<Expression> default_expression;
  public:
    template<class T>
    static size_t select_index(
      std::unique_ptr<Expression> gs,
      const T& gal
    ) {
      const Type* t = gs->get_type().raw_type().get();
      for(size_t i = 0; i < gal.size(); i++)
      {
        if(gal[i].first->compatible_type(t))
        {
          return i;
        }
      }
      return -1;
    }
    template<class T>
    static std::unique_ptr<Expression> select_from(
      std::unique_ptr<Expression> gs,
      T gal,
      std::unique_ptr<Expression> ds = nullptr
    ) {
      size_t i = select_index(std::move(gs), gal);
      if(i == -1) return ds;
      return std::move(gal[i].second);
    }

    GenericSelection(
      std::unique_ptr<Expression> gs,
      boost::container::small_vector<generic_assoc, sv_size> gal,
      std::unique_ptr<Expression> de = nullptr
    )
    :
    generic_selector(std::move(gs)),
    generic_assoc_list(std::move(gal)),
    default_expression(std::move(de))
    {}

    std::unique_ptr<Expression> evaluate() {
      return select_from(
        std::move(generic_selector),
        std::move(generic_assoc_list),
        std::move(default_expression)
      );
    }
  };

};

#endif
