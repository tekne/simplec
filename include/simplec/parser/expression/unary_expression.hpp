#ifndef SIMPLEC_PARSER_UNARY_EXPRESSION_INCLUDED
#define SIMPLEC_PARSER_UNARY_EXPRESSION_INCLUDED

#include <simplec/parser/expression/expression.hpp>
#include <simplec/parser/types/type.hpp>
#include <memory>

namespace simplec
{

  class UnaryExpression: public Expression
  {
    std::unique_ptr<Expression> arg;
  public:
    UnaryExpression(
      std::unique_ptr<Expression> a,
      QualifiedType t
    ): Expression(t), arg(std::move(a)) {}

    const Expression& get_arg() const {return *arg;}
    bool is_constant() const {return get_arg().is_constant();}

    Expression& update_subtypes(const FundamentalTypeSet* F) {
      if(!arg->has_type()) arg->update_type(F);
      return *this;
    }

    Expression& update_type(const FundamentalTypeSet* F) {
      update_subtypes(F);
      if(!has_type()) return resolve_type(F);
      return *this;
    }

    Expression& refresh_type(const FundamentalTypeSet* F)
    {
      arg->refresh_type(F);
      return resolve_type(F);
    }
  };


}

#endif
