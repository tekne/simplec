#ifndef SIMPLEC_PARSER_IDENTIFIER_INCLUDED
#define SIMPLEC_PARSER_IDENTIFIER_INCLUDED

#include <simplec/parser/expression/expression.hpp>
#include <simplec/parser/lexical_token.hpp>
#include <simplec/parser/types/type.hpp>

#include <string>
#include <utility>
#include <memory>

namespace simplec
{

  class Identifier: public Expression
  {
    std::string id;
    token_metadata md;
  public:
    Identifier(std::string i, QualifiedType t = invalid_type, token_metadata m = {})
    : Expression(t), id(std::move(i)), md(m) {}
    Identifier(Token t)
    : Expression(invalid_type), id(t.strip()), md(t.metadata()) {
      if(t.get_type() != tok_identifier) throw std::runtime_error(
        id + " is not a valid identifier"
      );
    }

    token_metadata get_metadata() const {return md;}
    Identifier& set_metadata(token_metadata m) {md = m; return *this;}

    bool is_constant() const final {return false;}
    const std::string& get_id() const {return id;}

    const Expression& print_to_stream(std::ostream& str) const {
      str << id;
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<Identifier>(id, get_type());
    }
    std::unique_ptr<Identifier> clone_id() const {
      return std::make_unique<Identifier>(id, get_type());
    }
  };

}

#endif
