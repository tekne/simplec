#ifndef SIMPLEC_PARSER_ARITHMETIC_EXPRESSION_INCLUDED
#define SIMPLEC_PARSER_ARITHMETIC_EXPRESSION_INCLUDED

#include <simplec/parser/expression/binary_expression.hpp>
#include <simplec/parser/types/fundamental_type_set.hpp>

#include <memory>
#include <ostream>

namespace simplec
{

  class AssignmentExpression: public BinaryExpression
  {
  public:

    Expression& resolve_type(const FundamentalTypeSet* F) {
      return set_type(get_lhs().get_type());
    }

    AssignmentExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): BinaryExpression(std::move(l), std::move(r), std::move(t)) {}
    AssignmentExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r
    ): BinaryExpression(
      std::move(l), std::move(r), invalid_type
    ) {
      resolve_type(nullptr);
    }
  };

  class Assignment: public AssignmentExpression
  {
  public:
    Assignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): AssignmentExpression(std::move(l), std::move(r), std::move(t)) {}
    Assignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r
    ): AssignmentExpression(std::move(l), std::move(r)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " = " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<Assignment>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class AdditionAssignment: public AssignmentExpression
  {
  public:
    AdditionAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): AssignmentExpression(std::move(l), std::move(r), std::move(t)) {}
    AdditionAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r
    ): AssignmentExpression(std::move(l), std::move(r)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " += " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<AdditionAssignment>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class MultiplicationAssignment: public AssignmentExpression
  {
  public:
    MultiplicationAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): AssignmentExpression(std::move(l), std::move(r), std::move(t)) {}
    MultiplicationAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r
    ): AssignmentExpression(std::move(l), std::move(r)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " *= " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<MultiplicationAssignment>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class SubtractionAssignment: public AssignmentExpression
  {
  public:
    SubtractionAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): AssignmentExpression(std::move(l), std::move(r), std::move(t)) {}
    SubtractionAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r
    ): AssignmentExpression(std::move(l), std::move(r)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " -= " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<SubtractionAssignment>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class DivisionAssignment: public AssignmentExpression
  {
  public:
    DivisionAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): AssignmentExpression(std::move(l), std::move(r), std::move(t)) {}
    DivisionAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r
    ): AssignmentExpression(std::move(l), std::move(r)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " /= " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<DivisionAssignment>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class ModuloAssignment: public AssignmentExpression
  {
  public:
    ModuloAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): AssignmentExpression(std::move(l), std::move(r), std::move(t)) {}
    ModuloAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r
    ): AssignmentExpression(std::move(l), std::move(r)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " %= " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<ModuloAssignment>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class LeftShiftAssignment: public AssignmentExpression
  {
  public:
    LeftShiftAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): AssignmentExpression(std::move(l), std::move(r), std::move(t)) {}
    LeftShiftAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r
    ): AssignmentExpression(std::move(l), std::move(r)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " <<= " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<LeftShiftAssignment>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class RightShiftAssignment: public AssignmentExpression
  {
  public:
    RightShiftAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): AssignmentExpression(std::move(l), std::move(r), std::move(t)) {}
    RightShiftAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r
    ): AssignmentExpression(std::move(l), std::move(r)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " >>= " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<RightShiftAssignment>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };


  class AndAssignment: public AssignmentExpression
  {
  public:
    AndAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): AssignmentExpression(std::move(l), std::move(r), std::move(t)) {}
    AndAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r
    ): AssignmentExpression(std::move(l), std::move(r)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " &= " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<AndAssignment>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class XorAssignment: public AssignmentExpression
  {
  public:
    XorAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): AssignmentExpression(std::move(l), std::move(r), std::move(t)) {}
    XorAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r
    ): AssignmentExpression(std::move(l), std::move(r)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " ^= " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<XorAssignment>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class OrAssignment: public AssignmentExpression
  {
  public:
    OrAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): AssignmentExpression(std::move(l), std::move(r), std::move(t)) {}
    OrAssignment(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r
    ): AssignmentExpression(std::move(l), std::move(r)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " |= " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<OrAssignment>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

}

#endif
