#ifndef SIMPLEC_PARSER_BITWISE_EXPRESSION_INCLUDED
#define SIMPLEC_PARSER_BITWISE_EXPRESSION_INCLUDED

#include <simplec/parser/expression/binary_expression.hpp>
#include <simplec/parser/types/integer_type.hpp>
#include <simplec/parser/types/floating_type.hpp>
#include <simplec/parser/types/pointer_type.hpp>
#include <simplec/parser/types/arithmetic_type.hpp>
#include <simplec/parser/types/vector_type.hpp>

#include <simplec/parser/types/fundamental_type_set.hpp>

#include <memory>
#include <ostream>

namespace simplec
{

  class BitwiseExpression: public BinaryExpression
  {
  public:
    static std::shared_ptr<const IntegerType> get_bitwise_result_type(
      std::shared_ptr<const Type> t1, std::shared_ptr<const Type> t2,
      const FundamentalTypeSet& F
    ) {
      std::shared_ptr<const IntegerType> i1 =
        std::dynamic_pointer_cast<const IntegerType>(t1);
      std::shared_ptr<const IntegerType> i2 =
        std::dynamic_pointer_cast<const IntegerType>(t2);
      if(!i1) throw
        std::runtime_error("LHS of bitwise expression must be an integer");
      if(!i2) throw
        std::runtime_error("RHS of bitwise expression must be an integer");
      auto ct = F.max_int_type(F.promote_integer(i1), F.promote_integer(i2));
      if(!ct) throw
        std::runtime_error("Internal error: cannot find common int");
      return ct;
    }
    Expression& resolve_type(const FundamentalTypeSet* F) {
      if(!(get_lhs().has_type() && get_rhs().has_type()))
      {
        return set_type(invalid_type);
      }
      if(!F) throw std::runtime_error(
        "Need fundamental type set to determine common integers"
      );

      return  set_type({
            get_bitwise_result_type(
              get_lhs().get_raw_type(),
              get_rhs().get_raw_type(),
              *F),
            get_lhs().get_qual() | get_rhs().get_qual()
          });
    }

    Expression& update_type(const FundamentalTypeSet* F)
    {
      if(!(get_lhs().has_type() && get_rhs().has_type())) update_subtypes(F);
      if(F && !has_type()) return resolve_type(F);
      return *this;
    }

    BitwiseExpression(
      std::unique_ptr<Expression> l,
      std::unique_ptr<Expression> r,
      QualifiedType t
    ): BinaryExpression(std::move(l), std::move(r), t) {}

    BitwiseExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): BinaryExpression(
      std::move(l), std::move(r), nullptr
    ) {
      resolve_type(&F);
    }
  };

  class LeftShiftExpression: public BitwiseExpression
  {
  public:
    LeftShiftExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): BitwiseExpression(std::move(l), std::move(r), F) {}
    LeftShiftExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): BitwiseExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " << " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<LeftShiftExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class RightShiftExpression: public BitwiseExpression
  {
  public:
    RightShiftExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): BitwiseExpression(std::move(l), std::move(r), F) {}
    RightShiftExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): BitwiseExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " >> " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<RightShiftExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class BitwiseAndExpression: public BitwiseExpression
  {
  public:
    BitwiseAndExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): BitwiseExpression(std::move(l), std::move(r), F) {}
    BitwiseAndExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): BitwiseExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " & " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<BitwiseAndExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class BitwiseOrExpression: public BitwiseExpression
  {
  public:
    BitwiseOrExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): BitwiseExpression(std::move(l), std::move(r), F) {}
    BitwiseOrExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): BitwiseExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " ^ " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<BitwiseOrExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class BitwiseXorExpression: public BitwiseExpression
  {
  public:
    BitwiseXorExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): BitwiseExpression(std::move(l), std::move(r), F) {}
    BitwiseXorExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): BitwiseExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " ^ " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<BitwiseXorExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

}

#endif
