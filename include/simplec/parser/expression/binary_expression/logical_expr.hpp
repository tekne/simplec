#ifndef SIMPLEC_PARSER_LOGICAL_EXPRESSION_INCLUDED
#define SIMPLEC_PARSER_LOGICAL_EXPRESSION_INCLUDED

#include <simplec/parser/types/fundamental_type_set.hpp>
#include <simplec/parser/expression/binary_expression.hpp>

namespace simplec
{

  class LogicalBinaryExpression: public BinaryExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F) final {
      if(!(get_lhs().has_type() && get_rhs().has_type()))
        return set_type(invalid_type);
      if(!F) throw std::runtime_error(
        "A fundamental integer type set is needed to resolve int"
      );

      std::shared_ptr<const ScalarType> s1 =
        std::dynamic_pointer_cast<const ScalarType>(get_lhs().get_raw_type());
      std::shared_ptr<const ScalarType> s2 =
        std::dynamic_pointer_cast<const ScalarType>(get_rhs().get_raw_type());
      if(!s1) throw
        std::runtime_error("LHS of logical expression must be a scalar");
      if(!s2) throw
        std::runtime_error("RHS of logical expression must be a scalar");

      return set_type(F->get_int_ptr());
    }

    LogicalBinaryExpression(
      std::unique_ptr<Expression> l,
      std::unique_ptr<Expression> r,
      QualifiedType t
    ): BinaryExpression(std::move(l), std::move(r), t) {}

    LogicalBinaryExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): BinaryExpression(
      std::move(l), std::move(r), nullptr
    ) {
      resolve_type(&F);
    }

    Expression& update_type(const FundamentalTypeSet* F)
    {
      if(!(get_lhs().has_type() && get_rhs().has_type())) update_subtypes(F);
      if(F && !has_type()) return resolve_type(F);
      return *this;
    }
  };

  class LogicalAndExpression: public LogicalBinaryExpression
  {
  public:
    LogicalAndExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): LogicalBinaryExpression(std::move(l), std::move(r), F) {}
    LogicalAndExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): LogicalBinaryExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " && " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<LogicalAndExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class LogicalOrExpression: public LogicalBinaryExpression
  {
  public:
    LogicalOrExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): LogicalBinaryExpression(std::move(l), std::move(r), F) {}
    LogicalOrExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): LogicalBinaryExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " || " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<LogicalOrExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class LessThanExpression: public LogicalBinaryExpression
  {
  public:
    LessThanExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): LogicalBinaryExpression(std::move(l), std::move(r), F) {}
    LessThanExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): LogicalBinaryExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " < " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<LessThanExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class GreaterThanExpression: public LogicalBinaryExpression
  {
  public:
    GreaterThanExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): LogicalBinaryExpression(std::move(l), std::move(r), F) {}
    GreaterThanExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): LogicalBinaryExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " > " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<GreaterThanExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class LEQExpression: public LogicalBinaryExpression
  {
  public:
    LEQExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): LogicalBinaryExpression(std::move(l), std::move(r), F) {}
    LEQExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): LogicalBinaryExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " <= " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<LEQExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class GEQExpression: public LogicalBinaryExpression
  {
  public:
    GEQExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): LogicalBinaryExpression(std::move(l), std::move(r), F) {}
    GEQExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): LogicalBinaryExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " >= " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<GEQExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class EqualityExpression: public LogicalBinaryExpression
  {
  public:
    EqualityExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): LogicalBinaryExpression(std::move(l), std::move(r), F) {}
    EqualityExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): LogicalBinaryExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " == " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<EqualityExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class InequalityExpression: public LogicalBinaryExpression
  {
  public:
    InequalityExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): LogicalBinaryExpression(std::move(l), std::move(r), F) {}
    InequalityExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): LogicalBinaryExpression(std::move(l), std::move(r), std::move(t)) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " != " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<InequalityExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

}

#endif
