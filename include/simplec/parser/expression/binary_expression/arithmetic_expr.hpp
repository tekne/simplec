#ifndef SIMPLEC_PARSER_ARITHMETIC_EXPRESSION_INCLUDED
#define SIMPLEC_PARSER_ARITHMETIC_EXPRESSION_INCLUDED

#include <simplec/parser/expression/binary_expression.hpp>
#include <simplec/parser/types/integer_type.hpp>
#include <simplec/parser/types/floating_type.hpp>
#include <simplec/parser/types/pointer_type.hpp>
#include <simplec/parser/types/arithmetic_type.hpp>
#include <simplec/parser/types/vector_type.hpp>

#include <simplec/parser/types/fundamental_type_set.hpp>

#include <memory>
#include <ostream>

namespace simplec
{

  class ArithmeticExpression: public BinaryExpression
  {
  public:
    static std::shared_ptr<const ScalarType> get_arithmetic_result_type(
      std::shared_ptr<const Type> t1, std::shared_ptr<const Type> t2,
      const FundamentalTypeSet& F
    ) {
      std::shared_ptr<const ScalarType> s1 =
        std::dynamic_pointer_cast<const ScalarType>(t1);
      std::shared_ptr<const ScalarType> s2 =
        std::dynamic_pointer_cast<const ScalarType>(t2);
      if(!s1) throw
        std::runtime_error("LHS of arithmetic expression must be a scalar");
      if(!s2) throw
        std::runtime_error("RHS of arithmetic expression must be a scalar");
      auto ct = F.scalar_common_type(F.promote(s1), F.promote(s2));
      if(!ct) throw
        std::runtime_error("Invalid operands for arithmetic expression");
      return ct;
    }

    Expression& resolve_type(const FundamentalTypeSet* F) {
      if(!(get_lhs().has_type() && get_rhs().has_type()))
      {
        return set_type(invalid_type);
      }
      if(!F) throw std::runtime_error(
        "Need fundamental type set to determine common integers"
      );
      return  set_type({
            get_arithmetic_result_type(
              get_lhs().get_raw_type(),
              get_rhs().get_raw_type(),
              *F),
            get_lhs().get_qual() | get_rhs().get_qual()
          });
    }

    Expression& update_type(const FundamentalTypeSet* F)
    {
      if(!(get_lhs().has_type() && get_rhs().has_type())) update_subtypes(F);
      if(F && !has_type()) return resolve_type(F);
      return *this;
    }

    ArithmeticExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): BinaryExpression(std::move(l), std::move(r), std::move(t)) {}
    ArithmeticExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): BinaryExpression(
      std::move(l), std::move(r), nullptr
    ) {
      resolve_type(&F);
    }
  };

  class AdditionExpression: public ArithmeticExpression
  {
  public:
    AdditionExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): ArithmeticExpression(std::move(l), std::move(r), std::move(t)) {}
    AdditionExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): ArithmeticExpression(std::move(l), std::move(r), F) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " + " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<AdditionExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class MultiplicationExpression: public ArithmeticExpression
  {
  public:
    MultiplicationExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): ArithmeticExpression(std::move(l), std::move(r), std::move(t)) {}
    MultiplicationExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): ArithmeticExpression(std::move(l), std::move(r), F) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " * " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<MultiplicationExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class SubtractionExpression: public ArithmeticExpression
  {
  public:
    SubtractionExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): ArithmeticExpression(std::move(l), std::move(r), std::move(t)) {}
    SubtractionExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): ArithmeticExpression(std::move(l), std::move(r), F) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " - " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<SubtractionExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class DivisionExpression: public ArithmeticExpression
  {
  public:
    DivisionExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): ArithmeticExpression(std::move(l), std::move(r), std::move(t)) {}
    DivisionExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): ArithmeticExpression(std::move(l), std::move(r), F) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " / " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<DivisionExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

  class ModuloExpression: public ArithmeticExpression
  {
  public:
    ModuloExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ): ArithmeticExpression(std::move(l), std::move(r), std::move(t)) {}
    ModuloExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      const FundamentalTypeSet& F
    ): ArithmeticExpression(std::move(l), std::move(r), F) {}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_lhs() << " % " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<ModuloExpression>(
        get_lhs().clone(), get_rhs().clone(), get_type()
      );
    }
  };

}

#endif
