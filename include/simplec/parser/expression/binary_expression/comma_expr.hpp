#ifndef SIMPLEC_PARSER_ARRAY_SUBSCRIPT_INCLUDED
#define SIMPLEC_PARSER_ARRAY_SUBSCRIPT_INCLUDED

#include <simplec/parser/expression/binary_expression.hpp>
#include <memory>
#include <exception>
#include <ostream>

namespace simplec
{

  class CommaExpression: public BinaryExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      return set_type(get_rhs().get_type());
    }

    CommaExpression(
      std::unique_ptr<Expression> l,
      std::unique_ptr<Expression> r
    ): BinaryExpression(std::move(l), std::move(r), nullptr)
    {
      resolve_type(nullptr);
    }

    const Expression& print_to_stream(std::ostream& str) const final {
      str << "(" << get_lhs() << " , " << get_rhs() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const final {
      return std::make_unique<CommaExpression>(
        get_lhs().clone(), get_rhs().clone());
    }
  };

}

#endif
