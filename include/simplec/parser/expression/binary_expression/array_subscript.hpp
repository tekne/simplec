#ifndef SIMPLEC_PARSER_ARRAY_SUBSCRIPT_INCLUDED
#define SIMPLEC_PARSER_ARRAY_SUBSCRIPT_INCLUDED

#include <simplec/parser/expression/binary_expression.hpp>
#include <memory>
#include <exception>
#include <ostream>

namespace simplec
{

  class ArraySubscript: public BinaryExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!(get_lhs().has_type() && get_rhs().has_type()))
        return set_type(invalid_type);

      auto p1 =
        dynamic_cast<const PointerType*>(get_lhs().get_raw_type().get());
      auto p2 =
        dynamic_cast<const PointerType*>(get_rhs().get_raw_type().get());
      auto i1 =
        dynamic_cast<const IntegerType*>(get_lhs().get_raw_type().get());
      auto i2 =
        dynamic_cast<const IntegerType*>(get_rhs().get_raw_type().get());
      if(!(i1 || i2)) throw
        std::runtime_error("Subscript must be of integer type");
      else if(p1 && p2) throw
        std::runtime_error("Cannot subscript pointer with another pointer");
      else if(p1) set_type(p1->base_type());
      else if(p2) set_type(p2->base_type());
      else throw
        std::runtime_error("Cannot subscript non-pointer");
      return *this;
    }

    ArraySubscript(
      std::unique_ptr<Expression> l,
      std::unique_ptr<Expression> r
    ): BinaryExpression(std::move(l), std::move(r), nullptr)
    {
      resolve_type(nullptr);
    }

    const Expression& print_to_stream(std::ostream& str) const final {
      str << get_lhs() << "[" << get_rhs() << "]";
      return *this;
    }

    std::unique_ptr<Expression> clone() const final {
      return std::make_unique<ArraySubscript>(
        get_lhs().clone(), get_rhs().clone());
    }
  };

}

#endif
