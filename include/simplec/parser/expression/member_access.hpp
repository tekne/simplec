#ifndef SIMPLEC_PARSER_MEMBER_ACCESS_INCLUDED
#define SIMPLEC_PARSER_MEMBER_ACCESS_INCLUDED

#include <simplec/parser/expression/expression.hpp>
#include <simplec/parser/expression/identifier.hpp>
#include <simplec/parser/types/type_id_map.hpp>
#include <simplec/parser/types/pointer_type.hpp>
#include <memory>
#include <exception>

namespace simplec
{

  class MemberAccessExpression: public Expression
  {
    std::unique_ptr<Expression> base;
    Identifier member;
  public:
    MemberAccessExpression(
      std::unique_ptr<Expression> b, Identifier id,
      QualifiedType t
    ): Expression(std::move(t)),
    base(std::move(b)),
    member(std::move(id))
    {}

    bool is_constant() const {return false;}

    const Expression& get_base() const {return *base;}
    const Identifier& get_id() const {return member;}
    const std::string& get_id_str() const {return member.get_id();}

    Expression& update_type(const FundamentalTypeSet* F)
    {
      if(!base->has_type()) base->update_type(F);
      if(!has_type()) return resolve_type(F);
      return *this;
    }

    Expression& refresh_type(const FundamentalTypeSet* F)
    {
      base->refresh_type(F);
      return resolve_type(F);
    }
  };

  class DirectMemberAccess: public MemberAccessExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!get_base().has_type()) return set_type(invalid_type);

      if(!get_base().get_type().is_complete()) throw std::runtime_error(
        "Accessed type must be complete"
      );

      auto tp = dynamic_cast<const TypeIDMap*>(get_base().get_raw_type().get());
      if(!tp) throw std::runtime_error("Accessed type must satisfy TypeIDMap");

      auto t = tp->recursive_get_type(get_id_str());
      t.set_qual(t.get_qual() | get_qual());
      return set_type(t);
    }

    DirectMemberAccess(
      std::unique_ptr<Expression> b, Identifier id,
      QualifiedType t
    ): MemberAccessExpression(std::move(b), std::move(id), std::move(t)) {}
    DirectMemberAccess(std::unique_ptr<Expression> b, Identifier id):
    MemberAccessExpression(std::move(b), std::move(id), nullptr) {
      resolve_type(nullptr);
    }

    const Expression& print_to_stream(std::ostream& str) const {
      get_base().print_to_stream(str);
      str << ".";
      get_id().print_to_stream(str);
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<DirectMemberAccess>(
        get_base().clone(),
        get_id(),
        get_type()
      );
    }
  };

  class PointerMemberAccess: public MemberAccessExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!get_base().has_type()) return set_type(invalid_type);

      auto pt =
        dynamic_cast<const PointerType*>(get_base().get_raw_type().get());
      if(!pt) throw std::runtime_error("Accessed type must be a pointer");

      if(!pt->base_type().is_complete()) throw std::runtime_error(
        "Type pointed to must be complete"
      );

      auto tp = dynamic_cast<const TypeIDMap*>(pt->raw_base_type().get());
      if(!tp) throw std::runtime_error(
        "Accessed type must be a pointer to struct or union"
      );

      auto t = tp->recursive_get_type(get_id_str());
      t.set_qual(t.get_qual() | get_qual());
      return set_type(t);
    }

    PointerMemberAccess(
      std::unique_ptr<Expression> b, Identifier id,
      QualifiedType t
    ): MemberAccessExpression(std::move(b), std::move(id), std::move(t)) {}
    PointerMemberAccess(std::unique_ptr<Expression> b, Identifier id):
    MemberAccessExpression(std::move(b), std::move(id), nullptr) {
      resolve_type(nullptr);
    }

    const Expression& print_to_stream(std::ostream& str) const {
      get_base().print_to_stream(str);
      str << "->";
      get_id().print_to_stream(str);
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<PointerMemberAccess>(
        get_base().clone(),
        get_id(),
        get_type()
      );
    }
  };

}

#endif
