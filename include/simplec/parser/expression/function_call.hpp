#ifndef SIMPLEC_PARSER_FUNCTION_CALL_INCLUDED
#define SIMPLEC_PARSER_FUNCTION_CALL_INCLUDED

#include <simplec/parser/expression/expression.hpp>
#include <simplec/parser/types/function_type.hpp>
#include <simplec/parser/types/pointer_type.hpp>
#include <simplec/parser/types/type.hpp>

#include <boost/container/small_vector.hpp>

#include <memory>

namespace simplec
{

  class FunctionCall: public Expression
  {
    static const size_t sv_size = 5;

    std::unique_ptr<Expression> callee;
    boost::container::small_vector<std::unique_ptr<Expression>, sv_size> args;
  public:

    typedef decltype(args) arglist_type;

    static QualifiedType return_type(Expression* c)
    {
      if(!c || !c->has_type()) return invalid_type;
      const Type* t = c->get_type().raw_type().get();
      const FunctionType* ft = dynamic_cast<const FunctionType*>(t);
      if(ft) return ft->return_type();

      const PointerType* pt = dynamic_cast<const PointerType*>(t);
      if(!pt) return invalid_type;

      ft = dynamic_cast<const FunctionType*>(pt->raw_base_type().get());
      if(!ft) return invalid_type;
      return ft->return_type();
    }

    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!callee->has_type()) return set_type(invalid_type);
      return set_type(return_type(callee.get()));
    }

    Expression& update_type(const FundamentalTypeSet* F)
    {
      if(!callee->has_type()) callee->update_type(F);
      for(auto& a : args)
      {
        if(!a->has_type()) a->update_type(F);
      }
      if(!has_type()) return resolve_type(F);
      return *this;
    }

    Expression& refresh_type(const FundamentalTypeSet* F)
    {
      callee->refresh_type(F);
      for(auto& a : args) a->refresh_type(F);
      return resolve_type(F);
    }

    FunctionCall(
      std::unique_ptr<Expression> c,
      arglist_type a
    ): Expression(return_type(c.get())),
    callee(std::move(c)),
    args(std::move(a))
    {
      for(size_t i = 0; i < args.size(); i++)
      {
        if(!args[i]) throw std::runtime_error(
          "Attempted to construct function call with null argument (at "
          + std::to_string(i) + ")"
        );
      }
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << *callee << ")" << "(";
      if(!args.empty())
      {
        str << *args[0];
        for(size_t i = 1; i < args.size(); i++)
          str << ", " << *args[i];
      }
      str << ")";
      return *this;
    }

    bool is_constant() const {return false;}
    const Expression& get_callee() const {return *callee;}
    const auto& get_args() const {return args;}

    std::unique_ptr<Expression> clone() const {
      boost::container::small_vector<std::unique_ptr<Expression>, sv_size> ne;
      ne.reserve(args.size());
      for(const auto& arg: args) ne.push_back(arg->clone());
      return std::make_unique<FunctionCall>(callee->clone(), std::move(ne));
    }
  };

}

#endif
