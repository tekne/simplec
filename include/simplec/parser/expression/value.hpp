#ifndef SIMPLEC_PARSER_VALUE_INCLUDED
#define SIMPLEC_PARSER_VALUE_INCLUDED

#include <simplec/parser/expression/expression.hpp>
#include <simplec/parser/types/fundamental_type_set.hpp>
#include <simplec/parser/types.hpp>
#include <simplec/parser/token/constant.hpp>
#include <simplec/parser/token/string_literal.hpp>
#include <simplec/preprocessor/token.hpp>
#include <cstdint>

namespace simplec
{

  class Value: public Expression
  {
    token_metadata md;
  public:
    Value(QualifiedType t, token_metadata m): Expression(t), md(m) {}
    bool is_constant() const final {return true;}

    token_metadata metadata() const {return md;}
    Value& set_metadata(token_metadata m) {md = m; return *this;}
  };

  class IntegerValue: public Value
  {
  public:
    std::shared_ptr<const IntegerType> get_int_type() const {
      return std::dynamic_pointer_cast<const IntegerType>(get_raw_type());
    }

    IntegerValue(std::shared_ptr<const IntegerType> t, token_metadata m)
    : Value(std::move(t), m) {
      auto p = get_int_type().get();
      if(!p) throw std::runtime_error("Cannot have void integer constant");
    }
  };

  class UnsignedIntegerValue: public IntegerValue
  {
    uint64_t val;
  public:
    UnsignedIntegerValue(
      uint64_t v, std::shared_ptr<const IntegerType> t, token_metadata m = {}
    )
    : IntegerValue(t, m), val(v) {
      if(!get_int_type()->is_unsigned()) throw std::runtime_error(
        "Unsigned integer constant must have unsigned integer type"
      );
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << val;
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<UnsignedIntegerValue>(*this);
    }
  };

  class SignedIntegerValue: public IntegerValue
  {
    int64_t val;
  public:
    SignedIntegerValue(
      int64_t v, std::shared_ptr<const IntegerType> t, token_metadata m = {}
    )
    : IntegerValue(t, m), val(v) {
      if(!get_int_type()->is_signed()) throw std::runtime_error(
        "Signed integer constant must have signed integer type"
      );
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << val;
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<SignedIntegerValue>(*this);
    }
  };

  class FloatValue: public Value
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!F) throw std::runtime_error(
        "Value type needs a fundamental type set to be resolved"
      );
      return set_type(F->get_float_ptr());
    }

    float value;
    FloatValue(float val, const FundamentalTypeSet& F, token_metadata m = {})
    : Value(F.get_float_ptr(), m), value(val) {}
    const Expression& print_to_stream(std::ostream& str) const {
      str << value << "f";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<FloatValue>(*this);
    }
  };

  class DoubleValue: public Value
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!F) throw std::runtime_error(
        "Value type needs a fundamental type set to be resolved"
      );
      return set_type(F->get_double_ptr());
    }

    double value;
    DoubleValue(double val, const FundamentalTypeSet& F, token_metadata m = {})
    : Value(F.get_double_ptr(), m), value(val) {}
    const Expression& print_to_stream(std::ostream& str) const {
      str << value;
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<DoubleValue>(*this);
    }
  };

  class LongDoubleValue: public Value
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!F) throw std::runtime_error(
        "Value type needs a fundamental type set to be resolved"
      );
      return set_type(F->get_long_double_ptr());
    }

    long double value;

    LongDoubleValue(
      long double val, const FundamentalTypeSet& F, token_metadata m = {}):
    Value(F.get_long_double_ptr(), m), value(val) {}
    const Expression& print_to_stream(std::ostream& str) const {
      str << value << "l";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<LongDoubleValue>(*this);
    }
  };

  class StringValue: public Value
  {
    bool u8;
    std::string val;
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!F) throw std::runtime_error(
        "Fundamental type set is necessary to determine string type"
      );
      return set_type(F->get_ptr_to_const_char_ptr());
    }

    StringValue(
      std::string v, const FundamentalTypeSet& F, bool u = false,
      token_metadata m = {}
    ): Value(F.get_ptr_to_const_char_ptr(), m), u8(u), val(std::move(v)) {}

    const std::string& value() const {return val;}

    const Expression& print_to_stream(std::ostream& str) const {
      if(u8) str << "u8";
      str << "\"" << value() << "\"";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<StringValue>(*this);
    }
  };

  class WideStringValue: public Value
  {
    std::string val;
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!F) throw std::runtime_error(
        "Fundamental type set is necessary to determine wide string type"
      );
      return set_type(F->get_ptr_to_const_wchar_ptr());
    }

    WideStringValue(
      std::string v, const FundamentalTypeSet& F, token_metadata m = {}
    ): Value(F.get_ptr_to_const_wchar_ptr(), m), val(std::move(v)) {}

    const std::string& value() const {return val;}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "L\"" << value() << "\"";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<WideStringValue>(*this);
    }
  };

  class Char16StringValue: public Value
  {
    std::string val;
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!F) throw std::runtime_error(
        "Fundamental type set is necessary to determine char16_t string type"
      );
      return set_type(F->get_ptr_to_const_char16_ptr());
    }

    Char16StringValue(
      std::string v, const FundamentalTypeSet& F, token_metadata m = {}
    ):
    Value(F.get_ptr_to_const_char16_ptr(), m), val(std::move(v)) {}

    const std::string& value() const {return val;}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "u\"" << value() << "\"";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<Char16StringValue>(*this);
    }
  };

  class Char32StringValue: public Value
  {
    std::string val;
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!F) throw std::runtime_error(
        "Fundamental type set is necessary to determine char32_t string type"
      );
      return set_type(F->get_ptr_to_const_char32_ptr());
    }

    Char32StringValue(
      std::string v, const FundamentalTypeSet& F, token_metadata m = {}
    ):
    Value(F.get_ptr_to_const_char32_ptr(), m), val(std::move(v)) {}

    const std::string& value() const {return val;}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "U\"" << value() << "\"";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<Char32StringValue>(*this);
    }
  };

}

#endif
