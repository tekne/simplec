#ifndef SIMPLEC_PARSER_TERNARY_EXPRESSION_INCLUDED
#define SIMPLEC_PARSER_TERNARY_EXPRESSION_INCLUDED

#include <simplec/parser/expression/expression.hpp>
#include <simplec/parser/types/type.hpp>
#include <memory>

namespace simplec
{

  class TernaryExpression: public Expression
  {
    std::unique_ptr<Expression> condition;
    std::unique_ptr<Expression> true_result;
    std::unique_ptr<Expression> false_result;
  public:
    static std::shared_ptr<const PointerType> get_common_ptr_type(
      std::shared_ptr<const PointerType> p1,
      std::shared_ptr<const PointerType> p2,
      const FundamentalTypeSet& F
    ) {
      if(p1->base_type().is_void() || p2->base_type().is_void())
      {
        auto btq = p1->base_type().get_qual() | p2->base_type().get_qual();
        if(!btq.is_qualified()) return F.get_ptr_to_void_ptr();
        if(btq == const_qualified) return F.get_ptr_to_const_void_ptr();
        return std::make_shared<PointerType>(QualifiedType{nullptr, btq});
      }
      else if(p1->base_type() == p2->base_type())
      {
        auto btq = p1->base_type().get_qual() | p2->base_type().get_qual();
        if(btq == p1->base_type().get_qual()) return p1;
        else if(btq == p2->base_type().get_qual()) return p2;
        return std::make_shared<PointerType>(
          QualifiedType{p1->base_type().raw_type(), btq}
        );
      }
      else throw std::runtime_error(
        "Pointer types " + p1->to_string() + " and " + p2->to_string()
        + "have no common type"
      );
    }

    static std::shared_ptr<const ScalarType> get_common_arith_type(
      std::shared_ptr<const ArithmeticType> a1,
      std::shared_ptr<const ArithmeticType> a2,
      const FundamentalTypeSet& F
    ) {
      return F.promote(F.arith_common_type(a1, a2));
    }

    Expression& resolve_type(const FundamentalTypeSet* F) {
      if(!(true_result->has_type() && false_result->has_type()))
      {
        return set_type(invalid_type);
      }
      if(!F) throw std::runtime_error(
        "Need fundamental type set to determine common type"
      );

      auto rqual = true_result->get_qual() | false_result->get_qual();

      if(true_result->get_type().compatible_type(false_result->get_type()))
      {
        return set_type({
          true_result->get_raw_type(), rqual
        });
      }
      std::shared_ptr<const PointerType> p1 =
        std::dynamic_pointer_cast<const PointerType>(true_result->get_raw_type());
      std::shared_ptr<const PointerType> p2 =
        std::dynamic_pointer_cast<const PointerType>(false_result->get_raw_type());
      if(p1 && p2)
      {
        return set_type({
          get_common_ptr_type(p1, p2, *F), rqual
        });
      }
      std::shared_ptr<const ArithmeticType> a1 =
        std::dynamic_pointer_cast<const ArithmeticType>(
          true_result->get_raw_type());
      std::shared_ptr<const ArithmeticType> a2 =
        std::dynamic_pointer_cast<const ArithmeticType>(
          true_result->get_raw_type());
      if(a1 && a2)
      {
        return set_type({
          get_common_arith_type(a1, a2, *F), rqual
        });
      }
      throw std::runtime_error(
        "Types " + true_result->get_type().to_string()
        + " and " + false_result->get_type().to_string()
        + " have no common type"
      );
    }

    TernaryExpression(
      std::unique_ptr<Expression> cond,
      std::unique_ptr<Expression> tr,
      std::unique_ptr<Expression> fr,
      QualifiedType ct
    )
    : Expression(ct),
    condition(std::move(cond)),
    true_result(std::move(tr)),
    false_result(std::move(fr))
    {}

    TernaryExpression(
      std::unique_ptr<Expression> cond,
      std::unique_ptr<Expression> tr,
      std::unique_ptr<Expression> fr,
      const FundamentalTypeSet& F
    )
    : Expression(invalid_type),
    condition(std::move(cond)),
    true_result(std::move(tr)),
    false_result(std::move(fr))
    {
      resolve_type(&F);
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << *condition << " ? " << *true_result << " : "
          << *false_result << ")";
      return *this;
    }

    bool is_constant() const {
      return
        condition->is_constant()
        && true_result->is_constant()
        && false_result->is_constant();
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<TernaryExpression>(
        condition->clone(), true_result->clone(), false_result->clone(),
        get_type()
      );
    }
  };

}

#endif
