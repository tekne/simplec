#ifndef SIMPLEC_UNARY_CAST_INCLUDED
#define SIMPLEC_UNARY_CAST_INCLUDED

#include <simplec/parser/expression/unary_expression.hpp>
#include <simplec/parser/expression/identifier.hpp>
#include <simplec/parser/types/type_set.hpp>
#include <simplec/parser/types/scalar_type.hpp>

namespace simplec
{

  class CastExpression: public UnaryExpression
  {
  public:
    CastExpression(
      std::unique_ptr<Expression> a,
      QualifiedType t
    ): UnaryExpression(std::move(a), std::move(t)) {}

    bool is_constant() const final {return get_arg().is_constant();}

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(";
      get_type().print_to_stream(str);
      str << ")" << "(" << get_arg() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<CastExpression>(
        get_arg().clone(), get_type()
      );
    }
  };

}

#endif
