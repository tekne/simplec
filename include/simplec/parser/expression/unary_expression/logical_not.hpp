#ifndef SIMPLEC_UNARY_LOGICAL_NOT_INCLUDED
#define SIMPLEC_UNARY_LOGICAL_NOT_INCLUDED

#include <simplec/parser/expression/unary_expression.hpp>
#include <simplec/parser/types/fundamental_type_set.hpp>
#include <simplec/parser/types/scalar_type.hpp>

namespace simplec
{

  class LogicalNotExpression: public UnaryExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!get_arg().has_type()) return set_type(invalid_type);
      if(!F) throw std::runtime_error(
        "A fundamental integer type set is needed to resolve int"
      );

      std::shared_ptr<const ScalarType> st =
        std::dynamic_pointer_cast<const ScalarType>(get_arg().get_raw_type());
      if(!st) throw
        std::runtime_error("Logical not is only defined for scalars");

      return set_type(F->get_int_ptr());
    }

    Expression& update_type(const FundamentalTypeSet* F) {
      if(!get_arg().has_type()) update_subtypes(F);
      if(F && !has_type()) return resolve_type(F);
      return *this;
    }

    LogicalNotExpression(
      std::unique_ptr<Expression> a,
      QualifiedType t = invalid_type
    ): UnaryExpression(std::move(a), t) {}
    LogicalNotExpression(
      std::unique_ptr<Expression> a,
      const FundamentalTypeSet& F
    ): UnaryExpression(std::move(a), nullptr) {
      resolve_type(&F);
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(!" << get_arg() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<LogicalNotExpression>(
        get_arg().clone(), get_type()
      );
    }
  };

}

#endif
