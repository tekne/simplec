#ifndef SIMPLEC_PARSER_UNARY_POINTER_EXPRESSION_INCLUDED
#define SIMPLEC_PARSER_UNARY_POINTER_EXPRESSION_INCLUDED

#include <simplec/parser/expression/unary_expression.hpp>
#include <simplec/parser/types/pointer_type.hpp>
#include <ostream>
#include <memory>
#include <exception>

namespace simplec
{

  class IndirectionExpression: public UnaryExpression
  {
  public:
    static QualifiedType get_pointed_type(
      std::shared_ptr<const Type> t)
    {
      const PointerType* pt = dynamic_cast<const PointerType*>(t.get());
      if(!pt) throw std::runtime_error("Only pointers can be dereferenced");
      return pt->base_type();
    }
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!get_arg().has_type()) return set_type(invalid_type);
      return set_type(get_pointed_type(get_arg().get_raw_type()));
    }

    IndirectionExpression(
      std::unique_ptr<Expression> a,
      QualifiedType t
    ): UnaryExpression(std::move(a), t) {}
    IndirectionExpression(
      std::unique_ptr<Expression> a
    ): UnaryExpression(std::move(a), invalid_type)
    {
      resolve_type(nullptr);
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(*" << get_arg() << ")";
      return *this;
    }
    std::unique_ptr<Expression> clone() const {
      return std::make_unique<IndirectionExpression>(
        get_arg().clone(), get_type()
      );
    }
  };

  class AddressExpression: public UnaryExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F) {
      if(!get_arg().has_type()) return set_type(invalid_type);
      return set_type(PointerType::make_pointer(get_arg().get_type()));
    }

    AddressExpression(
      std::unique_ptr<Expression> a,
      QualifiedType t
    ): UnaryExpression(std::move(a), t) {}
    AddressExpression(
      std::unique_ptr<Expression> a
    ): UnaryExpression(std::move(a), invalid_type)
    {
      resolve_type(nullptr);
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(&" << get_arg() << ")";
      return *this;
    }
    std::unique_ptr<Expression> clone() const {
      return std::make_unique<AddressExpression>(
        get_arg().clone(), get_type()
      );
    }
  };

};

#endif
