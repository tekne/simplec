#ifndef SIMPLEC_PARSER_UNARY_ARITHMETIC_EXPRESSION_INCLUDED
#define SIMPLEC_PARSER_UNARY_ARITHMETIC_EXPRESSION_INCLUDED

#include <simplec/parser/expression/unary_expression.hpp>
#include <simplec/parser/types/fundamental_type_set.hpp>
#include <simplec/parser/types/scalar_type.hpp>

namespace simplec
{

  class NegationExpression: public UnaryExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!get_arg().has_type()) return set_type(invalid_type);

      std::shared_ptr<const ScalarType> st =
        std::dynamic_pointer_cast<const ScalarType>(get_arg().get_raw_type());
      if(!st) throw
        std::runtime_error("Only scalars can be negated");
      return set_type(F->promote(st));
    }

    Expression& update_type(const FundamentalTypeSet* F) {
      if(!get_arg().has_type()) update_subtypes(F);
      if(F && !has_type()) return resolve_type(F);
      return *this;
    }

    NegationExpression(
      std::unique_ptr<Expression> a,
      QualifiedType t = invalid_type
    ): UnaryExpression(std::move(a), t) {}
    NegationExpression(
      std::unique_ptr<Expression> a,
      const FundamentalTypeSet& F
    ): UnaryExpression(std::move(a), nullptr) {
      resolve_type(&F);
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(-" << get_arg() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<NegationExpression>(
        get_arg().clone(), get_type()
      );
    }
  };

  class PromotionExpression: public UnaryExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!get_arg().has_type()) return set_type(invalid_type);

      std::shared_ptr<const ScalarType> st =
        std::dynamic_pointer_cast<const ScalarType>(get_arg().get_raw_type());
      if(!st) throw
        std::runtime_error("Only scalars can be promoted");
      return set_type(F->promote(st));
    }

    Expression& update_type(const FundamentalTypeSet* F) {
      if(!get_arg().has_type()) update_subtypes(F);
      if(F && !has_type()) return resolve_type(F);
      return *this;
    }

    PromotionExpression(
      std::unique_ptr<Expression> a,
      QualifiedType t = invalid_type
    ): UnaryExpression(std::move(a), t) {}
    PromotionExpression(
      std::unique_ptr<Expression> a,
      const FundamentalTypeSet& F
    ): UnaryExpression(std::move(a), nullptr) {
      resolve_type(&F);
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(+" << get_arg() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<PromotionExpression>(
        get_arg().clone(), get_type()
      );
    }
  };

  class PrefixIncrementExpression: public UnaryExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!get_arg().has_type()) return set_type(invalid_type);

      std::shared_ptr<const ScalarType> st =
        std::dynamic_pointer_cast<const ScalarType>(get_arg().get_raw_type());
      if(!st) throw
        std::runtime_error("Only scalars can be prefix-incremented");
      return set_type(F->promote(st));
    }

    Expression& update_type(const FundamentalTypeSet* F) {
      if(!get_arg().has_type()) update_subtypes(F);
      if(F && !has_type()) return resolve_type(F);
      return *this;
    }


    PrefixIncrementExpression(
      std::unique_ptr<Expression> a,
      QualifiedType t = invalid_type
    ): UnaryExpression(std::move(a), t) {}
    PrefixIncrementExpression(
      std::unique_ptr<Expression> a,
      const FundamentalTypeSet& F
    ): UnaryExpression(std::move(a), nullptr) {
      resolve_type(&F);
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(++" << get_arg() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<PrefixIncrementExpression>(
        get_arg().clone(), get_type()
      );
    }
  };

  class PrefixDecrementExpression: public UnaryExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!get_arg().has_type()) return set_type(invalid_type);

      std::shared_ptr<const ScalarType> st =
        std::dynamic_pointer_cast<const ScalarType>(get_arg().get_raw_type());
      if(!st) throw
        std::runtime_error("Only scalars can be prefix-decremented");
      return set_type(F->promote(st));
    }

    Expression& update_type(const FundamentalTypeSet* F) {
      if(!get_arg().has_type()) update_subtypes(F);
      if(F && !has_type()) return resolve_type(F);
      return *this;
    }

    PrefixDecrementExpression(
      std::unique_ptr<Expression> a,
      QualifiedType t = invalid_type
    ): UnaryExpression(std::move(a), t) {}
    PrefixDecrementExpression(
      std::unique_ptr<Expression> a,
      const FundamentalTypeSet& F
    ): UnaryExpression(std::move(a), nullptr) {
      resolve_type(&F);
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(--" << get_arg() << ")";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<PrefixDecrementExpression>(
        get_arg().clone(), get_type()
      );
    }
  };

  class PostfixIncrementExpression: public UnaryExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!get_arg().has_type()) return set_type(invalid_type);

      std::shared_ptr<const ScalarType> st =
        std::dynamic_pointer_cast<const ScalarType>(get_arg().get_raw_type());
      if(!st) throw
        std::runtime_error("Only scalars can be postfix-incremented");
      return set_type(F->promote(st));
    }

    Expression& update_type(const FundamentalTypeSet* F) {
      if(!get_arg().has_type()) update_subtypes(F);
      if(F && !has_type()) return resolve_type(F);
      return *this;
    }

    PostfixIncrementExpression(
      std::unique_ptr<Expression> a,
      QualifiedType t = invalid_type
    ): UnaryExpression(std::move(a), t) {}
    PostfixIncrementExpression(
      std::unique_ptr<Expression> a,
      const FundamentalTypeSet& F
    ): UnaryExpression(std::move(a), nullptr) {
      resolve_type(&F);
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_arg() << "++)";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<PostfixIncrementExpression>(
        get_arg().clone(), get_type()
      );
    }
  };

  class PostfixDecrementExpression: public UnaryExpression
  {
  public:
    Expression& resolve_type(const FundamentalTypeSet* F)
    {
      if(!get_arg().has_type()) return set_type(invalid_type);

      std::shared_ptr<const ScalarType> st =
        std::dynamic_pointer_cast<const ScalarType>(get_arg().get_raw_type());
      if(!st) throw
        std::runtime_error("Only scalars can be postfix-decremented");
      return set_type(F->promote(st));
    }

    Expression& update_type(const FundamentalTypeSet* F) {
      if(!get_arg().has_type()) update_subtypes(F);
      if(F && !has_type()) return resolve_type(F);
      return *this;
    }

    PostfixDecrementExpression(
      std::unique_ptr<Expression> a,
      QualifiedType t = invalid_type
    ): UnaryExpression(std::move(a), t) {}
    PostfixDecrementExpression(
      std::unique_ptr<Expression> a,
      const FundamentalTypeSet& F
    ): UnaryExpression(std::move(a), nullptr) {
      resolve_type(&F);
    }

    const Expression& print_to_stream(std::ostream& str) const {
      str << "(" << get_arg() << "--)";
      return *this;
    }

    std::unique_ptr<Expression> clone() const {
      return std::make_unique<PostfixDecrementExpression>(
        get_arg().clone(), get_type()
      );
    }
  };

}

#endif
