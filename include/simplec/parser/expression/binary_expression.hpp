#ifndef SIMPLEC_PARSER_BINARY_EXPRESSION_INCLUDED
#define SIMPLEC_PARSER_BINARY_EXPRESSION_INCLUDED

#include <simplec/parser/expression/expression.hpp>
#include <memory>

namespace simplec
{

  class BinaryExpression: public Expression
  {
    std::unique_ptr<Expression> lhs;
    std::unique_ptr<Expression> rhs;
  public:
    BinaryExpression(
      std::unique_ptr<Expression> l, std::unique_ptr<Expression> r,
      QualifiedType t
    ):
    Expression(t), lhs(std::move(l)), rhs(std::move(r)) {}

    const Expression& get_lhs() const {return *lhs;}
    const Expression& get_rhs() const {return *rhs;}

    bool is_constant() const {
      return lhs && rhs && !(lhs->is_constant() || rhs->is_constant());
    }

    Expression& update_subtypes(const FundamentalTypeSet* F)
    {
      if(!lhs->has_type()) lhs->update_type(F);
      if(!rhs->has_type()) rhs->update_type(F);
      return *this;
    }

    Expression& update_type(const FundamentalTypeSet* F)
    {
      update_subtypes(F);
      if(!has_type()) return resolve_type(F);
      return *this;
    }

    Expression& refresh_type(const FundamentalTypeSet* F)
    {
      lhs->refresh_type(F);
      rhs->refresh_type(F);
      return resolve_type(F);
    }
  };

}

#endif
