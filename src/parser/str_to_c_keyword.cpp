#include <simplec/parser/token/keyword.hpp>
#include <string>

namespace simplec
{

  C_keyword str_to_C_keyword(const std::string& str)
  {
    if(str.size() < 2) return invalid_keyword;

    int diff = str.compare("float");
    if(!diff) return keyword_float;
    else if(diff < 0)
    {
      diff = str.compare("break");
      if(!diff) return keyword_break;
      else if(diff < 0)
      {
        diff = str.compare("_Imaginary");
        if(!diff) return keyword__Imaginary;
        else if(diff < 0)
        {
          diff = str.compare("_Bool");
          if(!diff) return keyword__Bool;
          else if(diff < 0)
          {
            if(str == "_Atomic") return keyword__Atomic;
            else if(str == "_Alignas") return keyword__Alignas;
          }
          else
          {
            if(str == "_Complex") return keyword__Complex;
            else if(str == "_Generic") return keyword__Generic;
          }
        }
        else
        {
          diff = str.compare("_Thread_local");
          if(!diff) return keyword__Thread_local;
          else if(diff < 0)
          {
            if(str == "_Noreturn") return keyword__Noreturn;
            else if(str == "_Static_assert") return keyword__Static_assert;
          }
          else
          {
            if(str == "alignof") return keyword_alignof;
            else if(str == "auto") return keyword_auto;
          }
        }
      }
      else
      {
        diff = str.compare("default");
        if(!diff) return keyword_default;
        else if(diff < 0)
        {
          diff = str.compare("const");
          if(!diff) return keyword_const;
          else if(diff < 0)
          {
            if(str == "case") return keyword_case;
            else if(str == "char") return keyword_char;
          }
          else
          {
            if(str == "continue") return keyword_continue;
          }
        }
        else
        {
          diff = str.compare("else");
          if(!diff) return keyword_else;
          else if(diff < 0)
          {
            if(str == "do") return keyword_do;
            else if(str == "double") return keyword_double;
          }
          else
          {
            if(str == "enum") return keyword_enum;
            else if(str == "extern") return keyword_extern;
          }
        }
      }
    }
    else
    {
      diff = str.compare("short");
      if(!diff) return keyword_short;
      else if(diff < 0)
      {
        diff = str.compare("int");
        if(!diff) return keyword_int;
        else if(diff < 0)
        {
          diff = str.compare("if");
          if(!diff) return keyword_if;
          else if(diff < 0)
          {
            if(str == "for") return keyword_for;
            else if(str == "goto") return keyword_goto;
          }
          else
          {
            if(str == "inline") return keyword_inline;
          }
        }
        else
        {
          diff = str.compare("restrict");
          if(!diff) return keyword_restrict;
          else if(diff < 0)
          {
            if(str == "long") return keyword_long;
            else if(str == "register") return keyword_register;
          }
          else
          {
            if(str == "return") return keyword_return;
          }
        }
      }
      else
      {
        diff = str.compare("typedef");
        if(!diff) return keyword_typedef;
        else if(diff < 0)
        {
          diff = str.compare("static");
          if(!diff) return keyword_static;
          else if(diff < 0)
          {
            if(str == "signed") return keyword_signed;
            else if(str == "sizeof") return keyword_sizeof;
          }
          else
          {
            if(str == "struct") return keyword_struct;
            else if(str == "switch") return keyword_switch;
          }
        }
        else
        {
          diff = str.compare("void");
          if(!diff) return keyword_void;
          else if(diff < 0)
          {
            if(str == "union") return keyword_union;
            if(str == "unsigned") return keyword_unsigned;
          }
          else
          {
            if(str == "volatile") return keyword_volatile;
            if(str == "while") return keyword_while;
          }
        }
      }
    }
    
    return invalid_keyword;

  }

}
