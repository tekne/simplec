#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/binary_expression/arithmetic_expr.hpp>

namespace simplec
{

  std::unique_ptr<Expression>
  ExpressionParser::parse_multiplicative_expression() {

    auto rslt = parse_cast_expression();
    if(!rslt) return nullptr;

    // Try to append as many operations as possible
    while(true)
    {
      auto tok = parse_token();
      switch(tok.get_punctuator())
      {
      case punctuator_star:
      {
        prev_unar = false;
        auto cexpr = parse_cast_expression();
        if(!cexpr) throw std::runtime_error("Expected cast expression after *");
        rslt = std::make_unique<MultiplicationExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      case punctuator_divide:
      {
        prev_unar = false;
        auto cexpr = parse_cast_expression();
        if(!cexpr) throw std::runtime_error("Expected cast expression after /");
        rslt = std::make_unique<DivisionExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      case punctuator_modulo:
      {
        prev_unar = false;
        auto cexpr = parse_cast_expression();
        if(!cexpr) throw std::runtime_error("Expected cast expression after %");
        rslt = std::make_unique<ModuloExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      default: break;
      }
      unget_token();
      return rslt;
    }
  }

}
