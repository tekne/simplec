#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/binary_expression/logical_expr.hpp>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_equality_expression() {

    auto rslt = parse_relational_expression();
    if(!rslt) return nullptr;

    // Try to append as many operations as possible
    while(true)
    {
      auto tok = parse_token();
      switch(tok.get_punctuator())
      {
      case punctuator_eq:
      {
        prev_unar = false;
        auto cexpr = parse_relational_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected relational expression after =="
        );
        rslt = std::make_unique<EqualityExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      case punctuator_neq:
      {
        prev_unar = false;
        auto cexpr = parse_relational_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected relational expression after !="
        );
        rslt = std::make_unique<InequalityExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      default: break;
      }
      unget_token();
      return rslt;
    }
  }

}
