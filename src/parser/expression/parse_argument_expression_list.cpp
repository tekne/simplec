#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/binary_expression/comma_expr.hpp>

namespace simplec
{

  FunctionCall::arglist_type ExpressionParser::parse_argument_expression_list()
  {
    FunctionCall::arglist_type rslt = {};

    auto aexpr = parse_assignment_expression();
    if(!aexpr) return rslt;
    rslt.push_back(std::move(aexpr));

    // Attempt to append as many comma-separated expressions as possible
    while(true)
    {
      auto tok = parse_token();
      if(tok.get_punctuator() != punctuator_comma)
      {
        unget_token();
        return rslt;
      }
      auto asgn = parse_assignment_expression();
      if(!asgn) throw std::runtime_error(
        "Expected assignment expression after ,");
      rslt.push_back(std::move(asgn));
    }
  }

}
