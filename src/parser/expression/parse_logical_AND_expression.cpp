#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/binary_expression/logical_expr.hpp>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_logical_AND_expression() {

    auto rslt = parse_OR_expression();
    if(!rslt) return nullptr;

    // Try to append as many operations as possible
    while(true)
    {
      auto tok = parse_token();
      if(tok.get_punctuator() == punctuator_and)
      {
        prev_unar = false;
        auto cexpr = parse_OR_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected OR expression after &&"
        );
        rslt = std::make_unique<LogicalAndExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      unget_token();
      return rslt;
    }
  }

}
