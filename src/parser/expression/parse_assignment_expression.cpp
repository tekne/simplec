#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/binary_expression/assignment_expr.hpp>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_assignment_expression()
  {
    auto rslt = parse_conditional_expression();

    // Check if previous expression was unary.
    if(!prev_unar || !rslt) return rslt;

    // Attempt to parse an assignment expression
    auto tok = parse_token();

    std::unique_ptr<Expression> rhs;
    auto punc = tok.get_punctuator();

    switch(punc)
    {
      case punctuator_assign: // =
      case punctuator_timeseq: // *=
      case punctuator_diveq: // /=
      case punctuator_modeq: // %=
      case punctuator_pluseq: // +=
      case punctuator_mineq: // -=
      case punctuator_lshifteq: // <<=
      case punctuator_rshifteq: // >>=
      case punctuator_andeq: // &=
      case punctuator_xoreq: // ^=
      case punctuator_oreq: // |=
        rhs = parse_assignment_expression();
        if(!rhs) throw std::runtime_error(
          "Expected assignment expression after assignment operator");
        break;
      default:
        unget_token();
        return rslt;
    }

    prev_unar = false;
    switch(punc)
    {
      case punctuator_assign: // =
        rslt = std::make_unique<Assignment>(std::move(rslt), std::move(rhs));
        break;
      case punctuator_timeseq: // *=
        rslt = std::make_unique<MultiplicationAssignment>(
          std::move(rslt), std::move(rhs));
        break;
      case punctuator_diveq: // /=
        rslt = std::make_unique<DivisionAssignment>(
          std::move(rslt), std::move(rhs));
        break;
      case punctuator_modeq: // %=
        rslt = std::make_unique<ModuloAssignment>(
          std::move(rslt), std::move(rhs));
        break;
      case punctuator_pluseq: // +=
        rslt = std::make_unique<AdditionAssignment>(
          std::move(rslt), std::move(rhs));
        break;
      case punctuator_mineq: // -=
        rslt = std::make_unique<SubtractionAssignment>(
          std::move(rslt), std::move(rhs));
        break;
      case punctuator_lshifteq: // <<=
        rslt = std::make_unique<LeftShiftAssignment>(
          std::move(rslt), std::move(rhs));
        break;
      case punctuator_rshifteq: // >>=
        rslt = std::make_unique<RightShiftAssignment>(
          std::move(rslt), std::move(rhs));
        break;
      case punctuator_andeq: // &=
        rslt = std::make_unique<AndAssignment>(
          std::move(rslt), std::move(rhs));
        break;
      case punctuator_xoreq: // ^=
        rslt = std::make_unique<XorAssignment>(
          std::move(rslt), std::move(rhs));
        break;
      case punctuator_oreq: // |=
        rslt = std::make_unique<OrAssignment>(
          std::move(rslt), std::move(rhs));
        break;
      default: throw std::runtime_error(
        "Internal error: invalid punctuator " + std::to_string(punc)
      );
    }

    return rslt;
  }

}
