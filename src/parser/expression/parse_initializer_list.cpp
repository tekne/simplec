#include <simplec/parser/expression.hpp>
#include <simplec/parser/token/io.hpp>
#include <boost/none.hpp>

namespace simplec
{

  InitializerList ExpressionParser::parse_initializer_list() {
    InitializerList result{};

    while(true)
    {
      // Attempt to parse a designator
      {
        auto tok = parse_token();
        if(tok.get_punctuator() == punctuator_period)
        {
          auto id = parse_token();
          if(id.get_type() != tok_identifier) throw std::runtime_error(
            "Expected identifier after . in initializer list"
          );
          result.push_back(Identifier{id});
        }
        //TODO: punctuator_lsquare
        else
        {
          unget_token();
        }
      }

      // If a designator has been parsed, expect an equal sign or another
      // designator beginning
      if(result.at_designation())
      {
        auto tok = parse_token();
        switch(tok.get_punctuator())
        {
          case punctuator_period: continue;
          // case punctuator_lsquare: continue;
          case punctuator_assign: break;
          default: throw std::runtime_error(
            "Expected = after designator, got " + tok_to_string(tok)
          );
        }
      }

      // Try to parse an initializer
      {
        auto init = parse_initializer();
        if(!init && result.at_designation()) throw std::runtime_error(
          "Expected initializer after ="
        );
        else if(init) result.push_back(std::move(init));

        // Continue if and only if the next token is a comma
        auto nxt = parse_token();
        if(nxt.get_punctuator() == punctuator_comma) continue;
        unget_token();
        return result;
      }
    }
  }

  boost::optional<InitializerList>
  ExpressionParser::parse_bracketed_initializer_list() {
    auto tok = parse_token();
    if(tok.get_punctuator() != punctuator_lcurly) {
      unget_token();
      return boost::none;
    }

    auto il = parse_initializer_list();

    auto nxt = parse_token();
    switch(nxt.get_punctuator())
    {
      case punctuator_rcurly: break;
      case punctuator_comma:
      {
        auto end = parse_token();
        if(end.get_punctuator() == punctuator_rcurly) break;
      }
      default: throw std::runtime_error(
        "Expected } to match {, got " +
        tok_to_string(nxt)
      );
    }

    return il;
  }

}
