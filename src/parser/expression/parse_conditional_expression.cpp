#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/ternary_expression.hpp>
#include <exception>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_conditional_expression()
  {
    auto lorexpr = parse_logical_OR_expression();
    if(!lorexpr) return nullptr;

    auto tok = parse_token();
    if(tok.get_punctuator() != punctuator_ternary)
    {
      unget_token();
      return lorexpr;
    }

    prev_unar = false;

    auto true_expr = parse_expression();
    if(!true_expr) throw std::runtime_error(
      "Expected expression after" + lorexpr->to_string() + " ?"
    );

    auto sep = parse_token();
    if(sep.get_punctuator() != punctuator_colon) throw std::runtime_error(
      "Expected : after " + lorexpr->to_string()
      + " ? " +  true_expr->to_string()
    );

    auto false_expr = parse_conditional_expression();
    if(!false_expr) throw std::runtime_error(
      "Expected conditional expression after "
      + lorexpr->to_string() + " ? " + true_expr->to_string() + " : "
    );

    return std::make_unique<TernaryExpression>(
      std::move(lorexpr), std::move(true_expr), std::move(false_expr),
      typeset
    );
  }

}
