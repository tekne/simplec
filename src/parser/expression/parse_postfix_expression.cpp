#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/binary_expression/array_subscript.hpp>
#include <simplec/parser/expression/member_access.hpp>
#include <simplec/parser/expression/unary_expression/unary_arith_expr.hpp>
#include <exception>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_postfix_expression()
  {
    auto result = parse_primary_expression();
    // NOTE: compound literals look like (identifier){initializer list}, and
    // hence can only be parsed if a primary expression parse is successful
    if(!result) return nullptr;

    //TODO: attempt to construct a compound literal

    // Now, append as many valid postfixes (e.g ++, [2]) as possible
    while(true)
    {
      auto tok = parse_token();
      switch(tok.get_punctuator())
      {
        case punctuator_lsquare:
        {
          auto expr = parse_expression();
          if(!expr) throw std::runtime_error("Expected expression after [");
          auto nxt = parse_token();
          if(nxt.get_punctuator() != punctuator_rsquare)
          throw std::runtime_error(
            "Expected ]"
          );
          parsed_buffer();
          return std::make_unique<ArraySubscript>(
            std::move(result), std::move(expr)
          );
        }
        case punctuator_lparen:
        {
          auto arglist = parse_argument_expression_list();
          auto nxt = parse_token();
          if(nxt.get_punctuator() != punctuator_rparen) throw
            std::runtime_error(
              "Expected ) after ("
            );
          return std::make_unique<FunctionCall>(
            std::move(result), std::move(arglist)
          );
        }
        case punctuator_period:
        {
          auto nxt = parse_token();
          if(nxt.get_type() == tok_identifier)
          {
            result = std::make_unique<DirectMemberAccess>(
              std::move(result), Identifier(std::move(nxt))
            );
            parsed_buffer();
            continue;
          }
          else throw
            std::runtime_error("Invalid member access: expected identifier");
        }
        case punctuator_arrow:
        {
          auto nxt = parse_token();
          if(nxt.get_type() == tok_identifier)
          {
            result = std::make_unique<PointerMemberAccess>(
              std::move(result), Identifier(std::move(nxt))
            );
            parsed_buffer();
            continue;
          }
          else throw
            std::runtime_error("Invalid pointer access: expected identifier");
        }
        case punctuator_increment:
          result =
            std::make_unique<PostfixIncrementExpression>(std::move(result));
          continue;
        case punctuator_decrement:
          result =
            std::make_unique<PostfixDecrementExpression>(std::move(result));
          continue;
        default:
          unget_token();
      }

      return result;
    }


  }

}
