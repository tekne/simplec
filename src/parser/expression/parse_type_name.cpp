#include <simplec/parser/expression.hpp>

#include <exception>

namespace simplec
{

  QualifiedType ExpressionParser::parse_type_name()
  {
    auto t = parse_specifier_qualifier_list();
    if(!t.is_valid()) return invalid_type;
    return parse_abstract_declarator(t);
  }

  QualifiedType ExpressionParser::parse_bracketed_type_name()
  {
    auto tok = parse_token();
    if(tok.get_punctuator() != punctuator_lparen)
    {
      unget_token();
      return invalid_type;
    }

    auto tn = parse_type_name();
    if(tn.is_invalid())
    {
      unget_token();
      return invalid_type;
    }

    auto end = parse_token();
    if(end.get_punctuator() != punctuator_rparen) throw std::runtime_error(
      "Expected ) after bracketed type name"
    );
    parsed_buffer();
    return tn;
  }

}
