#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/binary_expression/logical_expr.hpp>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_logical_OR_expression() {

    auto rslt = parse_logical_AND_expression();
    if(!rslt) return nullptr;

    // Try to append as many operations as possible
    while(true)
    {
      auto tok = parse_token();
      if(tok.get_punctuator() == punctuator_or)
      {
        prev_unar = false;
        auto cexpr = parse_logical_AND_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected logical AND expression after ||"
        );
        rslt = std::make_unique<LogicalOrExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      unget_token();
      return rslt;
    }
  }

}
