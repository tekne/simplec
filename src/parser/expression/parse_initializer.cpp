#include <simplec/parser/expression.hpp>

namespace simplec
{

  std::unique_ptr<Initializer> ExpressionParser::parse_initializer()
  {
    auto aexpr = parse_assignment_expression();
    if(aexpr) return std::make_unique<ExpressionInitializer>(std::move(aexpr));

    auto il = parse_bracketed_initializer_list();
    if(il) return std::make_unique<InitializerList>(std::move(*il));
    return nullptr;
  }

}
