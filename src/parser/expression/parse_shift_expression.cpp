#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/binary_expression/bitwise_expr.hpp>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_shift_expression() {

    auto rslt = parse_additive_expression();
    if(!rslt) return nullptr;

    // Try to append as many operations as possible
    while(true)
    {
      auto tok = parse_token();
      switch(tok.get_punctuator())
      {
      case punctuator_lshift:
      {
        prev_unar = false;
        auto cexpr = parse_additive_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected additive expression after <<"
        );
        rslt = std::make_unique<LeftShiftExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      case punctuator_rshift:
      {
        prev_unar = false;
        auto cexpr = parse_additive_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected additive expression after >>"
        );
        rslt = std::make_unique<RightShiftExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      default: break;
      }
      unget_token();
      return rslt;
    }
  }

}
