#include <simplec/parser/expression.hpp>
#include <simplec/parser/types/pointer_type.hpp>

namespace simplec
{

  QualifiedType ExpressionParser::parse_abstract_declarator(QualifiedType t)
  {
    auto tok = parse_token();
    if(tok.get_punctuator() == punctuator_star)
    {
      t = QualifiedType{typeset.resolve_pointer(std::move(t))};
      parsed_buffer();
    }
    else unget_token();
    return parse_direct_abstract_declarator(t);
  }

  QualifiedType ExpressionParser::parse_direct_abstract_declarator(QualifiedType t)
  {
    return t;
  }

}
