#include <simplec/parser/expression.hpp>

namespace simplec
{

  QualifiedType ExpressionParser::parse_specifier_qualifier_list()
  {
    auto tok = parse_token();
    switch(tok.get_raw_id())
    {
      //TODO: deal with pointers and const, volatile, etc.
      case lex_tok_identifier:
      {
        auto type = typeset.resolve_typedef(tok.data());
        if(type.is_valid()) return type;
        break;
      }
      //TODO: deal with struct, int, union, const, void, etc.
      case lex_tok_keyword: break;
      default: break;
    }
    unget_token();
    return invalid_type;
  }

}
