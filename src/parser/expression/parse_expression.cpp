#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/binary_expression/comma_expr.hpp>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_expression()
  {
    auto rslt = parse_assignment_expression();
    if(!rslt) return nullptr;

    // Attempt to append as many comma-separated expressions as possible
    while(true)
    {
      auto tok = parse_token();
      if(tok.get_punctuator() != punctuator_comma)
      {
        unget_token();
        return rslt;
      }
      auto asgn = parse_assignment_expression();
      if(!asgn) throw std::runtime_error(
        "Expected assignment expression after ,");
      rslt = std::make_unique<CommaExpression>(
        std::move(rslt), std::move(asgn)
      );
    }
  }

}
