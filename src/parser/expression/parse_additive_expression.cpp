#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/binary_expression/arithmetic_expr.hpp>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_additive_expression() {

    auto rslt = parse_multiplicative_expression();
    if(!rslt) return nullptr;

    // Try to append as many operations as possible
    while(true)
    {
      auto tok = parse_token();
      switch(tok.get_punctuator())
      {
      case punctuator_plus:
      {
        prev_unar = false;
        auto cexpr = parse_multiplicative_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected multiplicative expression after +"
        );
        rslt = std::make_unique<AdditionExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      case punctuator_minus:
      {
        prev_unar = false;
        auto cexpr = parse_multiplicative_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected multiplicative expression after -"
        );
        rslt = std::make_unique<SubtractionExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      default: break;
      }
      unget_token();
      return rslt;
    }
  }

}
