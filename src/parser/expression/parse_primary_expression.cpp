#include <simplec/parser/expression.hpp>
#include <simplec/parser/token/punctuator.hpp>
#include <simplec/parser/expression/identifier.hpp>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_primary_expression()
  {
    auto val = parse_value();
    if(val) return val;

    auto tok = parse_token();
    std::unique_ptr<Expression> rslt;
    switch(tok.get_raw_id())
    {
      case lex_tok_identifier:
        if(tok.is_eof() || is_typedef(tok.data()))
        {
          unget_token();
          return nullptr;
        }
        rslt = std::make_unique<Identifier>(std::move(tok)); break;
        parsed_buffer();
      case lex_tok_punctuator:
        if(tok.get_punctuator() == punctuator_lparen)
        {
          auto expr = parse_expression();
          if(!expr)
          {
            unget_token();
            return nullptr;
          }
          auto nxt = parse_token();
          if(nxt.get_punctuator() != punctuator_rparen) throw
            std::runtime_error("Expected ) after expression");
          parsed_buffer();
          return expr;
        }
      default: unget_token(); return nullptr;
    }
    return rslt;
  }

}
