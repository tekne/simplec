#include <simplec/parser/expression/unary_expression/bitwise_not.hpp>
#include <simplec/parser/expression/unary_expression/logical_not.hpp>
#include <simplec/parser/expression/unary_expression/unary_arith_expr.hpp>
#include <simplec/parser/expression/unary_expression/pointer_expression.hpp>
#include <simplec/parser/expression.hpp>
#include <simplec/parser/token/punctuator.hpp>

#include <exception>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_unary_expression()
  {
    auto postfix = parse_postfix_expression();
    if(postfix) return postfix;

    auto tok = parse_token();
    if(is_unary_operator(tok.get_punctuator()))
    {
      auto punc = tok.get_punctuator();
      auto cexpr = parse_cast_expression();
      if(!cexpr)
      {
        unget_token();
        return nullptr;
      }
      parsed_buffer();

      switch(punc)
      {
        case punctuator_ampersand:
          return std::make_unique<AddressExpression>(std::move(cexpr));
        case punctuator_star:
          return std::make_unique<IndirectionExpression>(std::move(cexpr));
        case punctuator_plus:
          return std::make_unique<PromotionExpression>(std::move(cexpr));
        case punctuator_minus:
          return std::make_unique<NegationExpression>(std::move(cexpr));
        case punctuator_tilde:
          return std::make_unique<BitwiseNotExpression>(std::move(cexpr));
        case punctuator_not:
          return std::make_unique<LogicalNotExpression>(std::move(cexpr));
        default: throw std::runtime_error(
          "Internal error: invalid unary operator"
        );
      }
    }
    else if(tok.get_punctuator() == punctuator_increment)
    {
      auto uexp = parse_unary_expression();
      if(!uexp) throw std::runtime_error("Expected unary expression after ++");
      auto rslt = std::make_unique<PrefixIncrementExpression>(std::move(uexp));
      parsed_buffer();
      return rslt;
    }
    else if(tok.get_punctuator() == punctuator_decrement)
    {
      auto uexp = parse_unary_expression();
      if(!uexp) throw std::runtime_error("Expected unary expression after --");
      auto rslt = std::make_unique<PrefixDecrementExpression>(std::move(uexp));
      parsed_buffer();
      return rslt;
    }
    else if(tok.is_keyword())
    {
      //TODO: deal with size and alignment expressions
      switch(tok.get_keyword())
      {
        case keyword_sizeof:
        case keyword_alignof:
        default: return nullptr;
      }
    }
    unget_token();
    return nullptr;
  }

}
