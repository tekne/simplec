#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/binary_expression/bitwise_expr.hpp>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_OR_expression() {

    auto rslt = parse_XOR_expression();
    if(!rslt) return nullptr;

    // Try to append as many operations as possible
    while(true)
    {
      auto tok = parse_token();
      if(tok.get_punctuator() == punctuator_pipe)
      {
        prev_unar = false;
        auto cexpr = parse_XOR_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected XOR expression after |"
        );
        rslt = std::make_unique<BitwiseOrExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      unget_token();
      return rslt;
    }
  }

}
