#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/unary_expression/cast_expression.hpp>
#include <exception>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_cast_expression()
  {
    auto uexpr = parse_unary_expression();
    if(uexpr) {
      prev_unar = true;
      return uexpr;
    }
    
    prev_unar = false;

    // Attempt to parse a cast
    QualifiedType type = parse_bracketed_type_name();
    if(type.is_invalid()) return nullptr;

    auto cexpr = parse_cast_expression();
    if(!cexpr) throw
      std::runtime_error("Expected cast expression after type name");

    return std::make_unique<CastExpression>(std::move(cexpr), type);
  }

}
