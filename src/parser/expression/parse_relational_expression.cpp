#include <simplec/parser/expression.hpp>
#include <simplec/parser/expression/binary_expression/logical_expr.hpp>

namespace simplec
{

  std::unique_ptr<Expression> ExpressionParser::parse_relational_expression() {

    auto rslt = parse_shift_expression();
    if(!rslt) return nullptr;

    // Try to append as many operations as possible
    while(true)
    {
      auto tok = parse_token();
      switch(tok.get_punctuator())
      {
      case punctuator_lt:
      {
        prev_unar = false;
        auto cexpr = parse_shift_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected shift expression after <"
        );
        rslt = std::make_unique<LessThanExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      case punctuator_gt:
      {
        prev_unar = false;
        auto cexpr = parse_shift_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected shift expression after >"
        );
        rslt = std::make_unique<GreaterThanExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      case punctuator_leq:
      {
        prev_unar = false;
        auto cexpr = parse_shift_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected shift expression after <="
        );
        rslt = std::make_unique<LEQExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      case punctuator_geq:
      {
        prev_unar = false;
        auto cexpr = parse_shift_expression();
        if(!cexpr) throw std::runtime_error(
          "Expected shift expression after >="
        );
        rslt = std::make_unique<GEQExpression>(
          std::move(rslt), std::move(cexpr), typeset
        );
        continue;
      }
      default: break;
      }
      unget_token();
      return rslt;
    }
  }

}
