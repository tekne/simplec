#include <simplec/parser/expression.hpp>
#include <cstdlib>

namespace simplec
{

  std::unique_ptr<Value> ExpressionParser::parse_value()
  {
    auto tok = parse_token();
    if(tok.get_raw_id() != lex_tok_literal) {
      unget_token();
      return nullptr;
    }

    auto m = tok.metadata();

    switch(tok.get_literal_type())
    {
      case integer_constant:
      {
        std::shared_ptr<const IntegerType> tt;
        uint64_t val = strtoull(tok.data().c_str(), nullptr, 0);
        switch(tok.get_int_constant_suffix())
        {
        case no_suffix:
        case int_suffix_l:
        case int_suffix_ll:
          tt = typeset.get_signed_fit(val);
          break;
        case int_suffix_u:
        case int_suffix_ul:
        case int_suffix_ull:
          tt = typeset.get_unsigned_fit(val);
          break;
        default: throw std::runtime_error(
            "Internal error: invalid integer suffix "
            + std::to_string(tok.get_int_constant_suffix())
            + " in parse_value"
          );
        }
        if(!tt) throw std::runtime_error(
          "Cannot find native integer type fitting constant " + tok.data()
        );

        // Promote integer type to appropriate type based off suffix
        switch(tok.get_int_constant_suffix())
        {
          case no_suffix:
            if(*tt < typeset.get_int()) tt = typeset.get_int_ptr();
            break;
          case int_suffix_l:
            if(*tt < typeset.get_long()) tt = typeset.get_long_ptr();
            break;
          case int_suffix_ul:
            if(*tt < typeset.get_unsigned_long())
              tt = typeset.get_unsigned_long_ptr();
            break;
          case int_suffix_ll:
            if(*tt < typeset.get_long_long())
              tt = typeset.get_long_long_ptr();
            break;
          case int_suffix_ull:
            if(*tt < typeset.get_unsigned_long_long())
              tt = typeset.get_unsigned_long_long_ptr();
            break;
          default: throw std::runtime_error(
            "Internal error: invalid integer suffix in parse_value promotion"
          );
        }

        parsed_buffer();
        if(tt->is_unsigned()) return std::make_unique<UnsignedIntegerValue>(
          val, tt, m
        );
        return std::make_unique<SignedIntegerValue>(val, tt, m);
      }
      case floating_constant:
        switch(tok.get_float_type())
        {
          case floating_type_single:
          {
            float flt = strtof(tok.data().c_str(), nullptr);
            parsed_buffer();
            return std::make_unique<FloatValue>(flt, typeset);
          }
          case floating_type_double:
          {
            double dbl = strtod(tok.data().c_str(), nullptr);
            parsed_buffer();
            return std::make_unique<DoubleValue>(dbl, typeset, m);
          }
          case floating_type_long_double:
          {
            long double ldbl = strtold(tok.data().c_str(), nullptr);
            parsed_buffer();
            return std::make_unique<LongDoubleValue>(ldbl, typeset, m);
          }
          default: throw std::runtime_error(
            "Internal error: invalid float type in parse_value"
          );
        }
      case character_constant:
      {
        std::unique_ptr<Value> rslt;
        switch(tok.get_char_prefix())
        {
          case no_char_prefix:
            rslt = std::make_unique<SignedIntegerValue>(
              get_multichar_constant(tok.data()),
              typeset.get_int_ptr(), m
            );
            break;
          case char_prefix_u:
            rslt = std::make_unique<UnsignedIntegerValue>(
              get_character_constant(tok.data()), typeset.get_char16_ptr(), m
            );
            break;
          case char_prefix_U:
            rslt = std::make_unique<UnsignedIntegerValue>(
              get_character_constant(tok.data()), typeset.get_char32_ptr(), m
            );
            break;
          case char_prefix_L:
            rslt = std::make_unique<UnsignedIntegerValue>(
              get_character_constant(tok.data()), typeset.get_wchar_ptr(), m
            );
            break;
          default: throw std::runtime_error(
            "Internal error: invalid character constant prefix in parse_value"
          );
        }
        parsed_buffer();
        return rslt;
      }
      case string_literal:
      {
        std::unique_ptr<Value> rslt;
        switch(tok.get_string_prefix())
        {
          case no_prefix: case prefix_u8:
            rslt = std::make_unique<StringValue>(
              tok.strip(), typeset, tok.get_string_prefix() == prefix_u8
            ); break;
          case prefix_u:
            rslt = std::make_unique<Char16StringValue>(tok.strip(), typeset, m);
            break;
          case prefix_U:
            rslt = std::make_unique<Char32StringValue>(tok.strip(), typeset, m);
            break;
          case prefix_L:
            rslt = std::make_unique<WideStringValue>(tok.strip(), typeset, m);
            break;
          default: throw std::runtime_error(
            "Internal error: invalid string literal prefix in parse_value"
          );
        }
        parsed_buffer();
        return rslt;
      }
      default: throw std::runtime_error(
        "Internal error: invalid literal type in parse_value"
      );
    }
  }

}
