#include <simplec/parser/token/constant.hpp>
#include <simplec/lexer/read_universal_character_name.hpp>
#include <simplec/lexer/read_escape_sequence.hpp>
#include <exception>
#include <cuchar>
#include <climits>

namespace simplec
{

  int get_multichar_constant(const std::string& tok)
  {
    // Sanity check
    if(tok.empty()) throw std::runtime_error(
      "Cannot parse empty character literal"
    );

    const char* ptr = tok.data();
    const char* eptr = ptr + tok.size();
    const char* temp;

    int value = 0;
    char32_t eval;
    char buffer[4];

    while(ptr != eptr)
    {
      // Attempt to read universal character sequences
      temp = read_universal_character_name(ptr, eptr, eval);
      if(temp != ptr)
      {
        size_t r = std::c32rtomb(buffer, eval, nullptr);
        if(r <= 4)
        {
          for(unsigned i = 0; i < r; i++)
          {
            value <<= CHAR_BIT;
            value |= buffer[r];
          }
        }
        ptr = temp;
      }
      // Attempt to read escape sequences
      else if(*ptr == '\\')
      {
        ptr++;
        if(ptr == eptr) throw std::runtime_error(
          "Unterminated escape sequence in character literal"
        );
        switch(*ptr)
        {
          case 'a':
            ptr++;
            value <<= CHAR_BIT;
            value |= '\a';
            break;
          case 'b':
            ptr++;
            value <<= CHAR_BIT;
            value |= '\b';
            break;
          case 'f':
            ptr++;
            value <<= CHAR_BIT;
            value |= '\f';
            break;
          case 'n':
            ptr++;
            value <<= CHAR_BIT;
            value |= '\n';
            break;
          case 'r':
            ptr++;
            value <<= CHAR_BIT;
            value |= '\r';
            break;
          case 't':
            ptr++;
            value <<= CHAR_BIT;
            value |= '\t';
            break;
          case 'v':
            ptr++;
            value <<= CHAR_BIT;
            value |= '\v';
            break;
          case '\\':
            ptr++;
            value <<= CHAR_BIT;
            value |= '\\';
            break;
          case '\'':
            ptr++;
            value <<= CHAR_BIT;
            value |= '\'';
            break;
          case '\"':
            ptr++;
            value <<= CHAR_BIT;
            value |= '\"';
            break;
          case '\?':
            ptr++;
            value <<= CHAR_BIT;
            value |= '\?';
            break;
          case 'x':
            throw std::runtime_error("Not yet implemented!");
          default:
            //TODO warn about invalid escape sequences
            size_t c = 0;
            int tv = 0;
            while(c < 3 && isdigit(*ptr) && (*ptr - '0' <= 7))
            {
              c++;
              tv <<= 3;
              tv |= (*ptr - '0');
              ptr++;
            }
            value <<= CHAR_BIT;
            value |= tv;
        }
      }
      // Just read a single character
      else
      {
        value <<= CHAR_BIT;
        value |= *ptr;
        ptr++;
      }

    }

    return value;

  }

}
