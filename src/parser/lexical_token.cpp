#include <simplec/parser/lexical_token.hpp>
#include <simplec/parser/token/constant.hpp>
#include <exception>
#include <cctype>
#include <cstdlib>

namespace simplec
{

  const char* parse_digit_sequence(const char* s)
  {
    while(isdigit(*s)) s++;
    return s;
  }

  const char* parse_hex_digit_sequence(const char* s)
  {
    while(isxdigit(*s)) s++;
    return s;
  }

  bool is_oct_digit(char c)
  {
    return isdigit(c) && c != '8' && c != '9';
  }

  const char* parse_oct_digit_sequence(const char* s)
  {
    while(is_oct_digit(*s)) s++;
    return s;
  }

  const char* parse_integer(const char* s)
  {
    const char* fail = s;
    const char* end;
    if(*s == '0')
    {
      s++;
      switch(*s)
      {
        case 'x': case 'X':
        {
          end = parse_hex_digit_sequence(++s);
          if(end == s) return fail;
          return end;
        }
        default: return parse_oct_digit_sequence(s);
      }
    }
    return parse_digit_sequence(s);
  }

  Token::Token(preprocessor_token tok): val{}
  {
    md = tok.metadata();

    switch(tok.type())
    {
      case tok_unknown: case tok_nl: case tok_err: case tok_sym:
        throw std::runtime_error(
          "Invalid token ``" + tok.string() +
          "`` (type : " + std::to_string(tok.type()) + ")"
        );

      case tok_eof:
        type = get_eof_type_id();
        return;

      case tok_id:
      {
        C_keyword key = str_to_C_keyword(tok.string());
        if(key == invalid_keyword) type = get_identifier_type_id();
        else {
          type = get_keyword_type_id(key);
          return;
        }
        val = tok.strip();
        return;
      }

      case tok_char_literal:
        if(tok.empty())
          throw std::runtime_error(
            "Internal error: cannot have empty character literal"
          );
        val = tok.strip();
        switch(val.front())
        {
          case 'u': type = get_character_constant_type_id(char_prefix_u);
          val.erase(val.begin(), val.begin() + 2);
          val.pop_back();
          break;
          case 'U': type = get_character_constant_type_id(char_prefix_U);
          val.erase(val.begin(), val.begin() + 2);
          val.pop_back();
          break;
          case 'L': type = get_character_constant_type_id(char_prefix_L);
          val.erase(val.begin(), val.begin() + 2);
          val.pop_back();
          break;
          case '\'':  type = get_character_constant_type_id(no_char_prefix);
          val.erase(val.begin());
          val.pop_back();
          break;
          default: throw std::runtime_error(
            "Internal error: invalid character literal " + tok.string()
          );
        }
        return;

      case tok_punc:
      {
        C_punctuator punc = str_to_C_punctuator(tok.string());
        if(punc == invalid_punctuator) throw std::runtime_error(
          "Could not resolve preprocessing symbol "
          + tok.string()
          + " to valid C punctuator"
        );
        type = get_punctuator_type_id(punc);
        return;
      }

      case tok_str:
      {
        if(tok.empty()) throw std::runtime_error(
          "Internal error: cannot have empty string literal"
        );

        val = tok.strip();
        const char* str = val.data();
        switch(*(str++))
        {
          case 'u':
            str++;
            if(*str == '8')
            {
              type = get_string_literal_type_id(prefix_u8);
              val.erase(val.begin(), val.begin() + 3);
              val.pop_back();
              return;
            }
            type = get_string_literal_type_id(prefix_u);
            val.erase(val.begin(), val.begin() + 2);
            val.pop_back();
            return;
          case 'U':
            type = get_string_literal_type_id(prefix_U);
            val.erase(val.begin(), val.begin() + 2);
            val.pop_back();
            return;
          case 'L':
            type = get_string_literal_type_id(prefix_L);
            val.erase(val.begin(), val.begin() + 2);
            val.pop_back();
            return;
          case '\"':
            type = get_string_literal_type_id(no_prefix);
            val.erase(val.begin());
            val.pop_back();
            return;
          default: throw std::runtime_error(
            "Internal error: invalid string literal " + val
          );
        }
      }

      case tok_num:
      {
        if(tok.empty())
          throw std::runtime_error(
            "Internal error: cannot have empty preprocessing number"
          );
        const char* str = tok.string().data();
        const char* end = parse_integer(str);
        if(end != str)
        {
          size_t read = end - str;
          int_literal_suffix sfx = get_int_suffix(end);
          if(sfx != invalid_suffix)
          {
            type = get_int_constant_type_id(sfx);

            val = tok.strip();
            val.resize(read);
            return;
          }
        }

        char* dend;
        strtod(str, &dend);
        if(dend != str)
        {
          if(!dend[0] || !dend[1])
          {
            switch(*dend)
            {
            case '\0':
              type = get_float_constant_type_id(floating_type_double);
              val = tok.strip();
              return;
            case 'f': case 'F':
              type = get_float_constant_type_id(floating_type_single);
              val = tok.strip();
              val.pop_back();
              return;
            case 'l': case 'L':
              type = get_float_constant_type_id(floating_type_long_double);
              val = tok.strip();
              val.pop_back();
              return;
            default: break;
            }
          }
          throw std::runtime_error(
            "Could not parse float from preprocessing number " + tok.string()
          );
        }

      }

    }

  }

}
