#include <simplec/parser/token/constant.hpp>

namespace simplec
{

  int_literal_suffix get_int_suffix(const char* ptr)
  {
    switch(*ptr)
    {
      case '\0':
        return no_suffix;
      case 'u': case 'U':
        ptr++;
        switch(*ptr)
        {
          case '\0': return int_suffix_u; // u
          case 'l': case 'L':
          ptr++;
          switch(*ptr)
          {
            case '\0': return int_suffix_ul; // ul
            case 'l': case 'L':
              ptr++;
              switch(*ptr)
              {
                case '\0': return int_suffix_ull; // ull
                default: return invalid_suffix; // ull?
              }
            default: return invalid_suffix; // ul?
          }
          default: return invalid_suffix; // u?
        }
      case 'l': case 'L':
        ptr++;
        switch(*ptr)
        {
          case '\0': return int_suffix_l; // l
          case 'l': case 'L':
            ptr++;
            switch(*ptr)
            {
              case '\0': return int_suffix_ll; // ll
              case 'u': case 'U':
                ptr++;
                switch(*ptr)
                {
                  case '\0': return int_suffix_ull; // llu
                  default: return invalid_suffix; // llu?
                }
              default: return invalid_suffix; // ll?
            }
          case 'u': case 'U':
            ptr++;
            switch(*ptr)
            {
              case '\0': return int_suffix_ul; // lu
              default: return invalid_suffix; // lu?
            }
          default: return invalid_suffix; // l?
        }
      default: return invalid_suffix; // ?
    }
  }

}
