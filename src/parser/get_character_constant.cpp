#include <simplec/parser/token/constant.hpp>
#include <simplec/lexer/read_universal_character_name.hpp>

namespace simplec
{

  char32_t get_character_constant(const std::string& tok)
  {
    // Sanity check
    if(tok.empty()) throw std::runtime_error(
      "Cannot parse empty character literal"
    );

    const char* ptr = tok.data();
    const char* eptr = ptr + tok.size();
    char32_t value = 0;

    // Attempt to read universal character sequences
    auto temp = read_universal_character_name(ptr, eptr, value);
    if(temp != ptr)
    {
      return value;
    }
    // Attempt to read escape sequences
    else if(*ptr == '\\')
    {
      ptr++;
      if(ptr == eptr) throw std::runtime_error(
        "Unterminated escape sequence in character literal"
      );
      switch(*ptr)
      {
        case 'a':
          value = '\a'; break;
        case 'b':
          value = '\b'; break;
        case 'f':
          value = '\f'; break;
        case 'n':
          value = '\n'; break;
        case 'r':
          value = '\r'; break;
        case 't':
          value = '\t'; break;
        case 'v':
          value = '\v'; break;
        case '\\':
          value = '\\'; break;
        case '\'':
          value = '\''; break;
        case '\"':
          value = '\"'; break;
        case '\?':
          value = '\?'; break;
        case 'x':
          throw std::runtime_error("Not yet implemented!");
        default:
          //TODO warn about invalid escape sequences
          size_t c = 0;
          value = 0;
          while(c < 3 && isdigit(*ptr) && (*ptr - '0' <= 7))
          {
            value *= 8;
            value += *ptr - '0';
            ptr++;
            c++;
          }
      }
    }
    // Just read a single character
    else
    {
      value = *ptr;
    }
    return value;
  }

}
