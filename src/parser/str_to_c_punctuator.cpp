#include <simplec/parser/token/punctuator.hpp>
#include <string>

namespace simplec
{

  C_punctuator str_to_C_punctuator(const std::string& str)
  {
    switch(str.size())
    {
      case 0: return invalid_punctuator;
      case 1:
        switch(str.front())
        {
          case '[': return punctuator_lsquare;
          case ']': return punctuator_rsquare;
          case '(': return punctuator_lparen;
          case ')': return punctuator_rparen;
          case '{': return punctuator_lcurly;
          case '}': return punctuator_rcurly;
          case '.': return punctuator_period;
          case '&': return punctuator_ampersand;
          case '*': return punctuator_star;
          case '+': return punctuator_plus;
          case '-': return punctuator_minus;
          case '~': return punctuator_tilde;
          case '!': return punctuator_not;
          case '/': return punctuator_divide;
          case '%': return punctuator_modulo;
          case '<': return punctuator_lt;
          case '>': return punctuator_gt;
          case '^': return punctuator_xor;
          case '|': return punctuator_pipe;
          case '?': return punctuator_ternary;
          case ':': return punctuator_colon;
          case ';': return punctuator_semicolon;
          case '=': return punctuator_assign;
          case ',': return punctuator_comma;
          case '#': return preprocessor_hash;
          default: return invalid_punctuator;
        }
      case 2:
        switch(str.front())
        {
          case '-':
            switch(str.back())
            {
              case '>': return punctuator_arrow;
              case '-': return punctuator_decrement;
              case '=': return punctuator_mineq;
              default: return invalid_punctuator;
            }
          case '+':
            switch(str.back())
            {
              case '+': return punctuator_increment;
              case '=': return punctuator_pluseq;
              default: return invalid_punctuator;
            }
          case '<':
            switch(str.back())
            {
              case '<': return punctuator_lshift;
              case '=': return punctuator_leq;
              // Digraph handling
              case ':': return punctuator_lsquare;
              case '%': return punctuator_lcurly;
              default: return invalid_punctuator;
            }
          case '>':
            switch(str.back())
            {
              case '>': return punctuator_rshift;
              case '=': return punctuator_geq;
              default: return invalid_punctuator;
            }
          case '=': if(str.back() == '=') return punctuator_eq;
            return invalid_punctuator;
          case '!': if(str.back() == '=') return punctuator_neq;
            return invalid_punctuator;
          case '&':
            switch(str.back())
            {
              case '&': return punctuator_and;
              case '=': return punctuator_andeq;
              default: return invalid_punctuator;
            }
          case '|':
            switch(str.back())
            {
              case '|': return punctuator_or;
              case '=': return punctuator_oreq;
              default: return invalid_punctuator;
            }
          case '*':
            if(str.back() == '=')
              return punctuator_timeseq;
            return invalid_punctuator;
          case '/':
            if(str.back() == '=')
              return punctuator_diveq;
            return invalid_punctuator;
          case '%':
            switch(str.back())
            {
              case '=': return punctuator_modeq;
              // Digraph handling
              case '>': return punctuator_rcurly;
              case ':': return preprocessor_hash;
            }
          case '^':
            if(str.back() == '=')
              return punctuator_xoreq;
            return invalid_punctuator;
          case '#':
            if(str.back() == '#')
              return preprocessor_concat;
            return invalid_punctuator;
          // Digraph handling
          case ':':
            if(str.back() == '>')
              return punctuator_rsquare;
            return invalid_punctuator;
          default: return invalid_punctuator;
        }
      case 3:
        switch(str.front())
        {
          case '.': if("..." == str) return punctuator_ellipsis;
            break;
          case '<': if("<<=" == str) return punctuator_lshifteq;
            break;
          case '>': if(">>=" == str) return punctuator_rshifteq;
            break;
        }
        return invalid_punctuator;
      case 4: if("%:%:" == str) return preprocessor_concat;
      default: return invalid_punctuator;
    }
  }

}
