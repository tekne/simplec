#include <simplec/parser/types/fundamental_type_set.hpp>

namespace simplec
{

  #define SIMPLEC_MAP_TYPE_TO_POINTER(X) \
    if(t == get_ ## X ## _ptr()) return get_ptr_to_ ## X ## _ptr();

  #define SIMPLEC_MAP_TYPE_TO_CONST_POINTER(X) \
    if(t == get_ ## X ## _ptr()) return get_ptr_to_const_ ## X ## _ptr();

  #define SIMPLEC_MAP_PTR_TO_CONST_POINTER(X) \
    if(t == get_ptr_to_ ## X ## _ptr()) return get_ptr_to_const_ ## X ## _ptr();

  #define SIMPLEC_MAP_CONST_PTR_TO_POINTER(X) \
    if(t == get_ptr_to_const_ ## X ## _ptr()) return get_ptr_to_ ## X ## _ptr();

  std::shared_ptr<const PointerType> FundamentalTypeSet::resolve_pointer(
    QualifiedType qt
  ) const
  {
    if(!qt.is_qualified())
    {
      auto t = qt.raw_type();
      SIMPLEC_MAP_TYPE_TO_POINTER(bool);
      SIMPLEC_MAP_TYPE_TO_POINTER(char);
      SIMPLEC_MAP_TYPE_TO_POINTER(signed_char);
      SIMPLEC_MAP_TYPE_TO_POINTER(short);
      SIMPLEC_MAP_TYPE_TO_POINTER(int);
      SIMPLEC_MAP_TYPE_TO_POINTER(long);
      SIMPLEC_MAP_TYPE_TO_POINTER(long_long);
      SIMPLEC_MAP_TYPE_TO_POINTER(unsigned_char);
      SIMPLEC_MAP_TYPE_TO_POINTER(unsigned_short);
      SIMPLEC_MAP_TYPE_TO_POINTER(unsigned_int);
      SIMPLEC_MAP_TYPE_TO_POINTER(unsigned_long);
      SIMPLEC_MAP_TYPE_TO_POINTER(unsigned_long_long);
      SIMPLEC_MAP_TYPE_TO_POINTER(float);
      SIMPLEC_MAP_TYPE_TO_POINTER(double);
      SIMPLEC_MAP_TYPE_TO_POINTER(long_double);
      SIMPLEC_MAP_TYPE_TO_POINTER(imag_float);
      SIMPLEC_MAP_TYPE_TO_POINTER(imag_double);
      SIMPLEC_MAP_TYPE_TO_POINTER(imag_long_double);
      SIMPLEC_MAP_TYPE_TO_POINTER(complex_float);
      SIMPLEC_MAP_TYPE_TO_POINTER(complex_double);
      SIMPLEC_MAP_TYPE_TO_POINTER(complex_long_double);
      SIMPLEC_MAP_TYPE_TO_POINTER(wchar);
      SIMPLEC_MAP_TYPE_TO_POINTER(char16);
      SIMPLEC_MAP_TYPE_TO_POINTER(char32);
    }
    else if(qt.is_const() && !qt.is_volatile() && !qt.is_restrict())
    {
      return resolve_const_pointer(qt);
    }
    return std::make_shared<const PointerType>(qt);
  }

  std::shared_ptr<const PointerType> FundamentalTypeSet::resolve_const_pointer(
    QualifiedType qt
  ) const
  {
    if(qt.is_const() && !qt.is_volatile() && !qt.is_restrict())
    {
      auto t = qt.raw_type();
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(bool);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(char);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(signed_char);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(short);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(int);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(long);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(long_long);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(unsigned_char);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(unsigned_short);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(unsigned_int);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(unsigned_long);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(unsigned_long_long);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(float);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(double);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(long_double);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(imag_float);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(imag_double);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(imag_long_double);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(complex_float);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(complex_double);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(complex_long_double);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(wchar);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(char16);
      SIMPLEC_MAP_TYPE_TO_CONST_POINTER(char32);
    }
    qt.set_qual(qt.get_qual() | const_qualified);
    return std::make_shared<const PointerType>(qt);
  }

}
