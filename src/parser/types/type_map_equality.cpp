#include <simplec/parser/types/type_id_map.hpp>

namespace simplec
{

  bool TypeIDMap::compatible_map(const TypeIDMap& M) const {
    if(identifiers != M.identifiers) return false;
    for(size_t i = 0; i < members.size(); i++)
    {
      if(!members[i].compatible_type(M.members[i])) return false;
    }
    //NOTE: anonymous structs are determined by identifiers and members, so
    // don't need to be checked.
    return true;
  }

  bool TypeIDMap::equal_map(const TypeIDMap& M) const {
    if(identifiers != M.identifiers) return false;
    for(size_t i = 0; i < members.size(); i++)
    {
      if(members[i] != M.members[i]) return false;
    }
    //NOTE: anonymous structs are determined by identifiers and members, so
    // don't need to be checked.
    return true;
  }

}
