#include <simplec/parser/types/type_id_map.hpp>

namespace simplec
{

  const TypeIDMap& TypeIDMap::map_to_stream(std::ostream& str) const
  {
    if(members.empty()) {str << "{}"; return *this;}

    str << "{";
    auto ids = identifiers.begin();
    for(size_t i = 0; i < members.size(); i++)
    {
      members[i].print_to_stream(str);
      size_t mb = mapback[i];
      if(mb != -1)
      {
        str << " " << ids[mb].first;
      }
      else
      {
        str << " ANON!";
      }
      str << ";";
      if(i != members.size() - 1) str << " ";
    }
    str << "}";
    return *this;
  }

}
