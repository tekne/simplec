#include <simplec/parser/types/fundamental_type_set.hpp>

namespace simplec
{

  std::shared_ptr<const IntegerType>
  FundamentalTypeSet::get_signed_fit(uint64_t val) const
  {
    if(val <= 1) return get_bool_ptr();
    if(val < (1 << get_char().get_bits())/2) return get_char_ptr();
    if(val < (1 << get_char().get_bits())) return get_unsigned_char_ptr();
    if(val < (1 << get_short().get_bits())/2) return get_short_ptr();
    if(val < (1 << get_short().get_bits())) return get_unsigned_short_ptr();
    if(val < (1 << get_int().get_bits())/2) return get_int_ptr();
    if(val < (1 << get_int().get_bits())) return get_unsigned_int_ptr();
    if(val < (1 << get_long().get_bits())/2) return get_long_ptr();
    if(val < (1 << get_long().get_bits())) return get_unsigned_long_ptr();
    if(val < (1 << get_long_long().get_bits())/2) return get_long_long_ptr();
    if(val < (1 << get_long_long().get_bits()))
      return get_unsigned_long_long_ptr();
    return nullptr;
  }

  std::shared_ptr<const IntegerType>
  FundamentalTypeSet::get_unsigned_fit(uint64_t val) const
  {
    if(val <= 1) return get_bool_ptr();
    if(val < (1 << get_char().get_bits())) return get_unsigned_char_ptr();
    if(val < (1 << get_short().get_bits())) return get_unsigned_short_ptr();
    if(val < (1 << get_int().get_bits())) return get_unsigned_int_ptr();
    if(val < (1 << get_long().get_bits())) return get_unsigned_long_ptr();
    if(val < (1 << get_long_long().get_bits()))
      return get_unsigned_long_long_ptr();
    return nullptr;
  }

}
