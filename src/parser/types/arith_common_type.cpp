#include <simplec/parser/types/fundamental_type_set.hpp>
#include <simplec/parser/types/pointer_type.hpp>
#include <memory>

namespace simplec
{

  std::shared_ptr<const IntegerType> FundamentalTypeSet::max_int_type(
    std::shared_ptr<const IntegerType> i1,
    std::shared_ptr<const IntegerType> i2
  ) const {
    if(!i1 || !i2) return nullptr;
    if(*i1 < *i2)
    {
      if(i1->is_unsigned() || i2->is_signed()) return i2;
      if(i2->is_named()) // Try to avoid memory allocation
      {
        auto p = resolve_signed(
          std::dynamic_pointer_cast<const NamedIntegerType>(i2)
        );
        if(p) return p;
      }
      return i2->to_signed_ptr();
    }
    else
    {
      if(i2->is_unsigned() || i1->is_signed()) return i1;
      if(i1->is_named()) // Try to avoid memory allocation
      {
        auto p = resolve_signed(
          std::dynamic_pointer_cast<const NamedIntegerType>(i1)
        );
        if(p) return p;
      }
      return i1->to_signed_ptr();
    }
  }

  std::shared_ptr<const FloatingType> FundamentalTypeSet::max_flt_type(
    std::shared_ptr<const FloatingType> f1,
    std::shared_ptr<const FloatingType> f2
  ) const {
    if(!f1 || !f2) return nullptr;

    std::shared_ptr<const FloatingType> biggest =
      (f1->get_bits() > f2->get_bits()) ? f1 : f2;

    // If both types are real or imaginary, return the bigger one
    if(
      (f1->is_real() && f2->is_real()) ||
      (f1->is_imaginary() && f2->is_imaginary())
    )
    {
      return biggest;
    }

    // Otherwise, return a complexified version of the biggest one
    return resolve_complex(biggest);
  }

  std::shared_ptr<const ArithmeticType> FundamentalTypeSet::arith_common_type(
    std::shared_ptr<const ArithmeticType> a1,
    std::shared_ptr<const ArithmeticType> a2
  ) const {
    if(!a1 || !a2) return nullptr;

    auto f1 = std::dynamic_pointer_cast<const FloatingType>(a1);
    auto f2 = std::dynamic_pointer_cast<const FloatingType>(a2);
    if(f1 && f2) return max_flt_type(f1, f2);
    else if(f1) return f1;
    else if(f2) return f2;

    auto i1 = std::dynamic_pointer_cast<const IntegerType>(a1);
    auto i2 = std::dynamic_pointer_cast<const IntegerType>(a2);
    return max_int_type(i1, i2);
  }

  std::shared_ptr<const VectorType> FundamentalTypeSet::arith_common_type(
    std::shared_ptr<const VectorType> v1,
    std::shared_ptr<const VectorType> v2
  ) {
    if(!v1 || !v2) return nullptr;
    else if(*v1 != *v2) return nullptr;

    return v1;
  }

  std::shared_ptr<const ScalarType> FundamentalTypeSet::scalar_common_type(
    std::shared_ptr<const ScalarType> s1,
    std::shared_ptr<const ScalarType> s2
  ) const {
    auto v1 = std::dynamic_pointer_cast<const VectorType>(s1);
    auto v2 = std::dynamic_pointer_cast<const VectorType>(s2);
    if(v1 && v2) return arith_common_type(v1, v2);
    else if(v1 || v2) return nullptr;

    auto p1 = std::dynamic_pointer_cast<const PointerType>(s1);
    auto p2 = std::dynamic_pointer_cast<const PointerType>(s2);
    if(p1 && p2) return nullptr;
    else if(p1)
    {
      if(dynamic_cast<const IntegerType*>(s2.get())) return p1;
      return nullptr;
    }
    else if(p2)
    {
      if(dynamic_cast<const IntegerType*>(s1.get())) return p2;
      return nullptr;
    }

    auto a1 = std::dynamic_pointer_cast<const ArithmeticType>(s1);
    auto a2 = std::dynamic_pointer_cast<const ArithmeticType>(s2);
    if(a1 && a2) return arith_common_type(a1, a2);
    return nullptr;
  }

}
