#include <simplec/parser/types/fundamental_type_set.hpp>

namespace simplec
{

  FundamentalTypeSet& FundamentalTypeSet::set_wide_chars(uint8_t wcb)
  {
    if(wcb > 32) throw std::runtime_error(
      "wchar_t cannot be more than 32 bits"
    );

    if(unsigned_short_type.get_bits() >= wcb)
    {
      wchar_t_type = false_share(unsigned_short_type);
    }
    else if(unsigned_int_type.get_bits() >= wcb)
    {
      wchar_t_type = false_share(unsigned_int_type);
    }
    else if(unsigned_long_type.get_bits() >= wcb)
    {
      wchar_t_type = false_share(unsigned_long_type);
    }
    else if(unsigned_long_long_type.get_bits() >= wcb)
    {
      wchar_t_type = false_share(unsigned_long_long_type);
    }
    else throw std::runtime_error("No type large enough to fit wchar_t");

    if(unsigned_short_type.get_bits() == 16)
    {
      char16_t_type = false_share(unsigned_short_type);
    }
    else if(unsigned_int_type.get_bits() == 16)
    {
      char16_t_type = false_share(unsigned_int_type);
    }
    else throw std::runtime_error("No type large enough to fit char16_t");

    if(unsigned_short_type.get_bits() == 32)
    {
      char32_t_type = false_share(unsigned_short_type);
    }
    else if(unsigned_int_type.get_bits() == 32)
    {
      char32_t_type = false_share(unsigned_int_type);
    }
    else if(unsigned_long_type.get_bits() == 32)
    {
      char32_t_type = false_share(unsigned_long_type);
    }
    else if(unsigned_long_long_type.get_bits() == 32)
    {
      char32_t_type = false_share(unsigned_long_long_type);
    }
    else throw std::runtime_error("No type large enough to fit char32_t");

    ptr_to_wchar.set_pointed_type(wchar_t_type);
    ptr_to_char16.set_pointed_type(char16_t_type);
    ptr_to_char32.set_pointed_type(char32_t_type);
    ptr_to_const_wchar.set_pointed_type({wchar_t_type, const_qualified});
    ptr_to_const_char16.set_pointed_type({char16_t_type, const_qualified});
    ptr_to_const_char32.set_pointed_type({char32_t_type, const_qualified});

    return *this;
  }

}
