#include <simplec/parser/types/type_id_map.hpp>
#include <exception>
#include <algorithm>
#include <vector>

#include <boost/iterator/filter_iterator.hpp>
#include <boost/iterator/counting_iterator.hpp>
#include <boost/iterator/zip_iterator.hpp>
#include <boost/iterator/transform_iterator.hpp>

namespace simplec
{

  TypeIDMap& TypeIDMap::define_map(
    boost::container::small_vector<
    std::pair<std::string, QualifiedType>, sv_size
    > def)
  {

    members.clear();
    identifiers.clear();
    anonymous.clear();
    mapback.clear();

    members.reserve(def.size());
    identifiers.reserve(def.size());

    // Move member types out of argument
    for(size_t i = 0; i < def.size(); i++)
    {
      members.push_back(std::move(def[i].second));
      if(def[i].first.empty()) anonymous.push_back(i);
    }

    auto move_first = [](std::pair<std::string, QualifiedType>& i){
      return i.first;
    };
    auto moved_members_begin = boost::iterators::make_transform_iterator(
      def.begin(), move_first
    );
    auto moved_members_end = boost::iterators::make_transform_iterator(
      def.end(), move_first
    );
    auto counted_moved_members_begin = boost::iterators::make_zip_iterator(
      boost::make_tuple(
        moved_members_begin,
        boost::iterators::make_counting_iterator(static_cast<size_t>(0))
      )
    );
    auto counted_moved_members_end = boost::iterators::make_zip_iterator(
      boost::make_tuple(
        moved_members_end,
        boost::iterators::make_counting_iterator(def.size())
      )
    );

    auto first_not_empty = [](const boost::tuple<std::string, size_t>& x){
      return !x.get<0>().empty();
    };

    auto filtered_members_begin = boost::iterators::make_filter_iterator(
      first_not_empty,
      counted_moved_members_begin,
      counted_moved_members_end
    );
    auto filtered_members_end = boost::iterators::make_filter_iterator(
      first_not_empty,
      counted_moved_members_end,
      counted_moved_members_end
    );

    auto cast_to_pair = [](decltype(*filtered_members_begin) x){
      return std::make_pair(std::move(x.get<0>()), x.get<1>());
    };

    auto paired_members_begin = boost::iterators::make_transform_iterator(
      filtered_members_begin,
      cast_to_pair
    );

    auto paired_members_end = boost::iterators::make_transform_iterator(
      filtered_members_end,
      cast_to_pair
    );

    identifiers = boost::container::flat_map<std::string, size_t>{
      paired_members_begin, paired_members_end
    };

    auto ids = identifiers.begin();
    mapback.resize(members.size());

    for(size_t i = 0; i < members.size(); i++)
    {
      // -1 indicates a position has no mapback to an identifier
      mapback[i] = -1;
    }
    for(size_t i = 0; i < identifiers.size(); i++)
    {
      mapback[ids[i].second] = i;
    }

    return *this;
  }

}
