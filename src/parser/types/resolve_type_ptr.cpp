#include <simplec/parser/types/fundamental_type_set.hpp>
#include <memory>

namespace simplec
{

  std::shared_ptr<const NamedIntegerType> FundamentalTypeSet::resolve_signed(
    std::shared_ptr<const NamedIntegerType> i
  ) const {
    if(!i) return nullptr;
    if(i->is_signed()) return i;
    if(i->get_bits() > get_int().get_bits())
    {
      if(i == get_unsigned_long_ptr()) return get_long_ptr();
      if(i == get_unsigned_long_long_ptr()) return get_long_long_ptr();
    }
    else
    {
      if(i == get_unsigned_char_ptr()) return get_char_ptr();
      // int first, since it's more common than short
      if(i == get_unsigned_int_ptr()) return get_int_ptr();
      if(i == get_unsigned_short_ptr()) return get_short_ptr();
    }
    return
      std::dynamic_pointer_cast<const NamedIntegerType>(i->to_signed_ptr());
  }

  std::shared_ptr<const NamedIntegerType> FundamentalTypeSet::resolve_unsigned(
    std::shared_ptr<const NamedIntegerType> i
  ) const {
    if(!i) return nullptr;
    if(i->is_unsigned()) return i;
    if(i->get_bits() > get_int().get_bits())
    {
      if(i == get_long_ptr()) return get_unsigned_long_ptr();
      if(i == get_long_long_ptr()) return get_unsigned_long_long_ptr();
    }
    else
    {
      if(i == get_char_ptr() || i == get_signed_char_ptr())
        return get_unsigned_char_ptr();
      // int first, since it's more common than short
      if(i == get_int_ptr()) return get_unsigned_int_ptr();
      if(i == get_short_ptr()) return get_unsigned_short_ptr();
    }
    return
      std::dynamic_pointer_cast<const NamedIntegerType>(i->to_unsigned_ptr());
  }

  std::shared_ptr<const FloatingType> FundamentalTypeSet::resolve_real(
    std::shared_ptr<const FloatingType> ft
  ) const {
    if(ft->is_real()) return ft;
    if(ft == get_imag_float_ptr() || ft == get_complex_float_ptr()) return
      get_float_ptr();
    if(ft == get_imag_double_ptr() || ft == get_complex_double_ptr()) return
      get_double_ptr();
    if(ft == get_imag_long_double_ptr() || ft == get_complex_long_double_ptr()) return
      get_long_double_ptr();
    return std::make_shared<const FloatingType>(ft->to_real());
  }

  std::shared_ptr<const FloatingType> FundamentalTypeSet::resolve_imaginary(
    std::shared_ptr<const FloatingType> ft
  ) const {
    if(ft->is_imaginary()) return ft;
    if(ft == get_float_ptr() || ft == get_complex_float_ptr()) return
      get_imag_float_ptr();
    if(ft == get_double_ptr() || ft == get_complex_double_ptr()) return
      get_imag_double_ptr();
    if(ft == get_long_double_ptr() || ft == get_complex_long_double_ptr()) return
      get_imag_long_double_ptr();
    return std::make_shared<const FloatingType>(ft->to_imaginary());
  }

  std::shared_ptr<const FloatingType> FundamentalTypeSet::resolve_complex(
    std::shared_ptr<const FloatingType> ft
  ) const {
    if(ft->is_complex() && !ft->is_imaginary()) return ft;
    if(ft == get_float_ptr() || ft == get_imag_float_ptr()) return
      get_complex_float_ptr();
    if(ft == get_double_ptr() || ft == get_imag_double_ptr()) return
      get_complex_double_ptr();
    if(ft == get_long_double_ptr() || ft == get_imag_long_double_ptr()) return
      get_complex_long_double_ptr();
    return std::make_shared<const FloatingType>(ft->to_complex());
  }
}
