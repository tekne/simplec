#include <simplec/preprocessor/utils.hpp>
#include <simplec/lexer/read_token.hpp>

#include <vector>
#include <string>

namespace simplec
{

  std::vector<std::string> get_raw_tokens(const char* str)
  {
    return get_raw_tokens(str, str + strlen(str));
  }

  std::vector<std::string> get_raw_tokens(const std::string& str)
  {
    return get_raw_tokens(str.begin(), str.end());
  }

}
