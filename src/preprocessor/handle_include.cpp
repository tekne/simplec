#include <simplec/preprocessor/token.hpp>
#include <simplec/preprocessor.hpp>

#include <string>
#include <exception>

namespace simplec
{

  Preprocessor& Preprocessor::handle_include()
  {
    // Skip whitespace, but not newlines
    lexer.skip_ws();

    // Attempt to extract an hchar literal (<path/to/file>)
    lexer.get_next_hchar_literal();
    if(lexer.token_read()) { // hchar literal successfully extracted
      open_global_file(std::string(
        lexer.begin_current() + 1,
        lexer.end_current() - 1)
      );
    }
    else { // Attempt to extract a token
      auto tok = get_token();
      if(tok.type() != tok_str) throw std::runtime_error(
        "Invalid path " + tok.string() + " in #include directive"
      );
      open_local_file(std::string(tok.string().begin() + 1, tok.string().end() - 1));
    }

    if(get_token() != "\n") throw std::runtime_error(
      "Expected newline after #include"
    );

    return *this;
  }

}
