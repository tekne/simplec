#include <vector>
#include <string>

#include <simplec/preprocessor.hpp>
#include <simplec/preprocessor/token.hpp>

namespace simplec {

  std::vector<preprocessor_token> Preprocessor::get_token_line(
    const std::string& delim,
    bool strict
  )
  {
    std::vector<preprocessor_token> tokens;
    preprocessor_token tok;

    do {
      tok = get_token();
      if(strict && tok.empty()) throw std::runtime_error(
        "End of file before delimiter!"
      );
      else if(tok != "\n") tokens.push_back(std::move(tok));
    } while(tokens.empty() || tokens.back() != delim);

    return tokens;
  }

}
