#include <simplec/preprocessor/token.hpp>
#include <simplec/preprocessor.hpp>

#include <string>
#include <exception>
namespace simplec
{

  Preprocessor& Preprocessor::handle_directive()
  {
    preprocessor_token tok = get_raw_token();

    in_preprocessor = true;
    if(tok == "include") handle_include();
    else if(tok == "define") handle_define();
    else if(tok == "if") handle_if();
    else if(tok == "elif") handle_elif();
    else if(tok == "else") handle_else();
    else if(tok == "endif") handle_endif();
    else if(tok == "pragma") handle_pragma();
    else if(tok == "ifdef") handle_ifdef();
    else if(tok == "ifndef") handle_ifndef();
    else if(tok == "line") handle_line();
    else if(tok == "warning") handle_warning();
    else if(tok == "error") handle_error();
    else if(tok == "undef") handle_undef();
    else throw std::runtime_error(
      "Invalid preprocessor directive " + tok.string()
    );
    in_preprocessor = false;

    return *this;
  }

}
