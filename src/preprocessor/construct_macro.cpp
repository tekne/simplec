#include <simplec/preprocessor.hpp>

namespace simplec {

  macro::macro(
    std::vector<std::string> tk,
    std::vector<std::string> args,
    bool varargs,
    size_t def,
    const preprocessor_file_source* src
  ): tokens(tk), nargs(args.size()), type(varargs?variadic:function_like),
  defined_at(def), source(src)
  {

    for(auto& s: tokens) {
      if(varargs) {if(s == "__VA_ARGS__") s = write_argnum(nargs + 1);}
      else
      {
        for(size_t i = 0; i < args.size(); i++)
        {
          if(s == args[i]) {
            s = write_argnum(i);
            break;
          }
        }
      }
    }
  }

}
