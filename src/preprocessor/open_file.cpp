#include <utility>

#include <simplec/preprocessor/file_handling.hpp>
#include <simplec/preprocessor.hpp>

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem/fstream.hpp>

namespace simplec
{

  Preprocessor& Preprocessor::open_path(
    preprocessor_file_source pth
  )
  {
    file_stack.push_back(pth.open());
    lexer.open(file_stack.back());
    macro_state.enter_file(pth);
    return *this;
  }

  Preprocessor& Preprocessor::open_local_file(const std::string& file)
  {
    auto current_file = macro_state.get_file_ptr();
    if(current_file)
    {
      return open_path(
        paths.get_local(file, *current_file).set_line(macro_state.line)
      );
    }
    else
    {
      return open_path(
        paths.get_local(file).set_line(macro_state.line)
      );
    }
  }

  Preprocessor& Preprocessor::open_global_file(const std::string& file)
  {
    auto current_file = macro_state.get_file_ptr();
    if(current_file)
    {
      return open_path(
        paths.get_local(file, *current_file).set_line(macro_state.line)
      );
    }
    else
    {
      return open_path(
        paths.get_local(file).set_line(macro_state.line)
      );
    }
  }

}
