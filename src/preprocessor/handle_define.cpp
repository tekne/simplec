#include <simplec/preprocessor/token.hpp>
#include <simplec/preprocessor/ttype.hpp>
#include <simplec/preprocessor.hpp>

#include <string>
#include <exception>

namespace simplec
{

  Preprocessor& Preprocessor::handle_undef()
  {
    lexer.skip_ws();
    lexer.get_next_identifier();
    std::string tok = lexer.get_current_token();
    if(tok.empty()) throw std::runtime_error(
      "Invalid macro " + tok
    );
    macro_state.undef(tok);
    lexer.skip_ws();
    if(
      lexer.end_current() != lexer.end_buffer()
      && !isspace(*lexer.end_current())
    ) throw std::runtime_error(
      "Extraneous token after #undef"
    );
    skip_line();
    return *this;
  }

  Preprocessor& Preprocessor::handle_define()
  {
    preprocessor_token tok = get_raw_token();
    if(tok.empty()) throw
      std::runtime_error("End of file before definition!");
    if(tok == "\n") throw
      std::runtime_error("End of line before definition!");

    if(!is_identifier(tok)) throw std::runtime_error(
      "Cannot define invalid name " + tok.string() + " as macro"
    );

    preprocessor_token next_tok = get_token();

    // Define empty macro
    if((next_tok == "\n") || next_tok.empty())
    {
      macro_state.set_macro(
        tok.string(),
        macro(
          std::vector<std::string>(),
          0,
          macro_type::token_string,
          tok.get_line(),
          tok.get_src()
        )
      );
      return *this;
    }

    std::vector<std::string> tokens;

    // Define function style macro
    if(next_tok == "(")
    {
      std::vector<std::string> args;
      bool varargs = false;
      next_tok = get_raw_token();
      if((next_tok == "") || (next_tok == "\n"))
      {
        throw std::runtime_error("Runaway argument list!");
      }
      else if(is_identifier(next_tok))
      {
        args.push_back(std::move(next_tok.string()));
        while((next_tok = get_raw_token()) != "\n")
        {
          if(next_tok != ",")
            throw std::runtime_error(
              "Cannot have two arguments without separating comma"
            );
          next_tok = get_raw_token();
          if(!is_identifier(next_tok))
          {
            if(next_tok == "...")
            {
              varargs = true;
              next_tok = get_raw_token();
              if(next_tok != ")")
              {
                throw
                  std::runtime_error("Argument list must close after ellipsis");
              }
              break;
            }
            else
            {
              throw
                std::runtime_error("Invalid macro argument " + next_tok.string());
            }
          }
          args.push_back(std::move(next_tok.string()));
        }
      }
      else if(next_tok != ")")
      {
        throw std::runtime_error("Invalid macro argument " + next_tok.string());
      }

      next_tok = get_token();
      if((next_tok != "\n") && !next_tok.empty())
      {
        tokens.push_back(std::move(next_tok.string()));
      }
      while(((next_tok = get_token()) != "\n") && !next_tok.empty())
      {
        tokens.push_back(std::move(next_tok.string()));
      }

      macro_state.set_macro(
        tok.string(), macro(args, tokens, varargs, tok.get_line(), tok.get_src())
      );

      return *this;
    }

    // Define regular, token-string macro
    tokens.push_back(std::move(next_tok.string()));
    while(((next_tok = get_token()) != "\n") && !next_tok.empty())
    {
      tokens.push_back(std::move(next_tok.string()));
    }

    macro_state.set_macro(
      tok.string(),
      macro(
        tokens,
        0,
        token_string,
        tok.get_line(),
        tok.get_src()
      )
    );

    return *this;
  }

}
