#include <simplec/preprocessor/token.hpp>
#include <simplec/preprocessor.hpp>

#include <string>
#include <exception>

namespace simplec
{

  Preprocessor& Preprocessor::handle_pragma()
  {
    throw std::runtime_error("Pragma handling has not yet been implemented!");
  }

}
