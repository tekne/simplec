#include <simplec/preprocessor/token.hpp>
#include <simplec/preprocessor/ttype.hpp>
#include <simplec/preprocessor.hpp>

#include <string>
#include <exception>
#include <algorithm>

namespace simplec
{

    long long preprocessor_tok_to_num(
      const std::string& tok
    )
    {
      if(isdigit(tok[0]))
      {
        size_t pos;
        long long temp = stoll(tok, &pos);
        if(!is_int_suffix(tok.c_str() + pos)) throw std::runtime_error(
          tok + " is not a valid integer literal (invalid suffix)"
        );
        return temp;
      }
      else if(is_identifier(tok))
      {
        return 0;
      }
      else
      {
        throw std::runtime_error(
          tok + "is not a valid integer literal or identifier"
        );
      }
    }

    long long preprocessor_tok_to_num(
      const preprocessor_token& tok
    )
    {
      return preprocessor_tok_to_num(tok.string());
    }

    long long Preprocessor::parse_num_expr()
    {

      // Extract a *preprocessed* token (macros already expanded, buffered)
      std::string tok = get_token().string();

      // Check for unary operators
      if(tok == "!")
      {
        return !parse_num_expr();
      }
      else if(tok == "~")
      {
        return ~parse_num_expr();
      }
      else if(tok == "-")
      {
        return -parse_num_expr();
      }
      else if(tok == "+") // Unary + is a no-op
      {
        return parse_num_expr();
      }

      // Check for definition check
      if(tok == "defined")
      {
        tok = buffered_raw_token().string();
        if(is_identifier(tok))
        {
          return macro_state.is_defined(tok);
        }
        else if(tok != "(")
        {
          throw std::runtime_error(
            "'defined' must be followed by ( or identifier"
          );
        }
        else
        {
          tok = buffered_raw_token().string();
          if(!is_identifier(tok)) throw std::runtime_error(
            "Invalid macro " + tok + " (must be C identifier)"
          );
          long long temp = macro_state.is_defined(tok);
          if(buffered_raw_token() != ")") throw std::runtime_error(
            "Argument to 'defined' must be ended with )"
          );
          return temp;
        }
      }

      // Check for parentheses
      if(tok == "(")
        return parse_delim_expr(")", false);

      // Return numeric value, if any
      return preprocessor_tok_to_num(tok);
    }

    int preprocessor_op_precedence(const std::string& op)
    {
      if((op == "*") || (op == "/") || (op == "%")) return 3;
      if((op == "+") || (op == "-")) return 4;
      if((op == ">>") || (op == "<<")) return 5;
      if((op == ">") || (op == "<") || (op == ">=") || (op == "<=")) return 6;
      if((op == "==") || (op == "!=")) return 7;
      if(op == "&") return 8;
      if(op == "^") return 9;
      if(op == "|") return 10;
      if(op == "&&") return 11;
      if(op == "||") return 12;
      else return -1;

    }

    void eval_stack(std::vector<long long>& vals, std::vector<std::string>& ops)
    {
      while(!ops.empty())
      {
        //TODO: pick a more descriptive message
        if(vals.size() < 2)
          throw std::runtime_error(
            "Too few arguments in stack ("
            + std::to_string(ops.size())
            + " operations remaining)"
          );
        auto acc = vals.end() - 2;
        auto oprnd = *(vals.end() - 1);

        if(ops.back() == "*") *acc *= oprnd;
        else if(ops.back() == "/") {
          if(!oprnd) throw std::runtime_error("Division by zero!");
          *acc /= oprnd;
        }
        else if(ops.back()== "%") *acc %= oprnd;
        else if(ops.back()== "+") *acc += oprnd;
        else if(ops.back()== "-") *acc -= oprnd;
        else if(ops.back()== ">>") *acc >>= oprnd;
        else if(ops.back()== "<<") *acc <<= oprnd;
        else if(ops.back()== "&") *acc &= oprnd;
        else if(ops.back()== "^") *acc ^= oprnd;
        else if(ops.back()== "|") *acc |= oprnd;
        else if(ops.back()== ">") *acc = (*acc > oprnd);
        else if(ops.back()== "<") *acc = (*acc < oprnd);
        else if(ops.back()== ">=") *acc = (*acc >= oprnd);
        else if(ops.back()== "<=") *acc = (*acc <= oprnd);
        else if(ops.back()== "==") *acc = (*acc == oprnd);
        else if(ops.back()== "!=") *acc = (*acc != oprnd);
        else if(ops.back()== "&&") *acc = (*acc && oprnd);
        else if(ops.back()== "||") *acc = (*acc || oprnd);
        else throw std::runtime_error("Invalid operation " + ops.back());

        ops.pop_back();
        vals.pop_back();
      }
      //TODO: pick a more descriptive message
      if(vals.size() > 1)
        throw std::runtime_error(
          "Too many arguments in stack ("
          + std::to_string(vals.size() - 1)
          + ") remaining"
        );
    }

    long long Preprocessor::parse_delim_expr(std::string delim, bool allow_eof)
    {
      std::string tok;
      std::vector<long long> values {};
      std::vector<std::string> ops {};
      do {
        // Extract an operand, processed with applicable unary operators
        values.push_back(parse_num_expr());

        // Check for additional operands
        tok = buffered_raw_token().string();
        if(tok.empty() || (tok == delim)) {
          // Evaluate the stack then break
          eval_stack(values, ops);
          break;
        }
        // Extract a binary operator, after performing a sanity check
        if(tok.size() > 2)
        {
          throw std::runtime_error(
            "Expected binary operator, got \""
            + tok
            + "\""
          );
        }

        // Check operator precedence, and if so, collapse operation stack
        if(!ops.empty())
        {
          int prec = preprocessor_op_precedence(tok);
          if(prec == -1)
            throw std::runtime_error("Invalid operation " + tok);
          int prec_back = preprocessor_op_precedence(ops.back());
          if(prec_back == -1)
            throw std::runtime_error("Invalid operation " + ops.back());

          // If lower precedence than previous operator(s), evaluate them all
          // first. If higher precedence, add to stack
          if(prec < prec_back)
          {
            eval_stack(values, ops);
          }
        }

        // Append the current operator to the stack
        ops.push_back(std::move(tok));

      } while(lexer.good());

      eval_stack(values, ops);
      if(tok.empty() && !allow_eof) throw std::runtime_error(
        "EOF while parsing expression"
      );
      if(!ops.empty()) throw std::runtime_error(
        "Could not parse expression ("
        + std::to_string(ops.size())
        + " operations remaining)"
      );
      if(values.size() != 1) throw std::runtime_error(
        "Error parsing expression"
      );
      return values.front(); // Return expression parsing result
    }

}
