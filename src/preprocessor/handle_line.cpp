#include <simplec/preprocessor/token.hpp>
#include <simplec/preprocessor/ttype.hpp>
#include <simplec/preprocessor.hpp>

#include <string>
#include <exception>

namespace simplec
{

  Preprocessor& Preprocessor::handle_line()
  {
    preprocessor_token tok = get_token();
    if(tok.string() == "\n") throw std::runtime_error(
      "#line must be followed by line number"
    );
    size_t pos;

    if(!isdigit(tok.front())) throw std::runtime_error(
      tok.string() + " is not a valid integer literal (invalid first character)"
    );

    size_t line_num = std::stoul(tok.string(), &pos);
    if(!is_int_suffix(pos + tok.string().c_str())) throw std::runtime_error(
      tok.string() + " is not a valid integer literal (invalid suffix)"
    );

    macro_state.set_line(line_num);

    tok = get_token();

    // End of line or EOF
    if(tok.empty() || (tok == "\n")) return *this;
    else
    {
      if(!is_string_literal(tok)) throw std::runtime_error(
        "File name must be a valid string literal"
      );
      macro_state.set_file_name(tok.string());
    }

    return *this;
  }

}
