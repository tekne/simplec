#include <simplec/preprocessor/ttype.hpp>
#include <simplec/lexer/read_identifier.hpp>
#include <simplec/lexer/read_char_literal.hpp>
#include <simplec/lexer/read_string_literal.hpp>
#include <simplec/lexer/read_preprocessing_number.hpp>

namespace simplec
{

  bool is_identifier(const std::string& s)
  {
    return s.end() == read_identifier(s.begin(), s.end());
  }
  bool is_char_literal(const std::string& s)
  {
    return s.end() == read_char_literal(s.begin(), s.end());
  }
  bool is_string_literal(const std::string& s)
  {
    return s.end() == read_string_literal(s.begin(), s.end());
  }
  bool is_preprocessing_number(const std::string& s)
  {
    return s.end() == read_preprocessing_number(s.begin(), s.end());
  }

}
