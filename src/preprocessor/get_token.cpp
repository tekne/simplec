#include <istream>
#include <cctype>
#include <string>

#include <simplec/preprocessor/token.hpp>
#include <simplec/preprocessor.hpp>

namespace simplec
{

  std::string Preprocessor::get_raw_value() {
    lexer.get_next_token();
    macro_state.line += lexer.get_offset(); // Deal with offset every token
    // Deal with newlines on raw extraction
    if(lexer.begin_current() != lexer.end_current())
    {
      if('\n' == *lexer.begin_current()) macro_state.line++;
    }

    return lexer.get_current_token();
  }

  preprocessor_token Preprocessor::get_raw_token()
  {
    return tag_token(get_raw_value());
  }

  preprocessor_token Preprocessor::get_token()
  {
    using std::swap;

    // Check if most recent pushed token is EOF. If so, this implies the end
    // of the translation unit, i.e. stop reading, and empty the buffer!
    if((bpos < token_buffer.size()) && token_buffer.back().empty()) {
      return token_buffer[bpos++];
    }

    // Otherwise, check if more than 1 remain in buffer, in which case,
    // again, empty the buffer!
    if(token_buffer.size() - bpos > 1) {
      return token_buffer[bpos++];
    }

    // If only one element is in the buffer, but that element is a newline,
    // return it and clear the buffer
    if(!token_buffer.empty() && token_buffer.back() == "\n") {
      preprocessor_token temp = std::move(token_buffer.back());
      clear_buffer();
      return temp;
    }

    // Dump extraneous buffer elements (keep buffer size in check)
    if((bpos > 0) && (bpos < token_buffer.size()))
    {
      swap(token_buffer[0], token_buffer[bpos]);
      token_buffer.resize(1);
      bpos = 0;
    }
    else if((bpos > 0) && (bpos == token_buffer.size()))
    {
      bpos = 0;
      clear_buffer();
    }
    else if(bpos > token_buffer.size()) throw
      std::runtime_error("Internal error: buffer position out of bounds!");

    // Knowing there are either one or no elements in the buffer,
    // read a new token
    preprocessor_token tok = get_raw_token();

    // Check for newlines, which are not further processed, and dump the buffer
    if(tok == "\n")
    {
      return buffered_return(tok);
    }

    // Check for EOF
    if(tok.empty())
    {
      // Check for base file
      if(macro_state.get_file_ptr())
      {
        // Ascend up one include level in the preprocessor state
        macro_state.exit_current_file();

        // Check if any files are remaining
        if(file_stack.empty())
        {
          // Signal end of translation unit (base file). This will get returned
          // when the buffer is subsequently flushed (on the next function call)
          // and indicate to the caller that the translation unit has terminated
          return buffered_return(tok);
        }

        // Close the most recently opened file, then recursively get a new token
        file_stack.pop_back();
        if(file_stack.empty()) lexer.open(root_source);
        else lexer.open(file_stack.back());
        return get_token();
      }
      else // No files remaining. Signal end of translation unit, as above
      {
        return buffered_return(tok);
      }
    }

    // Check for preprocessor directives
    if(tok.front() == '#' || tok.front() == '%')
    {
      if(tok == "##" || tok == "%:%:")
      {
        // Check for empty token buffer
        if(token_buffer.empty()) throw std::runtime_error(
          "No valid token to concatenate with"
        );

        // Cache the token to be accumulated to
        preprocessor_token back = std::move(token_buffer.back());
        clear_buffer();

        // Obtain a new, non-newline token
        tok = get_stripped();
        if(tok.empty())
        {
          throw
          std::runtime_error("Cannot concatenate past end of file");
        }
        else if(tok.front() == '#')
        {
          throw
          std::runtime_error("Cannot concatenate with preprocessor directive");
        }

        // Prepend the cached token to the beginning of the buffer.
        prepend_to_buffer(std::move(back += tok));

        // Get a new token recursively on the thus-modified buffer
        // (to allow accumulation)
        return get_token();
      }
      else if(tok == "#" || tok == "%:")
      {
        if(!in_preprocessor && token_buffer.empty())
        {
          clear_buffer();
          handle_directive(); // Handle preprocessor directive
          return tag_token("\n"); // Return newlines
        }
        else if(in_preprocessor)
        {
          // Return # to be used by handle_define, which may need to stringify
          // arguments
          return buffered_return(tok);
        }
        else
        {
          throw std::runtime_error(
            "# must be preceded by newline"
          );
        }
      }
      else if(tok.front() == '#')
      {
        throw std::runtime_error(
          "Invalid token " + tok.string()
        );
      }
    }

    // Check for potential macros
    if(isalpha(tok.front()) || (tok.front() == '_'))
    {
      auto iter = macro_state.get_macro_iter(tok);
      if(iter == macro_state.end_macros()) // Not a macro, so pass it forwards
      {
        return buffered_return(tok);
      }
      // Check for function-like-macro
      if(iter->second.is_function_like())
      {
        tok = get_raw_token();
        if(tok != "(")
          throw std::runtime_error("Function-like macro requires arguments");
        auto temp = iter->second.resolve(strip_tokens(get_token_line(")")));
        if(token_buffer.empty())
        {
          for(auto& s: temp) {
            token_buffer.push_back(tag_token(std::move(s)));
          }
          return get_token();
        }
        else
        {
          tok = std::move(token_buffer.back());
          clear_buffer();
          for(auto&& s: temp) {
            token_buffer.push_back(tag_token(std::move(s)));
          }
          return tok;
        }
      }
      else
      {
        // Expand macro into buffer and return the token currently in buffer
        if(!token_buffer.empty())
        {
          tok = std::move(token_buffer.back());
          clear_buffer();
          auto temp = iter->second.resolve();
          token_buffer.reserve(temp.size());
          for(auto&& s: temp) {
            token_buffer.push_back(tag_token(std::move(s)));
          }
          return tok;
        }
        else
        {
          auto temp = iter->second.resolve();
          token_buffer.reserve(temp.size());
          for(auto& s: temp) {
            token_buffer.push_back(tag_token(std::move(s)));
          }
          return get_token();
        }
      }
    }

    // Move the buffer one forwards otherwise
    return buffered_return(tok);
  }

}
