#include <simplec/preprocessor/macro.hpp>

namespace simplec
{

  std::vector<std::string> macro::resolve() {
    if(is_function_like())
      throw std::runtime_error("Function-like macro not provided arguments");
    return tokens;
  }

  std::vector<std::string>& macro::append(std::vector<std::string>& in) {
      if(is_function_like())
        throw std::runtime_error("Function-like macro not provided arguments");
      for(const auto& s: tokens) in.push_back(s);
      return in;
  }

  std::string macro::resolve_to_str() {
    if(is_function_like())
      throw std::runtime_error("Function-like macro not provided arguments");
    if(tokens.empty()) return "";


    size_t len = tokens.size() - 1;
    for(const auto& s: tokens) len += s.size();

    std::string result = tokens.front();
    result.reserve(len);

    for(auto iter = tokens.begin() + 1; iter != tokens.end(); iter++)
    {
      if(!iter->empty())
      {
        result += " ";
        result += *iter;
      }
    }

    return result;
  }

}
