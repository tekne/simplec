#include <simplec/preprocessor/file_handling.hpp>

#include <exception>

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

namespace simplec
{

  preprocessor_file_source preprocessor_include_paths::get_local(
    const std::string& file,
    const preprocessor_file_source& src
  ) const
  {
    auto parent = src.parent();
    if(boost::filesystem::exists(parent / file))
    {
      return {parent/file, src};
    }
    for(const auto& dir: local_search_paths)
    {
      if(boost::filesystem::exists(dir / file))
      {
        return {dir/file, src};
      }
    }
    return get_global(file);
  }


  preprocessor_file_source preprocessor_include_paths::get_local(
    const std::string& file
  ) const
  {
    if(boost::filesystem::exists(file))
    {
      return {file};
    }
    for(const auto& dir: local_search_paths)
    {
      if(boost::filesystem::exists(dir / file))
      {
        return {dir/file};
      }
    }
    return get_global(file);
  }

  preprocessor_file_source preprocessor_include_paths::get_global(
    const std::string& file,
    const preprocessor_file_source& src
  ) const
  {
    return get_global(file).set_source(src);
  }

  preprocessor_file_source preprocessor_include_paths::get_global(
    const std::string& file
  ) const
  {
    for(const auto& dir: global_search_paths)
    {
      if(boost::filesystem::exists(dir / file))
      {
        return {dir / file};
      }
    }
    throw std::runtime_error("Cannot open " + file);
  }

}
