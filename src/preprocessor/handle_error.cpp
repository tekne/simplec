#include <simplec/preprocessor/token.hpp>
#include <simplec/preprocessor.hpp>

#include <string>

namespace simplec
{

  Preprocessor& Preprocessor::handle_error()
  {
    throw std::runtime_error("#error handling has not yet been implemented");
  }

  Preprocessor& Preprocessor::handle_warning()
  {
    throw std::runtime_error("#warning handling has not yet been implemented");
  }

}
