#include <simplec/preprocessor/token.hpp>
#include <simplec/preprocessor/ttype.hpp>
#include <simplec/preprocessor.hpp>

#include <string>
#include <vector>
#include <exception>
#include <istream>

namespace simplec
{

  Preprocessor& Preprocessor::if_exclude_helper()
  {
    preprocessor_token tok;
    clear_buffer(); // Clear the token buffer
    while(str_good())
      {
        tok = get_raw_token();
        if(!str_good()) throw std::runtime_error("EOF before #endif");
        // Scan for preprocessor directive to end skipped region
        if(tok == "#") {
          tok = get_raw_token();
          if(tok == "elif") {
            return handle_elif();
          }
          else if(tok == "else") {
            return handle_else();
          }
          else if(tok == "endif") {
            return handle_endif();
          }
        }
        // Guarantee newline before next token
        skip_line();
      }
    if(!str_good()) throw std::runtime_error("EOF before #endif");
    return *this;
  }

  Preprocessor& Preprocessor::else_exclude_helper()
  {
    preprocessor_token tok;
    clear_buffer(); // Clear the token buffer
    while(str_good())
      {
        tok = get_raw_token();
        if(!str_good()) throw std::runtime_error("EOF before #endif");
        // Scan for preprocessor directive to end skipped region
        if(tok == "#") {
          tok = get_raw_token();
          if(tok == "endif") {
            return handle_endif();
          }
        }
        skip_line(); // Guarantee newline before scanned token
      }
    if(!str_good()) throw std::runtime_error("EOF before #endif");
    return *this;
  }

  Preprocessor& Preprocessor::handle_if()
  {
    long long value = parse_line_expr();
    in_if.push_back(value);
    if(!in_if.back()) return if_exclude_helper();
    return *this;
  }

  Preprocessor& Preprocessor::handle_else()
  {
    if(in_if.empty())
      throw std::runtime_error("#else without associated #if");
    if(in_if.back()) return else_exclude_helper();
    return *this;
  }

  Preprocessor& Preprocessor::handle_elif()
  {
    if(in_if.empty())
      throw std::runtime_error("#elif without associated #if");
    if(in_if.back()) return else_exclude_helper(); // Skip to #endif
    return handle_if(); // Treat as a #if statement
  }

  Preprocessor& Preprocessor::handle_ifdef()
  {
    preprocessor_token tok = get_raw_token();
    if(!is_identifier(tok))
      throw std::runtime_error(
        "Expected identifier after ifdef, got " + tok.string()
      );
    in_if.push_back(macro_state.is_defined(tok));
    if(!in_if.back())
    {
      return if_exclude_helper();
    }
    else
    {
      tok = get_raw_token();
      if(tok != "\n") throw std::runtime_error(
        "Expected newline after ifdef, got " + tok.string()
      );
      return *this;
    }
  }

  Preprocessor& Preprocessor::handle_ifndef()
  {
    preprocessor_token tok = get_raw_token();
    if(!is_identifier(tok))
      throw std::runtime_error("Cannot check definition of non identifier");
    in_if.push_back(!macro_state.is_defined(tok));
    if(!in_if.back())
    {
      return if_exclude_helper();
    }
    else
    {
      tok = get_raw_token();
      if(tok != "\n") throw std::runtime_error(
        "Expected newline after ifndef, got " + tok.string()
      );
      return *this;
    }
  }

  Preprocessor& Preprocessor::handle_endif()
  {
    if(in_if.empty())
      throw std::runtime_error("#endif without associated #if");
    in_if.pop_back();
    return *this;
  }
}
