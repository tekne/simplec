#include <simplec/preprocessor/macro.hpp>
#include <simplec/preprocessor/macro_utils.hpp>

#include <string>

namespace simplec
{

  std::vector<std::string> macro::resolve(const std::vector<std::string>& args)
  {
    std::vector<std::string> result {};
    append(args, result);
    return result;
  }

  std::vector<std::string>& macro::append(
    const std::vector<std::string>& args,
    std::vector<std::string>& result
  )
  {
    if(!is_function_like())
      throw std::runtime_error("Cannot resolve token string macro as function");

    // Split the vector on "," tokens to get individual macro arguments
    auto split = split_vector(args, ",");

    if(split.size() < num_args())
      throw std::runtime_error("Too few arguments provided");
    if(!is_variadic() && split.size() != num_args())
      throw std::runtime_error(
        "Too many arguments provided to non-variadic function-like macro"
      );

    auto varargs = args.begin();
    // Beginning of the first variadic argument, not counting the comma
    if(num_args()) varargs = split[num_args() - 1] + 1;

    for(const auto& s: tokens)
    {
      if(read_argnum(s) != -1)
      {
        unsigned arg_no = read_argnum(s);
        if(arg_no < nargs)
        {
          auto pos = args.begin();
          if(arg_no) pos = split[arg_no - 1] + 1;
          auto epos = split[arg_no];
          while(pos != epos)
          {
            result.push_back(*(pos++));
          }
        }
        else
        {
          // Write all variadic arguments as tokens, along with "," tokens
          auto pos = varargs;
          while(pos != args.end())
          {
            result.push_back(*(pos++));
          }
        }
      }
      else
      {
        result.push_back(s);
      }
    }

    return result;
  }

  std::string macro::resolve_to_str(const std::vector<std::string>& args)
  {
    if(!is_function_like())
        throw std::runtime_error("Cannot resolve token string macro as function");

    std::string result;
    // Split the vector on "," tokens to get individual macro arguments
    auto split = split_vector(args, ",");

    if(split.size() < num_args())
      throw std::runtime_error(
        "Too few (" + std::to_string(split.size()) + ") arguments provided"
      );
    if(!is_variadic() && split.size() != num_args())
      throw std::runtime_error(
        "Too many arguments provided to non-variadic function-like macro"
      );

    auto varargs = args.begin();
    // Beginning of the first variadic argument, not counting the comma
    if(num_args()) varargs = split[num_args() - 1] + 1;

    for(const auto& s: tokens)
    {
      if(read_argnum(s) != -1)
      {
        unsigned arg_no = read_argnum(s);
        if(arg_no < nargs)
        {
          auto pos = args.begin();
          if(arg_no) pos = split[arg_no - 1] + 1;
          auto epos = split[arg_no];
          while(pos != epos)
          {
            result += *pos;
            result += " ";
            pos++;
          }
        }
        else
        {
          // Write all variadic arguments as tokens, along with "," tokens
          auto pos = varargs;
          while(pos != args.end())
          {
            result += *(pos++);
            result += " ";
          }
        }
      }
      else
      {
        result += s;
        result += " ";
      }
    }

    result.pop_back(); // Remove extraneous " "

    return result;
  }

}
