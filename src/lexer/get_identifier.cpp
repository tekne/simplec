#include <simplec/lexer.hpp>
#include <simplec/lexer/read_identifier.hpp>

namespace simplec
{

  Lexer::const_iterator Lexer::get_identifier(Lexer::const_iterator i)
  {
    return read_identifier(i, linebuffer.cend());
  }

}
