#include <simplec/lexer.hpp>
#include <simplec/lexer/read_preprocessing_number.hpp>

namespace simplec
{

  Lexer::const_iterator Lexer::get_preprocessing_number(Lexer::const_iterator i)
  {
    return read_preprocessing_number(i, linebuffer.cend());
  }

}
