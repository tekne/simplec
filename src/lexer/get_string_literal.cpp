#include <simplec/lexer.hpp>
#include <simplec/lexer/read_string_literal.hpp>

namespace simplec
{

  Lexer::const_iterator Lexer::get_string_literal(Lexer::const_iterator i)
  {
    return read_string_literal(i, linebuffer.cend());
  }

}
