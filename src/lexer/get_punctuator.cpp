#include <simplec/lexer.hpp>
#include <simplec/lexer/read_punctuator.hpp>

namespace simplec
{

  Lexer::const_iterator Lexer::get_punctuator(Lexer::const_iterator i)
  {
    return read_punctuator(i, linebuffer.cend());
  }

}
