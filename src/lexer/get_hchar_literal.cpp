#include <simplec/lexer.hpp>
#include <simplec/lexer/read_hchar_literal.hpp>

namespace simplec
{

  Lexer::const_iterator Lexer::get_hchar_literal(Lexer::const_iterator i)
  {
    return read_hchar_literal(i, linebuffer.cend());
  }

}
