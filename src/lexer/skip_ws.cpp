#include <simplec/lexer.hpp>
#include <simplec/lexer/skip_whitespace.hpp>

namespace simplec
{
  Lexer::const_iterator Lexer::skip_ws(Lexer::const_iterator i)
  {
    return skip_whitespace(i, linebuffer.cend());
  }
}
