#include <simplec/lexer.hpp>

#include <algorithm>
#include <iostream>

namespace simplec
{
  Lexer& Lexer::append_line()
  {
    // Check for end of file or invalid (null) source
    if(!good()) return *this;

    while(src->good())
    {
      // Make space for a new buffer chunk, first using any remaning capacity
      size_t end = linebuffer.size();
      if(linebuffer.capacity() > linebuffer.size())
      {
        // Don't zero a massive block of memory unnecessarily
        if(linebuffer.capacity() - linebuffer.size() > initial_buffer_size)
        {
          linebuffer.resize(linebuffer.size() + initial_buffer_size);
        }
        else // Avoid reading tiny chunks at a time
        {
          linebuffer.reserve(linebuffer.size() + initial_buffer_size);
          linebuffer.resize(linebuffer.capacity());
        }
      }
      else
      {
        linebuffer.reserve(linebuffer.size() + initial_buffer_size);
        linebuffer.resize(linebuffer.capacity());
      }

      // Write, starting at the old end of the buffer
      src->getline(
        linebuffer.data() + end, linebuffer.size() - end
      );
      linebuffer.resize(end + src->gcount());

      // Check stream status:
      if(src->good())
      {
        // Check for lines ending in '\\'
        if(linebuffer.size() > 1)
        {
          if('\\' == *(linebuffer.end() - 2))
          {
            linebuffer.pop_back(); // Terminating null
            linebuffer.pop_back(); // Escape character
            // Append the rest of the line by "calling" recursively
            continue;
          }
          // Replace terminating null with newline
          linebuffer.back() = '\n';
          break; // "return"
        }
      }
      else if(src->eof() || src->bad())
      {
        // NOTE: this is undefined behaviour, since the file would be
        // ending without a newline. If the file ends with a '\\', it will be
        // replaced with ' ', though again, this is undefined behaviour.

        // TODO: consider adding a warning for this behaviour

        // Check for the special case of an empty file (no null written)
        if(linebuffer.empty()) break;

        // Replace null character with newline
        if(linebuffer.back()) linebuffer.push_back('\n');
        else linebuffer.back() = '\n';

        // Check if any characters other than the terminating null have been
        // appended, and if so, check for backslashes, to replace
        if(linebuffer.size() > 1) if('\\' == *(linebuffer.end() - 2))
          *(linebuffer.end() - 2) = ' ';

        break; // Take what is in the buffer now as the last of the file
      }
      else if(src->fail())
      {
        src->clear(); // Clear fail bit
        continue; // Append the rest of the line "recursively"
      }
    }
    return *this;
  }

  Lexer& Lexer::getline()
  {
    // Clear the linebuffer, then append to the (empty) linebuffer
    linebuffer.clear();
    append_line();
    return *this;
  }

}
