#include <simplec/lexer.hpp>
#include <simplec/lexer/read_token.hpp>

namespace simplec
{

  Lexer::const_iterator Lexer::get_token(Lexer::const_iterator i)
  {
    return read_token(i, linebuffer.cend(), prev_typ);
  }

}
