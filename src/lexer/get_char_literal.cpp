#include <simplec/lexer.hpp>
#include <simplec/lexer/read_char_literal.hpp>

namespace simplec
{

  Lexer::const_iterator Lexer::get_char_literal(Lexer::const_iterator i)
  {
    return read_char_literal(i, linebuffer.cend());
  }

}
